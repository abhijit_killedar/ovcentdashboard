package dynamic.search.ObjectSearch;

import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.ProcessMem;
import java.util.HashMap;
import java.util.Map;

public class Out implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		Map<String, Object> outputParams = new HashMap<String, Object>();
		
		//outputParams.put("deviceId", ((Map<String, Object>) inputParams.get("deviceObj")).get("id"));
		outputParams.put("SearchObjectList",  inputParams.get("SearchObjectList"));
		return outputParams;
	}

}