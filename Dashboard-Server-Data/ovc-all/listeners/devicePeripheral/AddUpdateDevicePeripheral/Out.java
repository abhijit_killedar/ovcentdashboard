package dynamic.devicePeripheral.AddUpdateDevicePeripheral;

import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.ProcessMem;
import java.util.HashMap;
import java.util.Map;

public class Out implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		Map<String, Object> outputParams = new HashMap<String, Object>();
		outputParams.put("returnCode", inputParams.get("returnCode"));
		outputParams.put("returnMsg", inputParams.get("returnMsg"));
		return outputParams;
	}

}