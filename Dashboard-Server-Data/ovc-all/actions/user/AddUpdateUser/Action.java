package dynamic.user.AddUpdateUser; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;
import com.oneview.util.SHAHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {

		Map<String, Object> outputParams = new HashMap<String, Object>();
		//Connection conn = null;
		String returnMsg ="";
		ArrayList<HashMap<String, String>> rolesList = (ArrayList<HashMap<String, String>>)inputParams.get("rolesList");
		
		boolean roleUpdateFlag 			= false;
		
		try (Connection conn = DatabaseHelper.getConnection())
		{
			//Get valid roles and locations
			
			HashMap<String,String> validRoleMap = getRoleMap(conn);
			HashMap<String, String> validLocationMap = getLocationMap(conn);
			
			//Validation to check input params are valid and autofill other non mandatory fields.
			if(!(inputParams.containsKey("userId")))
			{
				outputParams.put("returnCode", "401");
				outputParams.put("returnMsg", "Invalid user id");
			}
			else if(!(inputParams.containsKey("userName")))
			{
				outputParams.put("returnCode", "402");
				outputParams.put("returnMsg", "Invalid user name.");
			}
			else if(inputParams.get("userId") == null || (inputParams.get("userId") != null && ((String) inputParams.get("userId")).equals("")))
			{
				outputParams.put("returnCode", "401");
				outputParams.put("returnMsg", "Invalid user id.");
			}
			else if(inputParams.get("userName") == null || (inputParams.get("userName") != null && ((String) inputParams.get("userName")).equals("")))
			{
				outputParams.put("returnCode", "402");
				outputParams.put("returnMsg", "Invalid user name.");
			}
			else if(!(validEmail((String) inputParams.get("email"))))
			{
				outputParams.put("returnCode", "404");
				outputParams.put("returnMsg", "Invalid location.");
			}
			else if(rolesList.size() != 0)
			{
				
				roleUpdateFlag 			= true;//indicates we have to update roles and locations.
				
				boolean validRoleFlag 		= true;
				boolean validLocationFlag 	= true;
				
				for(HashMap<String,String> roleLocMap : rolesList)
				{
					String tmpRole = roleLocMap.get("roleId");
					String tmpLoc = roleLocMap.get("locationId");
					if(!validRoleMap.containsKey(tmpRole))
					{
						validRoleFlag = false;
					}
					if(!validLocationMap.containsKey(tmpLoc))
					{
						validLocationFlag = false;
					}
					
				}
				//call the location add/update
				if(!validRoleFlag)
				{
					outputParams.put("returnCode", "403");
					outputParams.put("returnMsg", "Invalid role.");
				}
				else if(!validLocationFlag)
				{
					outputParams.put("returnCode", "405");
					outputParams.put("returnMsg", "Invalid location.");
				}
				else
				{
					returnMsg = addUpdateUser(inputParams,roleUpdateFlag, conn);

					outputParams.put("returnCode", (returnMsg.split("-"))[0]);
					outputParams.put("returnMsg", (returnMsg.split("-"))[1]);
				}
				
			}
			else
			{
				returnMsg = addUpdateUser(inputParams,roleUpdateFlag, conn);

				outputParams.put("returnCode", (returnMsg.split("-"))[0]);
				outputParams.put("returnMsg", (returnMsg.split("-"))[1]);
			}
			
			
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			outputParams.put("returnCode", "450");
			outputParams.put("returnMsg", "Generic SQL exception"+e.getLocalizedMessage());
			//throw new DynamicExecuterException(e.getMessage(), null, null);
		}	
	    return outputParams;
	}
	

	/****
	 * user_tbl  and user_role_tble store user  information and there is a fk mapping for the role information in the user_role_tble to the role
	 * 
	 ****/
	private static String addUpdateUser(Map<String, Object>  inputParams,boolean roleUpdateFlag,Connection conn){
		
		StringBuilder sbUserSelect 		= null;
		StringBuilder sbUserInsert 		= null;
		
		StringBuilder sbUserRoleInsert 		= null;

		
		PreparedStatement psUserSelect 		= null;
		PreparedStatement psUserInsert 		= null;
		
		PreparedStatement psUserRoleInsert 		= null;

		
		
		ResultSet rsUserSelect = null;
		
		String errorMsg ="";
		String errorCode ="";
		
		boolean doUserAdd = false;
		boolean doUserUpdate = false;
		
		try
		{
			String userId = (String) inputParams.get("userId");
			String userName = (String) inputParams.get("userName");
	        String firstName = (String) inputParams.get("firstName");
	        String lastName = (String) inputParams.get("lastName");
			String email = (String) inputParams.get("email");
			String active = (String) inputParams.get("active");
			ArrayList<HashMap<String, String>> rolesList = (ArrayList<HashMap<String, String>>) inputParams.get("rolesList");	
			

			
			
			//Step1: Check to see whether user exists
			
			//build the select location_and_group_tbl prepared statement
			sbUserSelect = new StringBuilder("select username,id "
					+ "from user_tbl "
					+ "where id  =? ");
			
			psUserSelect = conn.prepareStatement(sbUserSelect.toString());
			
			psUserSelect.setString(1,	userId);
			
			rsUserSelect = psUserSelect.executeQuery();

			if(rsUserSelect.isBeforeFirst())
			{
				while(rsUserSelect.next())
				{
					String selectedUserId = rsUserSelect.getString("id");
					//Add code to update user and user_role tbl
					
					//Step2: Since user tbl has entry run updates for both tables.
					doUserUpdate(conn,selectedUserId,userName,firstName,lastName,email,active,rolesList);
					

					errorMsg ="User updated.";
					errorCode="204";
					break;		
			
				}				
			}
			else
			{
				
				doUserAdd = true;
				//String rndUserId = UUID.randomUUID().toString();
				//Step3: If user does not exist then insert new user record in both user_tbl and user_role_tbl
				
				//build the user-tbl insert prepared statement
				sbUserInsert = new StringBuilder("insert into user_tbl  values (?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?)");
				
				psUserInsert = conn.prepareStatement(sbUserInsert.toString());

				SimpleDateFormat sdf = new SimpleDateFormat("MMYY");
				psUserInsert.setString(1,	userId);		
				psUserInsert.setString(2,	userName);
				
				psUserInsert.setString(3,	SHAHelper.generateSHA512String(userName+sdf.format(new Date())));
				psUserInsert.setString(4,	null);
				psUserInsert.setString(5,	null);
				psUserInsert.setString(6,		firstName);
				psUserInsert.setString(7,	null);
				psUserInsert.setString(8,	lastName);
				psUserInsert.setString(9,	null);
				psUserInsert.setString(10,	null);
				psUserInsert.setString(11,	email);
				psUserInsert.setInt(12,	0);
				psUserInsert.setString(13,	null);
				psUserInsert.setInt(14,	Integer.parseInt(active));
				psUserInsert.setInt(15,	0);


				psUserInsert.executeUpdate();
				
				psUserInsert.close();

				//Call method to refresh user roles
				if(roleUpdateFlag)
					refreshUserRoles(conn, userId, rolesList);
				
				
				errorMsg ="User created.";
				errorCode="201";

			}
			psUserSelect.close();
			rsUserSelect.close();
			
			//Step4: Return 
		
			

		}
		catch(Exception e)
		{
			errorMsg ="Generic SQL exception"+e.getLocalizedMessage();
			errorCode="450 ";
			e.printStackTrace();

			if (psUserInsert != null)
				psUserInsert.close();

			if(rsUserSelect != null)
				rsUserSelect.close();
			if(psUserSelect != null)
				psUserSelect.close();	
		}
		finally
		{
			return errorCode+"-"+errorMsg;
		}
	}
	
	
	/******
	 * The method needs to update the entry in the user_tbl.  
	 * *******/
	private static void doUserUpdate (Connection conn, String userId,String userName, String firstName,String lastName,String email,String active,ArrayList<HashMap<String, String>> rolesList) throws SQLException
	{
		
		StringBuilder sbUserUpdate = new StringBuilder(
				" update user_tbl set username = ?, firstName = ?,lastName = ?,email=? ,active = ? ,lastModified=CURRENT_TIMESTAMP where id= ? ");
		
		
		StringBuilder sbUserRoleSelect 		= null;
		StringBuilder sbUserRoleInsert 		= null;
		
		PreparedStatement psUserRoleSelect 		= null;
		PreparedStatement psUserRoleInsert 		= null;


		ResultSet rsUserRoleSelect = null;

		
		PreparedStatement psUserUpdate = null;

		try 
		{
			
			//Step 1 : Update user details in the user_tbl
			
			psUserUpdate = conn.prepareStatement(sbUserUpdate.toString());
			
			psUserUpdate.setString(1,userName);
			psUserUpdate.setString(2,firstName);
			psUserUpdate.setString(3,lastName);
			psUserUpdate.setString(4,email);
			psUserUpdate.setString(5,active);
			psUserUpdate.setString(6,userId);
			
			
			psUserUpdate.executeUpdate();
			
			psUserUpdate.close();
			
			
			
			//Step 2:Since the user already exists in the system check to see whether the user,role and location combo is present in the user_role_tbl
			
			refreshUserRoles(conn, userId, rolesList);
			

		} catch (SQLException sqlExp) 
		{
			// TODO Auto-generated catch block
			if(psUserUpdate !=null){psUserUpdate.close();}
			if(psUserRoleInsert !=null){psUserRoleInsert.close();}
			if(rsUserRoleSelect !=null){rsUserRoleSelect.close();}
			
			throw new SQLException("Unable to update user "+sqlExp.getLocalizedMessage());
		}
		
	}
	
	
	/******
	 * The method needs to update the entry in the user_role_tbl.  
	 * *******/
	private static void refreshUserRoles (Connection conn, String userId,ArrayList<HashMap<String, String>> rolesList) throws SQLException
	{

		
		StringBuilder sbUserRoleDelete 		= null;
		StringBuilder sbUserRoleInsert 		= null;
		
		PreparedStatement psUserRoleDelete 		= null;
		PreparedStatement psUserRoleInsert 		= null;




		try 
		{
			
			//Step 1:Since the user already exists in the system delete all the the user,role and location  entries present in the user_role_tbl
			
			sbUserRoleDelete = new StringBuilder("delete "
					+ "from user_role_tbl "
					+ "where userId=?");
			
			psUserRoleDelete = conn.prepareStatement(sbUserRoleDelete.toString());
			
			psUserRoleDelete.setString(1,	userId);

			psUserRoleDelete.executeUpdate();
			
			//Step 2 :Build the user_role_tbl insert prepared statement
			
			sbUserRoleInsert = new StringBuilder("insert into user_role_tbl  values (?,?,?)");
			psUserRoleInsert = conn.prepareStatement(sbUserRoleInsert.toString());
			//Step 3: Run a loop for all the roles in the list and insert every one.
			for (HashMap<String,String> roleMap : rolesList) 
			{
				psUserRoleInsert.setString(1,	userId);		
				psUserRoleInsert.setString(2,	roleMap.get("locationId"));
				psUserRoleInsert.setString(3,	roleMap.get("roleId"));
				
				psUserRoleInsert.executeUpdate();
			}
			
			psUserRoleInsert.close();

			

		} catch (SQLException sqlExp) 
		{
			// TODO Auto-generated catch block
			if(psUserRoleDelete !=null){psUserRoleDelete.close();}
			if(psUserRoleInsert !=null){psUserRoleInsert.close();}
			
			throw new SQLException(" Location information incorrect ");
		}
		
		
		
	}
	
	private static HashMap getRoleMap (Connection conn) throws SQLException
	{

		StringBuilder sbRoleSelect	 		= null;
		HashMap<String, String> roleMap = new HashMap<String,String>();

		try 
		{
			
			//Step 1:Select all roles in the roled_tbl
			
			sbRoleSelect = new StringBuilder("select id,name "
					+ "from role_tbl ");

			//Step 2: Build the role_tbl select prepared statement Run a loop for all the roles in the list and throw in hashmap.
			try (PreparedStatement psRoleSelect = conn.prepareStatement(sbRoleSelect.toString())) 
			{
				try (ResultSet rs = psRoleSelect.executeQuery()) 
				{					
					while (rs.next())
					{	
						roleMap.put( rs.getString(1), rs.getString(2));
					}					
				}
				catch(Exception e)
				{
					psRoleSelect.close();
				}
			}


		} catch (SQLException sqlExp) 
		{

		}
		
		return roleMap;
		
	}
	
	private static HashMap getLocationMap (Connection conn) throws SQLException
	{

		StringBuilder sbLocationSelect	 		= null;
		HashMap<String, String> locationMap = new HashMap<String,String>();

		try 
		{
			
			//Step 1:Select all roles in the roled_tbl
			
			sbLocationSelect = new StringBuilder("select id,displayName "
					+ "from location_tbl ");

			//Step 2: Build the role_tbl select prepared statement Run a loop for all the roles in the list and throw in hashmap.
			try (PreparedStatement psLocationSelect = conn.prepareStatement(sbLocationSelect.toString())) 
			{
				try (ResultSet rs = psLocationSelect.executeQuery()) 
				{					
					while (rs.next())
					{	
						locationMap.put( rs.getString(1), rs.getString(2));
					}					
				}
				catch(Exception e)
				{
					psLocationSelect.close();
				}
			}


		} catch (SQLException sqlExp) 
		{

		}
		return locationMap;		
	}
	
	
	static boolean validEmail(String email) {
	    return email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	}


}