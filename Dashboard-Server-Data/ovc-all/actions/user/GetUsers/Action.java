package dynamic.user.GetUsers; 

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {
		
		

		Map<String, Object> outputParams = new HashMap<String, Object>();

		try (Connection conn = DatabaseHelper.getConnection())
		{
				String userId 	 	= (String)inputParams.get("userId");
				String userName 	= (String)inputParams.get("userName");
				String roleId 		= (String)inputParams.get("roleId");
				String locationId 	= (String)inputParams.get("locationId");
				String activeFlag 	= (String)inputParams.get("active");
				
				ArrayList usersList = getUsers(userId,userName,roleId,locationId,activeFlag, conn);
				//System.out.println("aaaa");
				outputParams.put("usersList",usersList);
			
		}catch (Exception e) 
		{
			e.printStackTrace();
			throw new DynamicExecuterException(e.getMessage(), null, null);
		}
		
	    return outputParams;
	}
	
	private ArrayList getUsers(String userId,String userName,String roleId,String locationId,String activeFlag, Connection conn) throws SQLException
	{
		StringBuilder sbUserSelect = null;
		PreparedStatement psUserSelect = null;
		ResultSet rsUserSelect = null;
		ArrayList<Object> userList = new ArrayList<Object>();
		

		try
		{
			//build the device peripherals select prepared statement
			String filterClause = " ";
			if(userId != null && !(userId.equals("")))
			{
				filterClause+= "and  u.Id = ? ";
			}
			if(userName != null && !(userName.equals("")))
			{
				filterClause+= "and  u.userName = ? ";
			}
			if(roleId != null && !(roleId.equals("")))
			{
				filterClause+= "and  r.Name = ? ";
			}
			if(locationId != null && !(locationId.equals("")))
			{
				filterClause+= "and  ur.locationId = ? ";
			}
			if(activeFlag != null && !(activeFlag.equals("")))
			{
				filterClause+= "and  u.active = ? ";
			}
			sbUserSelect = new StringBuilder(
					"select u.Id as userId,u.userName,u.firstName,u.lastName,u.email,r.name as roleId,ur.locationId as locationId,u.active from user_tbl u,user_role_tbl  ur, role_tbl r "
					+ "where u.id = ur.userId and ur.roleId = r.Id and u.isDeleted = 0 ");
			
			sbUserSelect.append(filterClause);
			
			sbUserSelect.append(" order by u.userName ");
					
			psUserSelect = conn.prepareStatement(sbUserSelect.toString());
			int count = 0;
			
			if(userId != null && !(userId.equals("")))
			{
				count++;
				psUserSelect.setString(count,	userId);
			}
			
			if(userName != null && !(userName.equals("")))
			{
				count++;
				psUserSelect.setString(count,	userName);
			}
			if(roleId != null && !(roleId.equals("")))
			{
				count++;
				psUserSelect.setString(count,	roleId);
			}
			if(locationId != null && !(locationId.equals("")))
			{
				count++;
				psUserSelect.setString(count,	locationId);
			}
			if(activeFlag != null && !(activeFlag.equals("")))
			{
				count++;
				psUserSelect.setString(count,	activeFlag);
			}
			
			
			//System.out.println(" sbUserSelect " +sbUserSelect);
			
			rsUserSelect = psUserSelect.executeQuery();
			
			String prioruserName = "";
			ArrayList<HashMap<String, String>> roleList = new ArrayList<HashMap<String, String>> ();
			HashMap<String, Object> hashMapUserObj = new HashMap<>();
			count = 0;
			while(rsUserSelect.next())
			{		
				//System.out.println("step 1");
				count++;
				
				if(prioruserName.equals(rsUserSelect.getObject("userName")))
				{
					//System.out.println("step 2");
					HashMap<String,String> roleLocMap = new HashMap<String,String>();
					
					roleLocMap.put("roleId", (String)rsUserSelect.getObject("roleId"));
					roleLocMap.put("locationId", (String)rsUserSelect.getObject("locationId"));
					
					roleList.add(roleLocMap);
					
				}
				else
				{
					System.out.println("step 3");
					if(count == 1)
					{
						//System.out.println("step 4");
						
						HashMap<String,String> roleLocMap = new HashMap<String,String>();
						
						roleLocMap.put("roleId", (String)rsUserSelect.getObject("roleId"));
						roleLocMap.put("locationId", (String)rsUserSelect.getObject("locationId"));
						
						roleList.add(roleLocMap);
						
						hashMapUserObj.put("id", rsUserSelect.getObject("userId"));
						hashMapUserObj.put("userName", rsUserSelect.getObject("userName"));
						hashMapUserObj.put("firstName", rsUserSelect.getObject("firstName"));
						hashMapUserObj.put("lastName", rsUserSelect.getObject("lastName"));
						hashMapUserObj.put("email", rsUserSelect.getObject("email"));
						hashMapUserObj.put("active", rsUserSelect.getObject("active"));	
					}						
					else
					{
						//System.out.println("step 5");
						
						hashMapUserObj.put("roleList", roleList);
						userList.add(hashMapUserObj);
						new ArrayList<HashMap<String, String>> ();
						hashMapUserObj = new HashMap<>();
						roleList = new ArrayList<HashMap<String, String>> ();
						HashMap<String,String> roleLocMap = new HashMap<String,String>();
						
						roleLocMap.put("roleId", (String)rsUserSelect.getObject("roleId"));
						roleLocMap.put("locationId", (String)rsUserSelect.getObject("locationId"));
						
						roleList.add(roleLocMap);
					}
						
				}	
				//System.out.println("step 6");
				hashMapUserObj.put("id", rsUserSelect.getObject("userId"));
				hashMapUserObj.put("userName", rsUserSelect.getObject("userName"));
				hashMapUserObj.put("firstName", rsUserSelect.getObject("firstName"));
				hashMapUserObj.put("lastName", rsUserSelect.getObject("lastName"));
				hashMapUserObj.put("email", rsUserSelect.getObject("email"));
				hashMapUserObj.put("active", rsUserSelect.getObject("active"));	
				
				prioruserName = (String)rsUserSelect.getObject("userName");
			}
			hashMapUserObj.put("roleList", roleList);
			userList.add(hashMapUserObj);
			
			
			rsUserSelect.close();
			psUserSelect.close();
			
			return userList;
			
		}catch (Exception e){
			if(rsUserSelect !=null)
				rsUserSelect.close();
			if(psUserSelect !=null)
				psUserSelect.close();
			//e.printStackTrace();
			throw new SQLException("Something went wrong, do not commit: "+e.getLocalizedMessage());
		}
	}
	

}