package dynamic.devicePeripheral.GetDevicePeripheral; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {
		
		
		/*
		 *	select dvc.dUUID as dUUID, dvc.alias as Alias, dvc.locationId as LocationId, dvc.active as isActive, perphl.host PrinterIP 
			from 
				device_tbl dvc left join device_peripheral_tbl dvcprl ON dvc.id=dvcprl.deviceId 
				left join peripheral_tbl perphl ON dvcprl.peripheralId = perphl.id and perphl.deviceCategory = 'Printer'
				where 
				dvc.id ='4' 
		 */
		//System.out.println("inside Action class --->");
		Map<String, Object> outputParams = new HashMap<String, Object>();
		 //conn = null;
		try (Connection conn = DatabaseHelper.getConnection()){
			
			//call for device update method
			getDevicePeripheral(outputParams,processMem, conn);
			
		}catch (Exception e) {
/*			try {
				conn.close();
			} catch (SQLException sqlExcp) {
				sqlExcp.printStackTrace();
			}*/
			throw new DynamicExecuterException(e.getMessage(), null, null);
		}
		
	    return outputParams;
	}
	
	private static void getDevicePeripheral(Map<String, Object> outputParams,ProcessMem processMem, Connection conn) throws SQLException{
		StringBuilder sbDevicePerphrlSelect = null;
		PreparedStatement psDevicePerphrlSelect = null;
		ResultSet rsDevicePerphrlSelect = null;
		ArrayList<Object> printerIP = new ArrayList<Object>();
		ArrayList<Object> drawerIP  = new ArrayList<Object>();
		HashMap<String, Object> hashMapDevicePerphrlObj = new HashMap<>();

		try{
			//build the device peripherals select prepared statement
			sbDevicePerphrlSelect = new StringBuilder(
					"select dvc.dUUID as dUUID, dvc.alias as alias, dvc.locationId as locationId, dvc.active as isActive, perphl.host printerIP,perphl.port as printerPort ,d.host drawerIP,d.port as drawerPort "
							+ "from	device_tbl dvc left join device_peripheral_tbl dvcprl ON dvc.id=dvcprl.deviceId and dvcprl.isDeleted = '0'"
							+ "left join peripheral_tbl perphl ON dvcprl.peripheralId = perphl.id and perphl.deviceCategory = 'Printer' "
							+ "left join peripheral_tbl d on dvcprl.peripheralId = d.id and d.deviceCategory = 'Drawer'"
							+ "	where dvc.id =?");
			psDevicePerphrlSelect = conn.prepareStatement(sbDevicePerphrlSelect.toString());
			psDevicePerphrlSelect.setString(1,	(String) processMem.get("deviceId"));
			rsDevicePerphrlSelect = psDevicePerphrlSelect.executeQuery();
			
			
			while(rsDevicePerphrlSelect.next()){
				if(rsDevicePerphrlSelect.isFirst()){
					hashMapDevicePerphrlObj.put("dUUID",rsDevicePerphrlSelect.getObject("dUUID"));
					hashMapDevicePerphrlObj.put("alias",rsDevicePerphrlSelect.getObject("alias"));
					hashMapDevicePerphrlObj.put("locationId",rsDevicePerphrlSelect.getObject("locationId"));
					hashMapDevicePerphrlObj.put("isActive",rsDevicePerphrlSelect.getObject("isActive"));
				}
				if(rsDevicePerphrlSelect.getObject("printerIP") !=null){
					if(rsDevicePerphrlSelect.getObject("printerPort") != null)
					{
						printerIP.add(rsDevicePerphrlSelect.getObject("printerIP")+":"+rsDevicePerphrlSelect.getObject("printerPort"));
					}
					else
					{
						printerIP.add(rsDevicePerphrlSelect.getObject("printerIP"));
					}
					
					hashMapDevicePerphrlObj.put("printerIP",printerIP);
				}
				
				if(rsDevicePerphrlSelect.getObject("drawerIP") !=null){
					if(rsDevicePerphrlSelect.getObject("drawerPort") != null)
					{
						drawerIP.add(rsDevicePerphrlSelect.getObject("drawerIP")+":"+rsDevicePerphrlSelect.getObject("drawerPort"));
					}
					else
					{
						drawerIP.add(rsDevicePerphrlSelect.getObject("drawerIP"));
					}
					
					hashMapDevicePerphrlObj.put("cashDrawerIP",drawerIP);
				}
				
			}
			outputParams.put("devicePeripheralObj",hashMapDevicePerphrlObj);
			rsDevicePerphrlSelect.close();
			psDevicePerphrlSelect.close();
		}catch (Exception e){
			if(rsDevicePerphrlSelect !=null)
				rsDevicePerphrlSelect.close();
			if(psDevicePerphrlSelect !=null)
				psDevicePerphrlSelect.close();
			throw new SQLException("Something went wrong, do not commit: "+e.getLocalizedMessage());
		}
	}
	

}