package dynamic.devicePeripheral.AddUpdateDevicePeripheral; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {

		Map<String, Object> outputParams = new HashMap<String, Object>();
		//Connection conn = null;
		String returnMsg ="";
		try (Connection conn = DatabaseHelper.getConnection()){
			//conn = DatabaseHelper.getConnection();
			//call for device update method
			returnMsg = updateDevice(processMem, conn);
			
			//System.out.println("sysout printerIP: "+(String)processMem.get("printerIP"));
			if(!"0.0.0.0".equalsIgnoreCase((String)processMem.get("printerIP")) && ("".equals(returnMsg) || "200".equals((returnMsg.split("-"))[0]))){
				//call the device peripheral add/update

				//returnMsg = addUpdateDevicePerphrl(processMem, conn);
				
				returnMsg = addUpdateDevicePerphrl(processMem,"Printer",(String)processMem.get("printerIP"),(String)processMem.get("printerType"),(String)processMem.get("printerModel"),
						(String)processMem.get("printerVersion"),(String)processMem.get("printerAlias"),(String)processMem.get("printerIsActive"),(String)processMem.get("printerIsDeleted"),
						conn);
				
				//ProcessMem processMem,String perphl,String perphlIPString,String perphlType,String perphlModel,String perphlVersion,String perphlAlias,Connection conn
			}
			
			//System.out.println("-------returnMsg   "+returnMsg);
			//System.out.println("-------cashDrawerIP"+processMem.get("cashDrawerIP"));
			
			if(!"0.0.0.0".equalsIgnoreCase((String)processMem.get("cashDrawerIP"))){
				//call the device peripheral add/update

				//returnMsg = addUpdateDevicePerphrl(processMem, conn);
				//System.out.println("doing cash drawer add");
				returnMsg = addUpdateDevicePerphrl(processMem,"Drawer",(String)processMem.get("cashDrawerIP"),(String)processMem.get("cashDrawerType"),(String)processMem.get("cashDrawerModel"),
						(String)processMem.get("cashDrawerVersion"),(String)processMem.get("cashDrawerAlias"),(String)processMem.get("cashDrawerIsActive"),(String)processMem.get("cashDrawerIsDeleted"),
						conn);
				
				//ProcessMem processMem,String perphl,String perphlIPString,String perphlType,String perphlModel,String perphlVersion,String perphlAlias,Connection conn
			}
			
			
			//System.out.println("returnMsg: "+returnMsg);
			outputParams.put("returnCode", (returnMsg.split("-"))[0]);
			outputParams.put("returnMsg", (returnMsg.split("-"))[1]);
		}catch (Exception e) {
/*			if(conn !=null)
				try {
					//conn.rollback();
					conn.close();
				} catch (SQLException sqlExp) {
					sqlExp.printStackTrace();
				}*/
			outputParams.put("returnCode", "503");
			outputParams.put("returnMsg", "Generic SQL exception");
			//throw new DynamicExecuterException(e.getMessage(), null, null);
		}	
	    return outputParams;
	}
	

	private static String updateDevice(ProcessMem processMem, Connection conn) {
		//StringBuilder sbDeviceUpdate = null;
		PreparedStatement psDeviceUpdate = null;
		String errorMsg = "";
		String errorCode="";
		try {
			//build the device update prepared statement
			
			String sDeviceUpdate = "update device_tbl set ";
			if(!("".equalsIgnoreCase((String) processMem.get("deviceActive"))))
				sDeviceUpdate = sDeviceUpdate+"active ='"+(String) processMem.get("deviceActive")+"',";				
			if(!("".equalsIgnoreCase((String) processMem.get("locationId"))))
					sDeviceUpdate = sDeviceUpdate+"locationId ='"+(String) processMem.get("locationId")+"',";			
			if(!("".equalsIgnoreCase((String) processMem.get("deviceAlias"))))
				sDeviceUpdate = sDeviceUpdate+"alias ='"+(String) processMem.get("deviceAlias")+"',";						
			if(("true".equalsIgnoreCase((String) processMem.get("resetDevice")))){
				sDeviceUpdate = sDeviceUpdate+"syncToken ='0', ";
				errorMsg= "Device details updated with sync token 0";
			}else
				errorMsg= "Device details updated";
			
				sDeviceUpdate = sDeviceUpdate +" lastModified= CURRENT_TIMESTAMP where id='"+(String) processMem.get("deviceId")+"'";
				errorCode="200";
			
			psDeviceUpdate = conn.prepareStatement(sDeviceUpdate);
			//System.out.println("sDeviceUpdate: "+sDeviceUpdate);
	
			psDeviceUpdate.executeUpdate();
			psDeviceUpdate.close();
			//conn = null;
			//errorMsg ="Reset Device Token Successful";
		}catch (Exception e){
			errorMsg ="Device Update Failed";
			errorCode="500";
			//conn.commit();
			e.printStackTrace();
			//conn.close();
			//conn = null;
			if(psDeviceUpdate !=null)
				try {
					psDeviceUpdate.close();
				} catch (SQLException sqlExp) {
					psDeviceUpdate = null;
				}

			//throw new SQLException("Something went wrong, do not commit: "+e.getLocalizedMessage());
		}finally{
			return errorCode+"-"+errorMsg;
		}
		
	}
	

	private static String addUpdateDevicePerphrl(ProcessMem processMem,String perphlCategory,String perphlIPString,String perphlType,String perphlModel,String perphlVersion,
			String perphlAlias,String perphlisActive,String perphlisDeleted,Connection conn){
		
		
		StringBuilder sbDevicePerphrlSelect = null;
		StringBuilder sbPeripheralInsert = null;
		StringBuilder sbDevicePerphrlInsert = null;
		StringBuilder sbPeriPheralMaxSelect = null;
		StringBuilder sbPeriPheralSelect = null;
		
		PreparedStatement psPeripheralInsert = null;
		PreparedStatement psDevicePerphrlSelect = null;
		PreparedStatement psDevicePerphrlInsert = null;
		PreparedStatement psPeriPheralMaxSelect = null;
		PreparedStatement psPeriPheralSelect = null;
		
		ResultSet rsDevicePerphrlSelect = null;
		ResultSet rsPeriPheralMaxSelect = null;
		ResultSet rsPeriPheralSelect = null;
		
		String errorMsg ="";
		String errorCode ="";
		String host = "";
		boolean doPeriphrlAdd = false;
		boolean doPeriphrlUpdate = false;
		boolean printerExists = false;
		//boolean usePrinterHostId = false;
		
		
		//String printerIPString = (String)(processMem.get("printerIP"));
		String perphlIP   = "";
		String perphlPort = "";
		
		if(perphlIPString.contains(":"))
		{
			String[] parts = perphlIPString.split(":");
			perphlIP  = parts[0]; // 004
			perphlPort = parts[1];
		}
		else
		{
			perphlIP = perphlIPString;
		}
		
		
		try{
			//build the select device_peripheral prepared statement
			sbDevicePerphrlSelect = //new StringBuilder("select dvcperphrl.deviceId dvcId,dvcperphrl.peripheralId periPhrlId, perphrl.host host, dvcperphrl.isDeleted isDeleted "
									new StringBuilder("select dvcperphrl.deviceId dvcId,dvcperphrl.peripheralId periPhrlId,concat(perphrl.host,IF(perphrl.port IS NULL,'',concat(':',perphrl.port))) as host, dvcperphrl.isDeleted isDeleted "
					+ "from peripheral_tbl perphrl, device_peripheral_tbl dvcperphrl "
					+ "where dvcperphrl.deviceid =? and perphrl.id = dvcperphrl.peripheralId ");
			sbDevicePerphrlSelect.append(" and perphrl.deviceCategory = '"+perphlCategory+"'");
			sbDevicePerphrlSelect.append("order by 2 desc");
			
			//System.out.println("sbDevicePerphrlSelect   "+sbDevicePerphrlSelect.toString());
			
			psDevicePerphrlSelect = conn
					.prepareStatement(sbDevicePerphrlSelect.toString());
			
			//System.out.println("sbDevicePerphrlSelect: "+sbDevicePerphrlSelect);
			psDevicePerphrlSelect.setString(1,	(String) processMem.get("deviceId"));
			//psDevicePerphrlSelect.setString(2,	(String) processMem.get("printerIP"));

			rsDevicePerphrlSelect = psDevicePerphrlSelect.executeQuery();
			
			//System.out.println("isBeforeFirst: "+rsDevicePerphrlSelect.isBeforeFirst());
			if(rsDevicePerphrlSelect.isBeforeFirst()){
				while(rsDevicePerphrlSelect.next()){
					//String device
					//String existingHost = (String) rsDevicePerphrlSelect.getObject("printerIP");
					String isDeleted = ""+rsDevicePerphrlSelect.getObject("isDeleted");
					host = ""+rsDevicePerphrlSelect.getString("host");
					
					if(host==null)
					{
						host = "";
					}
					
					if(!host.equalsIgnoreCase(perphlIPString) && !doPeriphrlAdd){
						doPeriphrlAdd = true;
						//break;
						
					}
					else if(host.equalsIgnoreCase(perphlIPString) && "0".equals(isDeleted) && !printerExists){
						//System.out.println("bbbbbinside else if: "+isDeleted);
						errorMsg =perphlCategory +" Exists for the Device";
						errorCode="501";
						printerExists = true;
						break;
					}
					else if(host.equalsIgnoreCase(perphlIPString) && "1".equals(isDeleted) && !doPeriphrlUpdate){
						printerExists = true;
						//System.out.println("cccccinside else if: ");
						doDvcPeriPhrlUpdate(conn,"= '"+(String) rsDevicePerphrlSelect.getObject("periPhrlId")+"'",perphlCategory
								,(String) processMem.get("deviceId"),0);

						doDvcPeriPhrlUpdate(conn,"<> '"+(String) rsDevicePerphrlSelect.getObject("periPhrlId")+"'",perphlCategory
								,(String) processMem.get("deviceId"),1);
						errorMsg ="Add/Update to Device and Peripheral Successful";
						errorCode="202";
						break;
						
					}				
				}				
			}else{
				doPeriphrlAdd = true;
			}
			psDevicePerphrlSelect.close();
			rsDevicePerphrlSelect.close();
			//System.out.println("**************  doPeriphrlAdd "+doPeriphrlAdd);
			//System.out.println("**************  printerExists "+printerExists);
			
			
			if(doPeriphrlAdd && !printerExists){
				////System.out.println("inside else: ");

					String sMaxNumber= "";
					
					if(perphlPort != null && !(perphlPort.equals("")))
					{
						sbPeriPheralSelect = new StringBuilder("select id from peripheral_tbl where host = ? and port = ? order by 1 desc");
					}
					else
					{
						sbPeriPheralSelect = new StringBuilder("select id from peripheral_tbl where host = ?  order by 1 desc");
					}
					
					psPeriPheralSelect = conn.prepareStatement(sbPeriPheralSelect.toString());	
						
					psPeriPheralSelect.setString(1, perphlIP);
					if(perphlPort != null && !(perphlPort.equals("")))
					{
						psPeriPheralSelect.setString(2, perphlPort);
					}
					
					
						
					//System.out.println("**************  printerIP "+perphlIP);
					//System.out.println("**************  printerPort "+perphlPort);
												
					
						
					//psPeriPheralSelect = conn.prepareStatement(sbPeriPheralSelect.toString());				
					
					
					//System.out.println(" Step 1");
					
					
					//psPeriPheralSelect.setString(1, (String) processMem.get("printerIP"));
					
					//psPeriPheralSelect.setString(1, printerIP);
					rsPeriPheralSelect = psPeriPheralSelect.executeQuery();
					
					
					if(rsPeriPheralSelect.isBeforeFirst()){
						
						//System.out.println(" Step xxxxx");
						while(rsPeriPheralSelect.next()){
							//System.out.println(" Step yyyyyy");
							sMaxNumber = (String) rsPeriPheralSelect.getObject(1);
							//usePrinterHostId = true;
							break;
							//sMaxNumber = ""+(Integer.parseInt(sMaxNumber)+1);
						}
						psPeriPheralSelect.close();
						rsPeriPheralSelect.close();
					}
					else{
						//System.out.println(" Step zzzzzz");
						sbPeriPheralMaxSelect = new StringBuilder("select MAX(CAST(id as SIGNED)) max from peripheral_tbl");
						psPeriPheralMaxSelect = conn.prepareStatement(sbPeriPheralMaxSelect.toString());				
						
						rsPeriPheralMaxSelect = psPeriPheralMaxSelect.executeQuery();
						if(rsPeriPheralMaxSelect.isBeforeFirst()){
							while(rsPeriPheralMaxSelect.next()){
								sMaxNumber = rsPeriPheralMaxSelect.getObject(1)+"";
								//System.out.println("sMaxNumber: "+sMaxNumber);
								if(sMaxNumber != null && !("null".equalsIgnoreCase(sMaxNumber)) && !("".equals(sMaxNumber)))
									sMaxNumber = ""+(Integer.parseInt(sMaxNumber)+1);
								else
									sMaxNumber = "1";
							}
						}else
							sMaxNumber = "1";

						psPeriPheralMaxSelect.close();
						rsPeriPheralMaxSelect.close();

						//build the peripheral insert prepared statement
						sbPeripheralInsert = new StringBuilder(
							"insert into peripheral_tbl  values (?,'"+perphlCategory+"',?,?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?)");
						psPeripheralInsert = conn.prepareStatement(sbPeripheralInsert.toString());
						System.out.println("sbPeripheralInsert: "+sbPeripheralInsert);
						
						psPeripheralInsert.setString(1,sMaxNumber);
						//psPeripheralInsert.setString(2,	(String) processMem.get("printerType"));
						//psPeripheralInsert.setString(3,	(String) processMem.get("printerModel"));
						//psPeripheralInsert.setString(4,	(String) processMem.get("printerVersion"));
						//psPeripheralInsert.setString(5,	(String) processMem.get("printerAlias"));
						psPeripheralInsert.setString(2,	(String) perphlType);
						psPeripheralInsert.setString(3,	(String) perphlModel);
						psPeripheralInsert.setString(4,	(String) perphlVersion);
						psPeripheralInsert.setString(5,	(String) perphlAlias);
						psPeripheralInsert.setString(6,	perphlIP);
						
						if(perphlPort !=null && !(perphlPort.equals("")))
							{
								psPeripheralInsert.setString(7,	perphlPort);
							}
							else
							{
								psPeripheralInsert.setNull(7,java.sql.Types.INTEGER);
							}
						
						//psPeripheralInsert.setString(8,	(String) processMem.get("printerIsActive"));
						//psPeripheralInsert.setString(9,	(String) processMem.get("printerIsDeleted"));
						
						psPeripheralInsert.setString(8, perphlisActive);
						psPeripheralInsert.setString(9,	perphlisDeleted);		

						
						psPeripheralInsert.executeUpdate();
						
						psPeripheralInsert.close();
					}
					//build the device peripheral insert statement
					sbDevicePerphrlInsert = new StringBuilder("insert into device_peripheral_tbl  select id,'"+sMaxNumber+"', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 0 "
							+ "from device_tbl where id=?");
					psDevicePerphrlInsert = conn.prepareStatement(sbDevicePerphrlInsert.toString());
					psDevicePerphrlInsert.setString(1,(String) processMem.get("deviceId"));
					////System.out.println("sbDevicePerphrlInsert: "+sbDevicePerphrlInsert);
					psDevicePerphrlInsert.executeUpdate();									
					psDevicePerphrlInsert.close();			
					//update the previous printer(if any) to in active
					doDvcPeriPhrlUpdate(conn,"<> '"+sMaxNumber+"'",perphlCategory,(String) processMem.get("deviceId"), 1);
					errorMsg ="Add/Update to Device and Peripheral Successful";
					errorCode="202";
			}
		}catch(Exception e){
			////System.out.println("error : "+e.getLocalizedMessage());
			errorMsg ="Device and Printer Add/Update Failed";
			errorCode="502";
			e.printStackTrace();
			if (psPeripheralInsert != null)
				psPeripheralInsert.close();
			if (psDevicePerphrlInsert != null)
				psDevicePerphrlInsert.close();
			if(rsPeriPheralMaxSelect !=null)
				rsPeriPheralMaxSelect.close();
			if(psPeriPheralMaxSelect !=null)
				psPeriPheralMaxSelect.close();
			if(rsPeriPheralSelect !=null)
				rsPeriPheralSelect.close();
			if(psPeriPheralSelect != null)
				psPeriPheralSelect.close();
			if(rsDevicePerphrlSelect != null)
				rsDevicePerphrlSelect.close();
			if(psDevicePerphrlSelect != null)
				psDevicePerphrlSelect.close();	
		}finally{
			return errorCode+"-"+errorMsg;
		}
	}
	
	private static void doDvcPeriPhrlUpdate (Connection conn, String periPhrlId, String perphlCategory, String deviceId, int flag) throws SQLException{
		
		StringBuilder sbDvcPeriPhrlUpdate = new StringBuilder(
				" update device_peripheral_tbl set isDeleted = ?, lastModified=CURRENT_TIMESTAMP where deviceId= ? and peripheralId in (select "
				+ " id from peripheral_tbl where deviceCategory = '"+perphlCategory+"')and peripheralId "+periPhrlId);
		PreparedStatement psDvcPeriPhrlUpdate = null;
		try {
			psDvcPeriPhrlUpdate = conn.prepareStatement(sbDvcPeriPhrlUpdate.toString());
			psDvcPeriPhrlUpdate.setInt(1,flag);
			psDvcPeriPhrlUpdate.setString(2,deviceId);
			//psDvcPeriPhrlUpdate.setString(3, periPhrlId);
			psDvcPeriPhrlUpdate.executeUpdate();
			
			psDvcPeriPhrlUpdate.close();
			//conn.commit();
		} catch (SQLException sqlExp) {
			// TODO Auto-generated catch block
			if(psDvcPeriPhrlUpdate !=null){
				psDvcPeriPhrlUpdate.close();
				//conn.rollback();
				/*if(conn !=null){
					conn.close();
					conn = null;
				}*/
			}
			throw new SQLException("Something went wrong, do not commit"+sqlExp.getLocalizedMessage());
		}
			

		
	}
	

}