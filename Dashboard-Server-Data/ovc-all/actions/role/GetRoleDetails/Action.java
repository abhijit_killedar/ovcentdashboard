package dynamic.role.GetRoleDetails; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {
		
		
		
		//System.out.println("inside Action class --->");
		Map<String, Object> outputParams = new HashMap<String, Object>();
		 //conn = null;
		try (Connection conn = DatabaseHelper.getConnection()){
			
			//call for device update method
			getRole(outputParams,processMem, conn);
			
		}catch (Exception e) {
/*			try {
				conn.close();
			} catch (SQLException sqlExcp) {
				sqlExcp.printStackTrace();
			}*/
			throw new DynamicExecuterException(e.getMessage(), null, null);
		}
		
	    return outputParams;
	}
	
	private static void getRole(Map<String, Object> outputParams,ProcessMem processMem, Connection conn) throws SQLException{
		StringBuilder sbRoleSelect = null;
		PreparedStatement psRoleSelect = null;
		ResultSet rsRoleSelect = null;
		ArrayList<Object> printerIP = new ArrayList<Object>();
		HashMap<String, Object> hashMapRoleObj = new HashMap<>();

		try{
			//build the role select prepared statement
			sbRoleSelect = new StringBuilder(
					"select id  as ID, name as NAME "
							+ "from	role_tbl "
							+ "where id =?");
			psRoleSelect = conn.prepareStatement(sbRoleSelect.toString());
			psRoleSelect.setString(1,	(String) processMem.get("roleId"));
			rsRoleSelect = psRoleSelect.executeQuery();
			
			
			while(rsRoleSelect.next()){
				if(rsRoleSelect.isFirst()){
					hashMapRoleObj.put("id",rsRoleSelect.getObject("ID"));
					hashMapRoleObj.put("name",rsRoleSelect.getObject("NAME"));
				}
				
			}
			outputParams.put("roleObj",hashMapRoleObj);
			rsRoleSelect.close();
			psRoleSelect.close();
		}catch (Exception e){
			if(rsRoleSelect !=null)
				rsRoleSelect.close();
			if(psRoleSelect !=null)
				psRoleSelect.close();
			throw new SQLException("Something went wrong, do not commit: "+e.getLocalizedMessage());
		}
	}
	

}