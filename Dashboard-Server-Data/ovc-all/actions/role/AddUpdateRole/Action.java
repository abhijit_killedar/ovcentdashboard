package dynamic.role.AddUpdateRole; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {

		Map<String, Object> outputParams = new HashMap<String, Object>();
		//Connection conn = null;
		String returnMsg ="";
		try (Connection conn = DatabaseHelper.getConnection()){
			//conn = DatabaseHelper.getConnection();
			//call for device update method
			if(!processMem.containsKey("roleId"))
			{
				outputParams.put("returnCode", "201");
				outputParams.put("returnMsg", "Invalid role id.");
			}
			else if(!processMem.containsKey("roleName"))
			{
				outputParams.put("returnCode", "202"); 
				outputParams.put("returnMsg", "Invalid role name.");
			}
			else if(processMem.get("roleName") == null || (processMem.get("roleName") != null && ((String) processMem.get("roleName")).equals("")))
			{
				outputParams.put("returnCode", "201");
				outputParams.put("returnMsg", "Invalid role id.");
			}
			else if(processMem.get("roleId") == null || (processMem.get("roleId") != null && ((String) processMem.get("roleId")).equals("")))
			{
				outputParams.put("returnCode", "202");
				outputParams.put("returnMsg", "Invalid role name.");
			}
			else 
			{
				//call the location add/update

				returnMsg = addUpdateRole(processMem, conn);

				outputParams.put("returnCode", (returnMsg.split("-"))[0]);
				outputParams.put("returnMsg", (returnMsg.split("-"))[1]);
			}
		}catch (Exception e) {

			outputParams.put("returnCode", "402");
			outputParams.put("returnMsg", "Generic SQL exception"+e.getLocalizedMessage());
		}	
	    return outputParams;
	}
	

	
	private static String addUpdateRole(ProcessMem processMem,Connection conn){
		
		StringBuilder sbRoleSelect = null;
		StringBuilder sbRoleInsert = null;

		PreparedStatement psRoleSelect = null;
		PreparedStatement psRoleInsert = null;

		
		ResultSet rsRoleSelect = null;

		
		String errorMsg ="";
		String errorCode ="";
		String host = "";
		boolean doRoleAdd = false;
		boolean doRoleUpdate = false;
		
		try{
			//build the select role prepared statement
			sbRoleSelect =new StringBuilder(
					"select id  as ID, name as NAME "
							+ "from	role_tbl "
							+ "where id =?");
			
			psRoleSelect = conn
					.prepareStatement(sbRoleSelect.toString());
			
			//System.out.println("sbRoleSelect: "+sbRoleSelect);
			psRoleSelect.setString(1,	(String) processMem.get("roleId"));
			//psRoleSelect.setString(2,	(String) processMem.get("printerIP"));

			rsRoleSelect = psRoleSelect.executeQuery();
			
			//System.out.println("isBeforeFirst: "+rsRoleSelect.isBeforeFirst());
			if(rsRoleSelect.isBeforeFirst())
			{
				while(rsRoleSelect.next())
				{
					StringBuilder sbRoleUpdate = new StringBuilder(
							" update role_tbl set name = ? where id= ? ");
					PreparedStatement psRoleUpdate = null;
					try {
						psRoleUpdate = conn.prepareStatement(sbRoleUpdate.toString());
						psRoleUpdate.setString(1,	(String) processMem.get("roleName"));
						psRoleUpdate.setString(2,	(String) processMem.get("roleId"));
						psRoleUpdate.executeUpdate();
						
						psRoleUpdate.close();
						//conn.commit();
					} catch (SQLException sqlExp) {
						// TODO Auto-generated catch block
						if(psRoleUpdate !=null){
							psRoleUpdate.close();
							
						}
						throw new SQLException("Something went wrong, do not commit"+sqlExp.getLocalizedMessage());
					}
						errorMsg ="Role updated";
						errorCode="202";
						break;
			
				}				
			}
			else
			{
				doRoleAdd = true;
			}
			psRoleSelect.close();
			rsRoleSelect.close();
			
			if(doRoleAdd)
			{

				sbRoleInsert = new StringBuilder(
					"insert into role_tbl  values (?,?)");
				psRoleInsert = conn.prepareStatement(sbRoleInsert.toString());

				
				psRoleInsert.setString(1,	(String) processMem.get("roleId"));
				psRoleInsert.setString(2,	(String) processMem.get("roleName"));		
				
				psRoleInsert.executeUpdate();
				
				psRoleInsert.close();


				errorMsg ="Role Created.";
				errorCode="201";
			}
		}catch(Exception e)
		{
			errorMsg ="Generic SQL exception "+ e.getLocalizedMessage();
			errorCode="450";
			e.printStackTrace();

			if(rsRoleSelect != null)
				rsRoleSelect.close();
			if(psRoleSelect != null)
				psRoleSelect.close();	
		}finally
		{
			return errorCode+"-"+errorMsg;
		}
	}
	

}