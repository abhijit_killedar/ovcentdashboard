package dynamic.location.AddUpdateLocation; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.exception.DynamicExecuterException;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.util.db.DatabaseHelper;

public class Action implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) throws DynamicExecuterException {

		Map<String, Object> outputParams = new HashMap<String, Object>();
		//Connection conn = null;
		String returnMsg ="";
		try (Connection conn = DatabaseHelper.getConnection())
		{

			//Validation to check input params are valid and autofill other non mandatory fields.
			if(!(processMem.containsKey("locationId")))
			{
				outputParams.put("returnCode", "401");
				outputParams.put("returnMsg","Invalid location id.");
			}
			else if(processMem.get("locationId") == null || (processMem.get("locationId") != null && ((String) processMem.get("locationId")).equals("")))
			{
				outputParams.put("returnCode", "401");
				outputParams.put("returnMsg", "Invalid location id.");
			}
			//else if(!(processMem.containsKey("locationName")))
			//{
			//	outputParams.put("returnCode", "402");
			//	outputParams.put("returnMsg", "Invalid location name.");
			//}
			//else if(processMem.get("locationName") == null || (processMem.get("locationName") != null && ((String) processMem.get("locationName")).equals("")))
			//{
			//	outputParams.put("returnCode", "402");
			//	outputParams.put("returnMsg", "Invalid location name.");
			//}
			else
			{
				if(((String) processMem.get("locationType")).equals(""))
				{
					processMem.put("locationType", "Store");
				}
				
				//call the location add/update

				returnMsg = addUpdateLocandGroup(processMem, conn);
				outputParams.put("returnCode", (returnMsg.split("-"))[0]);
				outputParams.put("returnMsg", (returnMsg.split("-"))[1]);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			outputParams.put("returnCode", "450");
			outputParams.put("returnMsg", "Generic SQL exception"+e.getLocalizedMessage());
			//throw new DynamicExecuterException(e.getMessage(), null, null);
		}	
	    return outputParams;
	}
	

	/****
	 * location_tbl  stores location id information and one more entry needs to be location_and_group_tbl
	 * 
	 ****/
	private static String addUpdateLocandGroup(ProcessMem processMem,Connection conn){
		
		StringBuilder sbLocandGroupSelect 		= null;
		StringBuilder sbLocandGroupInsert 		= null;
		StringBuilder sbLocationInsert 		= null;
		
		PreparedStatement psLocandGroupSelect 		= null;
		PreparedStatement psLocandGroupInsert 		= null;
		PreparedStatement psLocationInsert 	= null;
		
		
		ResultSet rsLocandGroupSelect = null;
		
		String errorMsg ="";
		String errorCode ="";
		
		boolean doLocandGroupAdd = false;
		boolean doLocandGroupUpdate = false;
		
		try
		{
			//Step1: Check to see whether location exists
			
			//build the select location_and_group_tbl prepared statement
			sbLocandGroupSelect = new StringBuilder("select id "
					+ "from location_and_group_tbl "
					+ "where id =?");
			
			psLocandGroupSelect = conn.prepareStatement(sbLocandGroupSelect.toString());
			
			psLocandGroupSelect.setString(1,	(String) processMem.get("locationId"));
			
			rsLocandGroupSelect = psLocandGroupSelect.executeQuery();

			if(rsLocandGroupSelect.isBeforeFirst())
			{
				while(rsLocandGroupSelect.next())
				{
					//Add code to update location_and_group_tbl
					//Step2: If location_and_grp_tbl has entry then run update for both location_and_grp_tbl and location_tbl
					doLocandGroupUpdate(conn, (String) processMem.get("locationId"), (String) processMem.get("locationName"),processMem);
					errorMsg ="Location updated.";
					errorCode="202";
					break;		
			
				}				
			}
			else
			{
				
				doLocandGroupAdd = true;
				//Step3: If location does not exist then insert new location_tbl and location_and_group_tbl record.
				
				//build the location_tbl insert prepared statement
				sbLocandGroupInsert = new StringBuilder("insert into location_and_group_tbl  values (?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?,?)");
				
				psLocandGroupInsert = conn.prepareStatement(sbLocandGroupInsert.toString());
				//System.out.println("sbLocandGroupInsert: "+sbLocandGroupInsert);
				
				psLocandGroupInsert.setString(1,	(String) processMem.get("locationId"));		
				psLocandGroupInsert.setString(2,	(String) processMem.get("locationId"));
				psLocandGroupInsert.setString(3,	null);
				psLocandGroupInsert.setString(4,	null);
				psLocandGroupInsert.setInt(5,		0);
				psLocandGroupInsert.setString(6,	null);

				
				psLocandGroupInsert.executeUpdate();
				
				psLocandGroupInsert.close();
				
				
				//build the location_tbl insert prepared statement
				sbLocationInsert = new StringBuilder("insert into location_tbl  values (?,?,?,?,?,?,?,?,?,?,?)");
				
				psLocationInsert = conn.prepareStatement(sbLocationInsert.toString());
				//System.out.println("sbLocationInsert: "+sbLocationInsert);
				
				
				psLocationInsert.setString(1,	(String) processMem.get("locationId"));		
				psLocationInsert.setString(2,	(String) processMem.get("locationType"));
				psLocationInsert.setString(3,	(String) processMem.get("locationName"));
				psLocationInsert.setString(4,	(String) processMem.get("streetName"));
				psLocationInsert.setString(5,	(String) processMem.get("streetNumber"));
				psLocationInsert.setString(6,	(String) processMem.get("postalCode"));
				psLocationInsert.setString(7,	(String) processMem.get("town"));
				psLocationInsert.setString(8,	(String) processMem.get("country"));	
				psLocationInsert.setString(9,	(String) processMem.get("phoneNumber"));
				psLocationInsert.setString(10,	(String) processMem.get("faxNumber"));
				psLocationInsert.setInt(11,		 0);


				
				psLocationInsert.executeUpdate();
				
				psLocationInsert.close();
				errorMsg ="Location created.";
				errorCode="201";

			}
			psLocandGroupSelect.close();
			rsLocandGroupSelect.close();
			
			//Step4: Return 
		
			

		}
		catch(Exception e)
		{

			errorMsg ="Generic SQL exception"+e.getLocalizedMessage();
			errorCode="450";

			if (psLocandGroupInsert != null)
				psLocandGroupInsert.close();

			if(rsLocandGroupSelect != null)
				rsLocandGroupSelect.close();
			if(psLocandGroupSelect != null)
				psLocandGroupSelect.close();	
		}
		finally
		{
			return errorCode+"-"+errorMsg;
		}
	}
	
	private static void doLocandGroupUpdate (Connection conn, String locationId,String locationName, ProcessMem processMem) throws SQLException
	{
		
		StringBuilder sbLocandGroupUpdate = new StringBuilder(
				" update location_and_group_tbl set name = ?, lastModified=CURRENT_TIMESTAMP where id= ? ");
		
		StringBuilder sbLocationUpdate = new StringBuilder(
				"   update location_tbl set displayName =  ?,streetName = ? ,streetNumber = ?,postalCode = ?,town = ?,"
				+ " country = ?,phonenumber = ?,faxNumber = ? "
				+ " where id= ?  ");
		
		PreparedStatement psLocandGroupUpdate = null;
		PreparedStatement psLocationUpdate = null;
		try 
		{
			psLocandGroupUpdate = conn.prepareStatement(sbLocandGroupUpdate.toString());
			
			psLocandGroupUpdate.setString(1,locationId);
			psLocandGroupUpdate.setString(2,locationId);
			
			psLocandGroupUpdate.executeUpdate();
			
			psLocandGroupUpdate.close();
			
			
			psLocationUpdate = conn.prepareStatement(sbLocationUpdate.toString());
			
			psLocationUpdate.setString(1,locationName);
			psLocationUpdate.setString(2,	(String) processMem.get("streetName"));
			psLocationUpdate.setString(3,	(String) processMem.get("streetNumber"));
			psLocationUpdate.setString(4,	(String) processMem.get("postalCode"));
			psLocationUpdate.setString(5,	(String) processMem.get("town"));
			psLocationUpdate.setString(6,	(String) processMem.get("country"));	
			psLocationUpdate.setString(7,	(String) processMem.get("phoneNumber"));
			psLocationUpdate.setString(8,	(String) processMem.get("faxNumber"));
			
			psLocationUpdate.setString(9,locationId);
			
			
			psLocationUpdate.executeUpdate();
			
			psLocationUpdate.close();
			//conn.commit();
		} catch (SQLException sqlExp) 
		{
			// TODO Auto-generated catch block
			if(psLocandGroupUpdate !=null){
				psLocandGroupUpdate.close();

			}
			if(psLocationUpdate !=null){
				psLocationUpdate.close();

			}
			throw new SQLException("Something went wrong, do not commit"+sqlExp.getLocalizedMessage());
		}
	}
	

}