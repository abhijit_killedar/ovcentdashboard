package dynamic.devicePeripheral.GetDevicePeripheral;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Start Connector--->");
		
		//Map<String, Object> outputParams = new HashMap<String, Object>();		
		try {
	        String deviceId = (String) inputParams.get("deviceId");
	        processMem.put("deviceId", deviceId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processMem;
	}


}
