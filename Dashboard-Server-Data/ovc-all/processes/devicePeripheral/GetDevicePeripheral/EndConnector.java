package dynamic.devicePeripheral.GetDevicePeripheral;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;

public class EndConnector implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		Map<String, Object> outputParams = new HashMap<String, Object>();
		
		//System.out.println("inside End Connector--->");
		outputParams.put("devicePeripheralObj", inputParams.get("devicePeripheralObj"));		
		
		return outputParams;
	}

}