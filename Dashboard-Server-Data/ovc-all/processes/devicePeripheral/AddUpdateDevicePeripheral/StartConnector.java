package dynamic.devicePeripheral.AddUpdateDevicePeripheral;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Start Connector--->");
		
		//Map<String, Object> outputParams = new HashMap<String, Object>();		
		try {
	        String deviceType = (String) inputParams.get("deviceType");
	        String deviceId = (String) inputParams.get("deviceId");
			String deviceAlias = (String) inputParams.get("deviceAlias");
			String locationId= (String) inputParams.get("locationId");
			String printerIP = (String) inputParams.get("printerIP");		
			String printerType = (String) inputParams.get("printerType");
			String printerModel = (String) inputParams.get("printerModel");
			String printerVersion = (String) inputParams.get("printerVersion");
			String printerAlias = (String) inputParams.get("printerAlias");
			String printerIsActive = (String)inputParams.get("printerIsActive");
			String printerIsDeleted = (String)inputParams.get("printerIsDeleted");
			String deviceActive = (String)inputParams.get("deviceActive");
			String resetDevice = (String) inputParams.get("resetDevice");

			processMem.put("deviceType", deviceType);
			processMem.put("deviceId", deviceId);
			processMem.put("deviceAlias", deviceAlias);
			processMem.put("locationId", locationId);
			
			if(printerIP == null || "null".equalsIgnoreCase(printerIP) || "".equalsIgnoreCase(printerIP))
				processMem.put("printerIP", "0.0.0.0");
			else
				processMem.put("printerIP", printerIP);
			
			processMem.put("printerType", printerType);
			processMem.put("printerModel", printerModel);
			processMem.put("printerVersion", printerVersion);
			processMem.put("printerAlias", printerAlias);
			processMem.put("printerIsActive", printerIsActive);
			processMem.put("printerIsDeleted", printerIsDeleted);
			processMem.put("deviceActive", deviceActive);
			processMem.put("resetDevice", resetDevice);
			
			
			String cashDrawerIP = (String) inputParams.get("cashDrawerIP");		
			String cashDrawerType = (String) inputParams.get("cashDrawerType");
			String cashDrawerModel = (String) inputParams.get("cashDrawerModel");
			String cashDrawerVersion = (String) inputParams.get("cashDrawerVersion");
			String cashDrawerAlias = (String) inputParams.get("cashDrawerAlias");
			String cashDrawerIsActive = (String)inputParams.get("cashDrawerIsActive");
			String cashDrawerIsDeleted = (String)inputParams.get("cashDrawerIsDeleted");


			
			if(cashDrawerIP == null || "null".equalsIgnoreCase(cashDrawerIP) || "".equalsIgnoreCase(cashDrawerIP))
				processMem.put("cashDrawerIP", "0.0.0.0");
			else
				processMem.put("cashDrawerIP", cashDrawerIP);
			
			processMem.put("cashDrawerType", cashDrawerType);
			processMem.put("cashDrawerModel", cashDrawerModel);
			processMem.put("cashDrawerVersion", cashDrawerVersion);
			processMem.put("cashDrawerAlias", cashDrawerAlias);
			processMem.put("cashDrawerIsActive", cashDrawerIsActive);
			processMem.put("cashDrawerIsDeleted", cashDrawerIsDeleted);

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processMem;
	}


}
