package dynamic.user.GetUsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Start Connector--->");
		
		Map<String, Object> outputParams = new HashMap<String, Object>();		
		try {
			
			String userId		 	= (String) inputParams.get("userId");
	        String userName		 	= (String) inputParams.get("userName");
	        String roleId		    = (String) inputParams.get("roleId");
	        String locationId 		= (String) inputParams.get("locationId");
	        String active	 		= (String) inputParams.get("active");

	        
	        outputParams.put("userId", userId);
	        outputParams.put("userName", userName);
	        outputParams.put("roleId", roleId);
	        outputParams.put("locationId", locationId);
	        outputParams.put("active", active);
	       


		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputParams;
	}


}
