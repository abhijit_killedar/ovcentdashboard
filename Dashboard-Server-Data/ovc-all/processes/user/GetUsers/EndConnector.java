package dynamic.user.GetUsers;

import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;

public class EndConnector implements DynamicClass 
{

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) 
	{
		Map<String, Object> outputParams = new HashMap<String, Object>();
		//System.out.println("inside End Connector--->");
		outputParams.put("usersList", inputParams.get("usersList"));
		//outputParams.put("returnMsg", inputParams.get("returnMsg"));
		return outputParams;
	}
}