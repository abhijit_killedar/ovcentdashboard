package dynamic.user.AddUpdateUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Start Connector--->");
		
		Map<String, Object> outputParams = new HashMap<String, Object>();		
		try {
			ArrayList<String> rolesList =new ArrayList<>();
			
	        String userId = (String) inputParams.get("userId");
	        String userName = (String) inputParams.get("userName");
	        String firstName = (String) inputParams.get("firstName");
	        String lastName = (String) inputParams.get("lastName");
			String email = (String) inputParams.get("email");
			String active = (String) inputParams.get("active");

			
			rolesList= (ArrayList<String>)  inputParams.get("rolesList");
			




			outputParams.put("userId", userId);
			outputParams.put("userName", userName);
			outputParams.put("firstName", firstName);
			outputParams.put("lastName", lastName);
			outputParams.put("email", email);
			outputParams.put("rolesList", rolesList);
			outputParams.put("active", active);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputParams;
	}


}
