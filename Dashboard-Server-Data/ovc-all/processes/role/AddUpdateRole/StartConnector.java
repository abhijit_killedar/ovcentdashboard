package dynamic.role.AddUpdateRole;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		

		//Map<String, Object> outputParams = new HashMap<String, Object>();		
		try {
	        String roleId = (String) inputParams.get("roleId");
	        String roleName = (String) inputParams.get("roleName");

			processMem.put("roleId", roleId);
			processMem.put("roleName", roleName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processMem;
	}


}
