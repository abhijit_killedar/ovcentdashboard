package dynamic.location.AddUpdateLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Start Connector--->");
		
		//Map<String, Object> outputParams = new HashMap<String, Object>();		
		try {
			
	        String locationId = (String) inputParams.get("locationId");
	        String locationType = (String) inputParams.get("locationType");
	        String locationName = (String) inputParams.get("locationName");
			String streetName = (String) inputParams.get("streetName");
			String streetNumber= (String) inputParams.get("locationId");
			String postalCode = (String) inputParams.get("postalCode");		
			String town = (String) inputParams.get("town");
			String country = (String) inputParams.get("country");
			String phoneNumber = (String) inputParams.get("phoneNumber");
			String faxNumber = (String) inputParams.get("faxNumber");


			processMem.put("locationId", locationId);
			processMem.put("locationType", locationType);
			processMem.put("locationName", locationName);
			processMem.put("streetName", streetName);
			processMem.put("streetNumber", streetNumber);
			processMem.put("postalCode", postalCode);
			processMem.put("town", town);
			processMem.put("country", country);
			processMem.put("phoneNumber", phoneNumber);
			processMem.put("faxNumber", faxNumber);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return processMem;
	}


}
