package dynamic.search.ObjectSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;

public class GetEntities implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Get Entity--->");
		Map<String, Object> outputParams = new HashMap<String, Object>();
		//Map<String, Object> params = new HashMap<String, Object>();
		
		try {	
			HashMap<String, String> entityMap = (HashMap<String, String>) processMem.get("entityMapping");
			HashMap<String, String> searchOperMap = (HashMap<String, String>) processMem.get("searchOpers");
			ArrayList<String> searchParams = (ArrayList<String>) processMem.get("searchParams");
			ArrayList<String> searchVals = (ArrayList<String>) processMem.get("searchVals");
			ArrayList<String> searchOperators = (ArrayList<String>) processMem.get("searchOperators");
			ArrayList<String> searchType = (ArrayList<String>) processMem.get("searchType");
			

			String entityType = (String)(processMem.get("entityType"));
			
			outputParams.put("ovcObjType", entityMap.get(entityType));
			outputParams.put("whereClause",buildWhereClause(entityMap.get(entityType),searchOperMap,
					searchParams, searchVals,searchOperators,searchType));
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return outputParams;
	}

	private String buildWhereClause(String entity, HashMap<String, String> searchOperMap,
			ArrayList<String> searchParams,	ArrayList<String> searchVals, ArrayList<String> searchOperators, ArrayList<String> searchType){

		String whereClause = ""; 
				//"not("+entity+".id is null)";
		System.out.println("before whereClause :"+whereClause);
		for(int i=0; i < searchParams.size(); i++){
			if(searchOperators.get(i).equalsIgnoreCase("LIKE")){
				if(i==0)
					whereClause = whereClause + " upper("+entity+"."+searchParams.get(i)+") "
						+ ""+searchOperMap.get(searchOperators.get(i))+"'%"+searchVals.get(i).toUpperCase()+"%'";
				else
					whereClause = whereClause + " "+searchType.get(0)+" upper("+entity+"."+searchParams.get(i)+") "
							+ ""+searchOperMap.get(searchOperators.get(i))+"'%"+searchVals.get(i).toUpperCase()+"%'";
			}
			else{
				if(i==0)
					whereClause = whereClause +" "+entity+"."+searchParams.get(i)
					+ " "+searchOperMap.get(searchOperators.get(i))+" '"+searchVals.get(i).toUpperCase()+"'";
				else
					whereClause = whereClause +" "+searchType.get(0)+" "+entity+"."+searchParams.get(i)
						+ " "+searchOperMap.get(searchOperators.get(i))+" '"+searchVals.get(i).toUpperCase()+"'";
			}
		}
		System.out.println("whereClause :"+whereClause);
		return whereClause;
	}
}