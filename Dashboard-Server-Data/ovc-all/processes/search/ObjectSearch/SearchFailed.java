package dynamic.search.ObjectSearch;

import java.util.HashMap;
import java.util.Map;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;

public class SearchFailed implements DynamicClass {

	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		Map<String, Object> outputParams = new HashMap<>();
		outputParams.put("error", processMem.get("message"));
		
		return outputParams;
	}

}