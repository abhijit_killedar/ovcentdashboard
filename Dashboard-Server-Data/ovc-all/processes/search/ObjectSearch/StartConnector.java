package dynamic.search.ObjectSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.oneview.server.process.ProcessMem;
import com.oneview.server.process.executer.DynamicClass;
import com.oneview.server.process.util.ConfigManager;

public class StartConnector implements DynamicClass {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> execute(Map<String, Object> inputParams, ProcessMem processMem) {
		
		//System.out.println("inside Start Connector--->");
		
		Map<String, Object> outputParams = new HashMap<String, Object>();
		//Map<String, Object> params = new HashMap<String, Object>();
		
		HashMap<String, String> entityMap = new HashMap<String, String>();
		HashMap<String, String> searchOperMap = new HashMap<String, String>();
		ArrayList<String> searchParams =new ArrayList<>();
		ArrayList<String>  searchVals = new ArrayList<>();
		ArrayList<String>  searchOperators = new ArrayList<>();
		ArrayList<String>  searchType = new ArrayList<>();

		
		try {
			
			//read the entity type passed to the gateway
			processMem.put("entityType",inputParams.get("entityType"));
			
			//read the entity mapping JSON
			JSONObject entityMappingConfig = ConfigManager.getConfigObject("entityObjectMapping.ovccfg", "grp-all");
			entityMap.put("Device", entityMappingConfig.getString("Device"));
			entityMap.put("Location", entityMappingConfig.getString("Location"));
			entityMap.put("Peripheral", entityMappingConfig.getString("Peripheral"));	
			entityMap.put("Role", entityMappingConfig.getString("Role"));	
			processMem.put("entityMapping", entityMap);
			
			//read the search operation JSON
			JSONObject searchOperConfig = ConfigManager.getConfigObject("searchOper.ovccfg", "grp-all");
			searchOperMap.put("AND", searchOperConfig.getString("AND"));
			searchOperMap.put("OR", searchOperConfig.getString("OR"));
			searchOperMap.put("LIKE", searchOperConfig.getString("LIKE"));
			searchOperMap.put("EQUALS", searchOperConfig.getString("EQUALS"));
			searchOperMap.put("NOTEQUALS", searchOperConfig.getString("NOTEQUALS"));
			processMem.put("searchOpers", searchOperMap);

			//Populate the search params from input to a list 			
			searchParams = (ArrayList<String>)  inputParams.get("searchParams");
			processMem.put("searchParams", searchParams);
			
			//Populate the search values from input to a list
			searchVals = (ArrayList<String>)  inputParams.get("searchValues");		
			processMem.put("searchVals", searchVals);
			
			//Populate the search operators from input to a list
			searchOperators = (ArrayList<String>)  inputParams.get("searchOperators");		
			processMem.put("searchOperators", searchOperators);
			
			//Populate the search type from input to a list
			searchType = (ArrayList<String>)  inputParams.get("searchType");		
			processMem.put("searchType",searchType);
			
			if(searchParams != null && searchVals.size() > 0 
					&& searchVals != null && searchParams.size() > 0
					&& searchOperators != null && searchOperators.size() > 0
					&& searchType != null && searchType.size() >0
					&& searchVals.size() == searchParams.size() 
					&& searchParams.size() == searchOperators.size()){
				outputParams.put("condition", true);
			}
			else{
				outputParams.put("condition", false);
				processMem.put("message","Incorrect Search Parameters");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputParams;
	}


}
