define(["dojo/_base/lang", "dijit/registry", "dojo/dom",  "dojo/dom-style", "widgets/OVCManagementConsole/widget",  "widgets/OVCManagementConsole/controller"], function(lang, registry, dom, domStyle, OVCManagementConsole, controller) {
	
	var USERS_BTN, LOCATIONS_BTN, DEVICES_BTN, REPORTS_BTN, MAIN_TITLE, PLEASE_SELECT;
	var currentView;
	
	function onLoad(parser) {
		
		parser.parse();
		
	//	USERS_BTN = registry.byId("usersBtn");
	//	LOCATIONS_BTN = registry.byId("locationsBtn");
	//	DEVICES_BTN = registry.byId("devicesBtn");
	//	REPORTS_BTN = registry.byId("reportsBtn");
	//	MAIN_TITLE = registry.byId("mainTitle");
	//	PLEASE_SELECT = dom.byId("pleaseSelect");
		
	//	currentView = registry.byId("pleaseSelectView");
		
	//	require(["dojo/i18n!nls/menus.js"], replaceLabels);
	//	registerMainMenuEvents();
		
	//	registry.byId("mainListView").resize();
		
	    var managementConsole = new OVCManagementConsole({}, "managementConsole");
	    domStyle.set(managementConsole.domNode,"width","100%");
	    domStyle.set(managementConsole.domNode,"height","100%");
	    domStyle.set(managementConsole.domNode,"background-color","white");
	    managementConsole.startup();
	    controller.setParent(managementConsole);
		
		
	}
	
	function registerMainMenuEvents() {
		showViewFactory(USERS_BTN, "users");
		showViewFactory(LOCATIONS_BTN, "locations");
		showViewFactory(DEVICES_BTN, "devices");
	}
	
	function showView(page) {
	
		var to = page.switchTo; 
		if (to !== currentView) {
			currentView.performTransition(to.get("id"));
			currentView = to;
		}
	}
	
	function showViewFactory(button, page) {
	
		require(["dojo/on"], lang.partial(registerButtonEvent, button, page));
	}
	
	function registerButtonEvent(button, page, on) {
	
		USERS_BTN.on("Click", lang.partial(requireView, page)); 
	}
	
	function requireView(page) {
		require(["ec/" + page], showView);
	}
	
	function replaceLabels(menus) {
		
	
		MAIN_TITLE.set("label", menus.main.title);
		USERS_BTN.set("label", menus.main.users);
		LOCATIONS_BTN.set("label", menus.main.locations);
		DEVICES_BTN.set("label", menus.main.devices);
		REPORTS_BTN.set("label", menus.main.reports);
		PLEASE_SELECT.innerHTML = menus.main.pleaseSelect;
		
		//Devices menu
		var title = registry.byId("deviceTitle");
		title.set("back", menus.main.title);
		title.set("label", menus.main.devices);
		registry.byId("devActiveCB").set("label", menus.devices.active);
		registry.byId("devInactiveCB").set("label", menus.devices.inactive);
		registry.byId("devDeletedCB").set("label", menus.devices.deleted);
		
		//Reports menu
		title = registry.byId("reportsTitle");
		title.set("back", menus.main.title);
		title.set("label", menus.main.reports);
		registry.byId("tranReportsBtn").set("label", menus.reports.tran);
	}
	
	require(["dojox/mobile/parser", "dojo/domReady!", "dojox/mobile/Pane", "dojox/mobile/ContentPane",
	         "dojox/mobile/FixedSplitter", "dojox/mobile/ScrollableView", "dojox/mobile/Heading",
	         "dojox/mobile/RoundRectList", "dojox/mobile/ListItem"], onLoad);
});