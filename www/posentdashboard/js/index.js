define(["dojo/dom", "dijit/registry", "dojo/on", "dojo/i18n!nls/index.js"], function(dom, registry, on, resources) {
	
	var lastUname, location, usernameTB, passwordTB, locationTB, loginBtn, loginForm, errorMsg;
	
	function onLoad(parser, domStyle) {
		parser.parse();
		
		usernameTB =  registry.byId("username");
		passwordTB = registry.byId("password");
		locationTB = registry.byId("location");
		loginBtn = registry.byId("login");
		loginForm = dom.byId("loginForm");
		errorMsg = dom.byId("errorMessages");
		
		usernameTB.set("placeholder", resources.usernamePlaceholder);
		passwordTB.set("placeholder", resources.passwordPlaceholder);
		locationTB.set("placeholder", resources.locationPlaceholder);
		loginBtn.set("label", resources.loginLabel);
		
		on(loginForm, "submit", preventSubmission);
		enableForm();
		
		var focused;
		if ((lastUname = localStorage.getItem("lastUname"))) {
			usernameTB.set("value", lastUname);
			focused = passwordTB;
		}

		if ((location = localStorage.getItem("location"))) {
			locationTB.set("value", location);
		} else {
			focused = locationTB;
		}
		
		domStyle.set(document.body, "visibility", "");
		focused.focus();
	}

	function enableForm() {
		on.once(loginForm, "submit", login);
	}
	
	function preventSubmission(evt) {
		evt.stopPropagation();
		evt.preventDefault();
	}
	
	function login(evt) {
		errorMsg.innerHTML = "";
		require(["dojo/request"], makeLoginRequest);
	}

	function makeLoginRequest(request) {

		var options = {
				data : JSON.stringify({
					location: registry.byId("location").get("value"),
					username: registry.byId("username").get("value"),
					password: registry.byId("password").get("value")
				}),
				preventCache: true,
				method: "POST",
				timeout: 10000,
				headers: {
					"Content-Type": "application/json"
				}
		};
		request("../../json/enterpriseconsole/login", options).response.then(preAuthSuccess, preAuthFailed);
	}
	
	function preAuthSuccess(response) {
		if (response.status === 200) {
			localStorage.setItem("lastUname", usernameTB.get("value"));
			localStorage.setItem("location", locationTB.get("value"));
			loginForm.submit();
		} else {
			displayError(response.status);
			enableForm();
		}
	}
	
	function preAuthFailed(e) {
		displayError(e.response.status);
		enableForm();
	}
	
	function displayError(status) {
		var message;
		switch (status) {
		case 0:
			message = resources.noServer;
			break;
		case 400:
			message = resources.noLocation;
			break;
		case 401:
			message = resources.badLogin;
			break;
		case 403:
			message = resources.badRole;
			break;
		default:
			message = resources.unknownError;
			break;
		}
		
		errorMsg.innerHTML = message;
	}

	if (!window.localStorage) {
		alert('Device does not support Web Storage');
	}

	require(["dojox/mobile/parser", "dojo/dom-style", "dojo/domReady!", "dojox/mobile/ContentPane",
	         "dojox/mobile/TextBox", "dojox/mobile/Button", "dojox/mobile/FormLayout"], onLoad);
});