define(["dojo/_base/declare", "dojo/request" , 
        "dojo/dom-construct",
      	"dojox/mobile/SimpleDialog",
        "dojox/mobile/ProgressIndicator",
        "dojox/mobile/Button",
        "dojo/_base/window", 
        "dojo/i18n!nls/index.js"], function(declare, request, domConstruct, SimpleDialog, ProgressIndicator, Button, win, indexResources) {
	
	var JSON_PATH = "../../json/";
	
	function processResponse(response) {
		if (response.status !== 200) {
			var e = new Error(response.data.message);
			e.code = response.status;
			throw e;
		}
		
		return response.data.data;
	}
	
	function getCurrentURL(){
		var base = location.href.split("/");
		base.pop();
		base = base.join("/");
		return base;
	}
	
	function responseError(e) {
		var status = e.response.status;
		var message;
		if (status === 0) {
			message = indexResources.noServer;
		} else {
			message = e.response.data.message;
		}
		
		var error = new Error(message);
		error.code = status;
		throw e;
	}
	  
	var Utils = declare(null, {
		getCurrentURL: getCurrentURL,
		
		execRestRequest: function(service, data, isProcess) {
			
			var envelope = {
				source: "external",
				username: "eCommerce",
				password: "changeme",
				data: data
			};
			
			var options = {
					data : JSON.stringify(envelope),
					preventCache: true,
					method: "POST",
					timeout: 150000,
					headers: {
						"Content-Type": "application/json"
					},
					handleAs: "json"
			};
			
			var path = JSON_PATH;
			if (!isProcess) {
				path += "enterpriseconsole/";
			}
			console.log("\npath = " + path);
			console.log("\nservice = " + service);
			return request(path + service, options).response.then(processResponse, responseError);
		},
		
		handleError: function (returnCode){
			if(returnCode == null)
				return true;
			var result = true;
			if(returnCode == 500){
				result = false;
			}else if(returnCode == 501){
			//	result = false;
			}else if(returnCode == 502){
				result = false;
			}else if(returnCode == 503){
				result = false;
			} else if(returnCode == 401){
				result = false;
			}  else if(returnCode == 402){
				result = false;
			}  else if(returnCode == 403){
				result = false;
			}  else if(returnCode == 404){
				result = false;
			}  else if(returnCode == 405){
				result = false;
			}  else if(returnCode == 411){
				result = false;
			}  else if(returnCode == 412){
				result = false;
			}  else if(returnCode == 450){
				result = false;
			} 
			return result;
		},

	    showProgIndDlg : function(title, errorMessage){ 
		    var dlg = new SimpleDialog();
		    win.body().appendChild(dlg.domNode);
		    var msgBox = domConstruct.create("div",
		                                     {class: "mblSimpleDialogText",
		                                      innerHTML: title},
		                                      dlg.domNode);
		    var piBox = domConstruct.create("div",
	                {class: "mblSimpleDialogText"},
	                 dlg.domNode);
		    piBox.appendChild(dojo.doc.createTextNode(errorMessage));
		    var cancelBtn = new Button({class: "mblSimpleDialogButton mblRedButton",
		                                innerHTML: "Cancel"});
		    cancelBtn.connect(cancelBtn.domNode, "click",
		                      function(e){dlg.hide();});
		    cancelBtn.placeAt(dlg.domNode);
		    dlg.show(); 
		  },
		
		execProcess: function(name, inputParams) {
			
			return this.execRestRequest("process/execute/" + name, inputParams, true);
		}
	});
	
	return new Utils(); 
});
