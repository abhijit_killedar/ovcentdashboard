define({
	
	root: {
		locationPlaceholder: "XlocationX",
		usernamePlaceholder: "XusernameX",
		passwordPlaceholder: "XpasswordX",
		loginLabel: "XloginX",
			
		noServer: "XCould not reach serverX",
		noLocation: "XInvalid location givenX",
		badLogin: "XUsername or password incorrectX",
		unknownError: "XAn unknown error occurredX",
		badRole: "XUser does not have permission to access this serviceX"
	},

	"en-us": true,
	"de": true
});
