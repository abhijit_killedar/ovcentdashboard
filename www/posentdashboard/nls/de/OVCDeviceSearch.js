define({ 
    deviceType	: "Geräte Typ",
    deviceAlias	: "Geräte Alias",
    reset		: "Zurücksetzen",
    action		: "Aktion",
    active		: "Aktiv",
    locationId	: "Verkaufsstelle ID",
    
    
    buttonDefinitions:[
        {
            label:'Speichern',
            eventType:'Save',
			id:'savedevlist'
        },
        {
            label:'Abbrechen',
            eventType:'Cancel',
			id:'canceldevlist'
        } 
    ]
});