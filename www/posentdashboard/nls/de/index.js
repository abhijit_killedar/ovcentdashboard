define({
	locationPlaceholder: "Verkaufsstelle ID",
	usernamePlaceholder: "Benutzername",
	passwordPlaceholder: "Passwort",
	loginLabel : "Anmeldung",
	
	noServer: "Could not reach server",
	noLocation: "Invalid location given",
	badLogin: "Username or password incorrect",
	unknownError: "An unknown error occurred",
	badRole: "User does not have permission to access this service",
});