define({ 
    SearchObjectList:
        {
        	
        	userid:["1234501"],
			gender:"Mr",
            active:1,
            firstname:"abcde",
            lastname:"xyz",
            email:"email@mail.com",
			resetpassword:"12345",
			retypepassword:"12345",
			roles:"Manager",
			location:"South Boston",
			addrole:"Add Additional Roles",
            isDeleted:0 
        } ,
    
    userID			: "User ID",
	gender			: "Gender",
    firstName		: "First Name",
    lastName		: "Last Name",
    email			: "Email",
    active			: "Aktiv",
	resetpassword	: "Reset Password",
	retypepassword	: "Retype Password",
    roles			: "Roles",
	location		: "Location",
	addrole			:	"Add Additional Roles",
	action      	: "Action",
	
	
	
    buttonDefinitions:[
	
		/*{
            label:'Cancel',
            eventType:'Cancel'
        },
		{
            label:'Abbrechen',
            eventType:'Delete'
        },
		{
            label:'Speichern',
            eventType:'Save'
        }*/
    ]
});