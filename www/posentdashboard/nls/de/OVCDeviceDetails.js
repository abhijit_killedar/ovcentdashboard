define({ 
    SearchObjectList:
        {
        	
        	printerIP:[""],
            active:1,
            alias:"",
            dUUID:"||",
            id:"",
            isDeleted:0,
            locationId:""
        }
    ,
    editDeviceHeader: "Geräte Bearbeiten",
    printerAddress 	: "Drucker Adresse", 
    deviceVersion	: "Geräte Version:",
    deviceType		: "Geräte Typ:",
    deviceAlias		: "Geräte Alias",
    resetData		: "Daten Zurücksetzen",
    active			: "Aktiv",
    locationId		: "Verkaufsstelle ID:",
    
    buttonDefinitions:[
        {
            label:'Speichern',
            eventType:'Save',
			id:'savedevdet'
        },
        {
            label:'Abbrechen',
            eventType:'Cancel',
			id:'canceldevdet'
        } 
    ]
});