 
define({
	data:[				
		{
			"Devices": {
				"id1":"Inactive",
				"id2":"Active",
				"id4":"All Devices",
			}
		},  
		{
			"Users": {
				"id5":"All Users",
				}
		},
		{
			"Roles": {
				"id6":"All Roles",
				
				}
		},   
		{
				"Location": {
					"id7":"All Locations",
					
					}
		},
		/*{
			"Upload": {
				"idUpload":"Device upload" 
			},
		},*/
		{
			"Reports": {
				"id8":"Transactions",
				"id11": "JDE Transactions Extract",
				"id12":"JDE Items Extract",
				"id9":"Till Totals",
				"id10": "Sales Summary"
				
			},
		}
	]
});
