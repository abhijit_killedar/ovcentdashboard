define({ 
    SearchObjectList:
        {
        	
        	userId:["1234501"], 
            active:0,
            firstName:"",
            lastName:"",
        	userName:"User Name",
            email:"",
			resetpassword:"",
			retypepassword:"",
			roles:"",
			location:"",
			addrole:"Add Additional Roles",
            isDeleted:0 
        } ,
    
	userID			: "User ID", 
	userName		: "User Name",
	firstName		: "First Name",
	lastName		: "Last Name",
	email			: "Email",
	active			: "Active",
	resetpassword	: "Reset Password",
	retypepassword	: "Retype Password",
	roles			: "Roles",
	location		:"Location",
	addrole			:"Add Additional Roles",
	action      	: "Action",
	
    
    buttonDefinitions:[
        
		/*{
            label:'Cancel',
            eventType:'Cancel'
        },
		{
            label:'Delete',
            eventType:'Delete'
        },
		{
            label:'Save',
            eventType:'Save'
        }*/
    ]
});