define({
	locationPlaceholder: "Location",
	usernamePlaceholder: "Username",
	passwordPlaceholder: "Password",
	loginLabel : "Login",
	
	noServer: "Could not reach server",
	noLocation: "Invalid location given",
	badLogin: "Username or password incorrect",
	unknownError: "An unknown error occurred",
	badRole: "User does not have permission to access this service",
});