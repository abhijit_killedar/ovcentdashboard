define({ 
	deviceType	: "Device Type",
    deviceAlias	: "Device Alias",
    reset		: "Reset",
    action		: "Action",
    active		: "Active",
    locationId	: "Location ID",
    
    buttonDefinitions:[
        {
            label:'Save',
            eventType:'Save',
			id:'savedevlist'
        },
        {
            label:'Cancel',
            eventType:'Cancel',
			id:'canceldevlist'
        } 
    ]
});