define({ 
	 SearchObjectList:
     {
     	
     	"id": "99",
        "locationType": "Store",
        "displayName": "TESTStore2",
        "streetName": "TESTStreet",
        "streetNumber": "TESTNumber",
        "postalCode": "TESTCode",
        "town": "TESTTown",
        "country": "TESTCountry",
        "phoneNumber": "9999999999",
        "faxNumber": "8888888888"
     },
     
	 
     id				: "Location ID:",
	 locationType	: "Location Type:",  
	 displayName	: "Location Name",
	 streetName		: "Street Name",
     streetNumber	: "Street Number",
     postalCode     : "Postal Code",
     town			: "Town",
     country		: "Country",
     phoneNumber    : "Phone Number",
     faxNumber		: "Fax Number",
	 
	 buttonDefinitions:[
		/*{
		    label:'Delete',
		    eventType:'delete',
			id:'savelocdet',
			disabled:'true'
		},
        {
            label:'Save',
            eventType:'save',
			id:'savelocdet',
			disabled:'true'
        },
        {
            label:'Cancel',
            eventType:'cancel',
			id:'cancellocdet',
			disabled:'true'
        } */
    ]
});