define({ 
	 SearchObjectList:
     {
     	
     	 printerIP:[""],
		 cashDrawerIP:[""],
         active:1,
         alias:"",
         dUUID:"||",
         id:"",
         isDeleted:0,
         locationId:""
     },
     
	 editDeviceHeader: "Edit Device",
	 printerAddress : "Printer Address", 
	 cashDrawerAddress : "Cash Drawer Address", 
	 deviceVersion	: "Device Version:",
	 deviceType		: "Device Type:",
	 deviceAlias	: "Device Alias",
	 resetData		: "Reset Data",
	 active			: "Active",
	 locationId		: "Location ID:",
	 
	 buttonDefinitions:[
        {
            label:'Save',
            eventType:'Save',
			id:'savedevdet'
        },
        {
            label:'Cancel',
            eventType:'Cancel',
			id:'canceldevdet'
        } 
    ]
});