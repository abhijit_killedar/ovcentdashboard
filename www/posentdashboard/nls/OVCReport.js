define({
	
	root: 
	{ 
		      "cfgReportServer" : "http://OVCHOST:OVCPORT/OVCBIRT/",
			  "cfgReportPath": "OVCHOME/ovc-repo/www/posentdashboard/reports/",
			  "Transactions":"Transactions.rptdesign",
			  "TransactionItems":"TransactionItems.rptdesign",
			  "TenderTotalTills":"TenderTotalTill.rptdesign",
			  "SalesSummary" : "SalesSummary.rptdesign",
		 	  "JDExtract": "JDExtractReport.rptdesign",
			  "TenderTotalTills2":"TenderTotalTill2.rptdesign",
			  "Pickups":"BOSSPOSRStatusreport2.rptdesign",
			  "cfgReportDBURL":"jdbc:mysql://OVCHOST/xdb",
			  "cfgReportDBUserId":"DB_USER",	
			  "cfgReportDBPassword":"DB_PASS"
	},

	"en-us": true,
	"de": true
});
