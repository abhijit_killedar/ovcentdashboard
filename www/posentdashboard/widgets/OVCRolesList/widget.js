	var widgetBoxContainer=null;
define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/dom",
	"dojo/on",
	"dojo/dom-class", 
	"dojo/dom-style",  
	"dojo/query", 
	"dojo/Deferred",
	"dgrid/Grid", 
	"dgrid/editor", 
	"dgrid/Keyboard", 
	 "widgets/OVCManagementConsole/controller",
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry", 
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCRolesList/widget.html:dojo/text!widgets/OVCRolesList/widget.html",
    "dojo/i18n!resource/nls/OVCRolesList",
    "custome/BoxContainer/widget",
    "custome/TableView/widget", 
	"dojox/mobile/Button",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/ListItem",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/EdgeToEdgeCategory"
	], function(declare, lang, dom, on , domClass, domStyle, query, Deferred, Grid, editor, Keyboard, ManagementConsole, Selection,  Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, 
			registry, JSON, template, bundle, BoxContainer, TableView, Button){
 	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() { 
			this._buttonClick(eventType, captureData);
 		});
	}
	 
	function optimalPush(array, value){ 
 		var temp = array[value.deviceId];
		if(temp == null){
			array[value.deviceId] = value;
		}else{
			array[value.deviceId] = lang.mixin(temp, value); 
		} 
	} 
	
    return declare("widgets/OVCRolesList/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
  		buttonDefinitions: [],
		buttonArray: [],
		store: "",
 		rowId: null,
 		rowDesc: "",
		selectedElements: {},
  		
 		SearchObjectList:[],

        onEditCallback: null,
        onButtonClickCallback: null,
        
		// Our template - important!
        templateString: template,
        
        gridRolesListWidget: null,
        
        constructor: function(){
         },
          
        baseClass: "OVCRolesList",
 
        setProps : function(){ 
        	data = bundle; 
            var widgetBoxContainer = new BoxContainer({ widgetDefinitions: bundle.buttonDefinitions });
            widgetBoxContainer.placeAt(this.buttonContainer);
            widgetBoxContainer.buttonClickEvent = lang.hitch(this, function(eventType){ 
				var roledata=Array(this.store);
				console.log(roledata);
				this.onButtonClickCallback(eventType, roledata);
			});   
        }, 
	        
	        setData: function(data){	 
				this.j = this.j +1;
				console.log(data);
				console.log("setData j = " + this.j);
				var _this = this; 
				this.SearchObjectList=data.SearchObjectList;
				this.store = new Memory({ data:this.SearchObjectList }); 
  				this.gridRolesListWidget = new (declare([OnDemandGrid, Selection]))({ 
						store:this.store,
						columns: { 
						    id: editor( {  
						        	label: bundle.roleId,
						            width: "5 px"  , 
									editor: "text",
									editOn: "click",
									canEdit: function(){
										//return true;(SIM-140 making stuff readonly for PHASE-1)
										return false;
									}
						}),                            
						name:editor( { 
							label:bundle.rolename ,
						    width: "5 px" ,
							editor: "text",
							editOn: "click",
							canEdit: function(){
								//return true;(SIM-140 making stuff readonly for PHASE-1)
								return false; 
							}
						})
					  },
					  cellNavigation: false // for Keyboard; allow only row-level keyboard navigation
			        }, this.grid);  
				this.gridRolesListWidget.renderArray(this.store);
				console.log(this.store);
				
				query(".Add").on("click", lang.hitch(this,function(evt){
					this.addNewRow();
				}));
				
				this.gridRolesListWidget.on(".dgrid-row:click", lang.hitch(this, function(event){
					var row = this.gridRolesListWidget.row(event);
					if (row.id != "") {
						_this.rowId = row.id;
					}
				}));
			
				this.gridRolesListWidget.on("dgrid-datachange", lang.hitch(this, function(evt){
						if (evt.cell.column.id == "id"){
							_this.rowId = evt.value;	
							var role = {id: "", name:""};
							debugger;
							if (evt.oldValue == "" || evt.oldValue == null){
								var oldValue = evt.oldValue
								role = _this.store.get(null);
								
								if (role != undefined){
									if (role.name == null){
										role = {id: _this.rowId, name: ""};
									}
									else{
										role = {id: _this.rowId, name: role.name};
									}
									_this.store.remove(null);
									_this.store.add(role);
								}
								
							}
							else
							{
								role = _this.store.get(evt.oldValue);
							}	 
						}
						else if (evt.cell.column.id == "name"){
							if (_this.rowId == ""){
								
							}
							else {
								if (evt.oldValue != evt.value){
									//var role = _this.store.query({role: evt.oldValue});
									var role = _this.store.query({name: evt.oldValue});
									console.log(role);
									if (role[0].id != "") {
										_this.rowId = role[0].id;
									}
									if (role[0].id != _this.rowId)
									{
										_this.store.remove(_this.rowId)
										_this.store.remove("");
										role = {id: _this.rowId, name: evt.value};
										_this.store.add(role);	
									}
									else {
										_this.store.remove(role[0].id)
										_this.store.remove("");
										role = {id: role[0].id, name: evt.value};
										_this.store.add(role);
									}
									
								}
							}						
						} 
				}));	
				 
	        },
			 
			addNewRow: function() {  
				var role = {	
					id: "", 
					name: ""
				}; 
				this.rowId = null; 
	    		var roleLine = this.store.get("");
				 
	    		if (roleLine == null) {
	    			this.store.add(role);
	    		} 
	    		this.gridRolesListWidget.set("store", this.store);
	    		this.gridRolesListWidget.select(this.gridRolesListWidget.contentNode.children[this.store.data.length]);
				
				i=1; 
				var gid=this.gridRolesListWidget.id;
				console.log(gid+"-row- td.field-id");
				query("#"+gid+"-row- td.field-id").addClass("dgrid-cell-editing"); 
				var label = query("#"+gid+"-row- td.dgrid-cell-editing"); 
				this.gridRolesListWidget.edit(label[0]); 
			},
			
	       
	        i: 0,
	        j: 0,
	        
	        onClickCallback : function(event) {   
         		if(event.srcElement.type == "checkbox"){  
  	        		var str = event.srcElement.id.split("|")[0];
	        		var id = event.srcElement.id.split("|")[1];	
	        		var alias = event.srcElement.id.split("|")[2];
	        		var locationId = event.srcElement.id.split("|")[3];
	        		var inputParams = { 	
							"deviceId" : id, 
 							"deviceAlias": alias,
							"locationId" : locationId == "null" ? "":locationId 
						}; 
	        		if(str == "active"){  
    					inputParams.deviceActive = event.srcElement.checked? "1" : "0"; 
    				}else if(str == "reset"){ 
    					inputParams.resetDevice ="true";  
    				}
	        		optimalPush(this.selectedElements, inputParams);
					console.log("i = " + this.i + " this.selectedElements = " + this.selectedElements.length);
	        		this.i = 0;  
         		}else if(event.srcElement.type == "submit"){ 
         			if(event.srcElement.defaultValue == "Edit"){ 
						this.setVisibility(false);
					}else if(event.srcElement.defaultValue == "View"){ 
						this.setVisibility(false); 
					}
         			this.onEditCallback(event.srcElement.id);
         			this.i = 0;
         		}
	        },
			         
	        setVisibility: function(isShow){
 	        	if(isShow){
	        		domStyle.set(this.domNode, "display", "inherit"); 
 	        	}
	        	else{
	        		domStyle.set(this.domNode, "display", "none");
	        	}
	        },
	        
	        destroy: function(){ 
	        	if(this.gridRolesListWidget == null){
	        		return;
	        	}
	        	this.gridRolesListWidget.noDataMessage = "";
 	        	this.gridRolesListWidget.set("store", null);
	        },
	        
			postCreate: function() {
 	            this.inherited(arguments);   
 	            this.setProps();
 	        } 
    });
});

