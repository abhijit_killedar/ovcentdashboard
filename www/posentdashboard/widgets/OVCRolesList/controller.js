define(
	[	"dojo/_base/declare", 
	 	"ec/util", 
		"dojo/_base/lang",
	   	 "widgets/OVCRolesList/controller", 
	   	 "OVCManagementConsole/controller",
		 "widgets/OVCDeviceSearch/widget",
	   	 "widgets/OVCRolesList/widget",
	     "dojo/request"
   	],  
   function(declare, Utils, lang, RolesController, ManagementController, OVCDeviceSearch, OVCRolesList, request) {
   
   var updateRoles = function(inputparams){  
		for(i in inputparams)
		console.log("in controller AddUpdateRole inputparams["+i+"] = " + inputparams[i]);
		//return Utils.execProcess("AddUpdateRole", inputparams).then(roleUpdateCallback);  
		return Utils.execProcess("AddUpdateRole", inputparams).then(function(data){
			for(i in data)
			console.log("in controller GetRole data["+i+"] = " + data[i]);
			console.log(data);
			if(Utils.handleError(data.returnCode)){ 
				rolesWidget.setVisibility(false); 
				var lastSelected = mgmtconsole.lastSelected; 	
				RolesController.setRoles(false, true, false);
				mgmtconsole.toggleRightPane(mgmtconsole.roleListWidget);
			} else{ 
				console.log("in controller roleUpdateCallback error")
				Utils.showProgIndDlg("Error", data.returnMsg);
			}  
			return data;
		});
	};
		
	/*var roleUpdateCallback = function(data){
		debugger;
		console.log("data in roleUpdateCallback = " + data);
		for(i in data)
		console.log("in controller GetRole data["+i+"] = " + data[i]);
		console.log(i);
		//rolesWidget.setData(data)
		if(Utils.handleError(data.returnCode)){ 
		rolesWidget.setData(data);
		} else{ 
			console.log("in controller roleUpdateCallback error")
			Utils.showProgIndDlg("Error", data.returnMsg);
		}  
		return data;
	};*/
	 
	var getRoles = function(isActive, isDeleted, isAll){
		 var inputParams = { 
					"entityType" : "Role", 
					"searchOperators" :["NOTEQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :["*"], 
					"searchType" : ["AND"] 
				}
		 if(!isAll){
			 if(!isDeleted){
				 if(isActive){
					 inputParams.searchParams.push("active");
					 inputParams.searchValues.push("1");
					 inputParams.searchOperators.push("EQUALS");
				 }else{
					 inputParams.searchParams.push("active");
					 inputParams.searchValues.push("0");
					 inputParams.searchOperators.push("EQUALS");
				 }  
			 }else{
				 if(!isDeleted){
					 inputParams.searchParams.push("isDeleted");
					 inputParams .searchValues.push("0");
					 inputParams .searchOperators.push("EQUALS");
				 }else{
					 inputParams .searchParams.push("isDeleted");
					 inputParams .searchValues.push("1");
					 inputParams .searchOperators.push("EQUALS");
				 }
			 }
		 } 
		 return Utils.execProcess("ObjectSearch", inputParams).then(roleListCallback); 
		 
	}; 
	
	var roleListCallback = function(data){
		console.log("data in controller = " + data);
		if(Utils.handleError(data.returnCode)){ 
			rolesWidget.destroy();
			rolesWidget.setData(data);
		} else{
			debugger; 
			console.log("in controller AddUpdateDevicePeripheral error")
			Utils.showProgIndDlg("Error", data.returnMsg);
		}  
		return data;
	};
	
	var rolesWidget = null;
	var deviceDetails = null;
	var mgmtconsole = null;
	var managementController=null;
	var RolesController = declare(null, {	  
		setParent: function(widget){
			rolesWidget = widget.roleListWidget;
			mgmtconsole = widget;
			//rolesWidget.setProps(widget.roleListWidget);
		},
		 		 
		setRoles : function(isActive, isDeleted, isAll){		 
			return getRoles(isActive, isDeleted, true);
		},
		 
		onButtonClickCallback: function(eventtype, roledata){  
			console.log("eventType = " + eventtype + " capturedData = "+roledata)
			if(eventtype == "Save"){  
				debugger;
				for(i in roledata){
					var deviceDetails = roledata[i]; 
					for(j in deviceDetails.data){
						console.log(deviceDetails);
						var newrole=deviceDetails.data[j];
						console.log(newrole); 
						var inputparams = {};
						inputparams.roleId = newrole.id;
						inputparams.roleName = newrole.name;
						updateRoles(inputparams);
					} 
				}
			}else if(eventtype == "Cancel"){
				rolesWidget.setVisibility(false); 
				var lastSelected = mgmtconsole.lastSelected; 	
				RolesController.setRoles(false, true, false);
				mgmtconsole.toggleRightPane(mgmtconsole.roleListWidget);
			} 
		},
		  
		navigateToRolesList: function(){ 
			rolesWidget.setVisibility(true); 
		} 
	}); 
	var RolesController = new RolesController();
	return  RolesController;
	
});
