define(
	[	"dojo/_base/declare", 
	 	"ec/util", 
		"dojo/_base/lang",
	   	 "widgets/OVCLocationList/controller", 
	   	 "OVCManagementConsole/controller",
		 "widgets/OVCLocationList/widget",
	     "dojo/request"
   	],  
   function(declare, Utils, lang, ManagementController,  OVCLocationList, request) {
	var getLocations = function( ){
		 var inputParams = { 
					"entityType" : "Location", 
					"searchOperators" :["NOTEQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :["*"], 
					"searchType" : ["AND"] 
				} 	
		 return Utils.execProcess("ObjectSearch", inputParams).then(locationListCallback); 
		 
	};
	
	var locationListCallback = function(data){
		console.log("data in controller = " + data);
		if(Utils.handleError(data.returnCode)){ 
			LocationsListWidget.destroy();
			LocationsListWidget.setData(data);
		} else{
			debugger; 
			console.log("in controller AddUpdateDevicePeripheral error")
			Utils.showProgIndDlg("Error", data.returnMsg);
		}  
		return data;
	};
	
	var LocationsListWidget = null;
	var locationDetails = null;
	var mgmtconsole = null;
	
	var LocationListController = declare(null, {	  
		setParent: function(widget){
		this.LocationsListWidget = widget.locationListWidget;
			LocationsListWidget = widget.locationListWidget;
			mgmtconsole = widget;
			LocationsListWidget.onEditCallback = ManagementController.navigateToEditlocationScreen;
			//LocationsListWidget.setProps(widget.locationListWidget);
		},
		 		 
		setLocations : function( ){		 
			return getLocations();
			
		},
		
			 
		onButtonClickCallback: function(eventtype, capturedData){
			debugger;
			console.log("eventType = " + eventtype + " capturedData = "+ capturedData)
			if(eventtype == "save"){ 
				for(i in capturedData){
					var locationDetails = capturedData[i];
					console.log("capturedData["+i+"]"+ locationDetails); 
 					//LocationListController.saveToDB(locationDetails);
				} 
			}else if(eventtype == "cancel"){
				var lastSelected = mgmtconsole.lastSelected; 
				//ManagementController.resetToDBForLastSelected(lastSelected, LocationListController);
			} 
		},
		  
		navigateToLocationListing: function(){  
			mgmtconsole.toggleRightPane(LocationsListWidget);
			return getLocations();  
		},
		
		saveToDB:function(inputParams){ 
			console.log("call inside saveToDB = " +inputParams );  
			var lastSelected = mgmtconsole.lastSelected; 
			//LocationListController.saveToDeviceDB(inputParams).then(lang.hitch(ManagementController, ManagementController.resetToDBForLastSelected(lastSelected, LocationListController)));
		}		 
	}); 
	var LocationListController = new LocationListController();
	return  LocationListController;
	
});
