//var setGridRowDataFormatter;
define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/dom",
	"dojo/on",
	"dojo/_base/window",
	"dojo/query",
	"dgrid/Grid", 
	"dgrid/Keyboard", 
	 "widgets/OVCManagementConsole/controller",
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry",
    "dojo/dom-class",
    "dojo/dom-style",
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCLocationList/widget.html:dojo/text!widgets/OVCLocationList/widget.html",
	 "dojo/i18n!resource/nls/OVCLocationList",
	  "custome/BoxContainer/widget",
	"dojox/mobile/Button",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/ListItem",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/ContentPane",
	"dojox/mobile/EdgeToEdgeCategory"
	], function(declare, lang, dom, on, win, query, Grid, Keyboard, ManagementController, Selection,  Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, registry, domClass, domStyle, JSON, template, bundle,  BoxContainer, Button){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
 
	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() { 
			this._buttonClick(eventType, captureData);
			//debugger; 
		});
	}
	 
	function optimalPush(array, value){ 
		debugger;
		var temp = array[value.locationId];
		if(temp == null){
			array[value.locationId] = value;
		}else{
			array[value.locationId] = lang.mixin(temp, value); 
		} 
	} 

    return declare("widgets/OVCLocationList/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
  		buttonlocDefinitions: [],
		buttonArray: [],
		selectedElements: {},
 		SearchObjectList:[],
		onEditCallback: null,
        onButtonClickCallback: null,
		 
		// A class to be applied to the root node in our template
        baseClass: "OVCLocationList",
 
		// Our template - important!
        templateString: template,
        
        gridLocationListWidget: null,
        
        constructor: function(){
        },
		
		 setProps : function(){ 
			console.log(this);
  	         data = bundle; 
			 
			var widgetBoxContainer = new BoxContainer({ widgetDefinitions: bundle.locbuttonDefinitions});
            widgetBoxContainer.placeAt(this.buttonContainer);
            widgetBoxContainer.buttonClickEvent = lang.hitch(this, function(eventType){
				debugger;
				this.onButtonClickCallback(eventType, this.selectedElements);
			});      
 	    },
		
		setData: function(data){	 
			this.j = this.j +1;
			console.log(data);
			console.log("setData j = " + this.j);
			// Create a new constructor by mixing in the components
			var CustomGrid = declare([ Grid, Keyboard, Selection ]);
			var SearchObjectList = data.SearchObjectList;
			var store = new Memory({ data: SearchObjectList });
	 
			this.gridLocationListWidget = new (declare([OnDemandGrid, Selection]))({
				sort:[{attribute: "type"}],	
	          loadingMessage: "Loading data...",
	          noDataMessage: "No results found.",
	          cellNavigation: false, // for Keyboard; allow only row-level keyboard navigation
	          columns: {	 
                     id:bundle.locationID, 
			         locationType: bundle.type,
					 displayName: bundle.name
					 //SIM-140 COMMENTNG  for SIMPLOT PHASE-1
					/* action	:{ 
								field: "_item",
								label: bundle.action,
								sortable: false,
								formatter: function(SearchObjectList) {
									debugger;
									 var id = "\""+SearchObjectList.id+"\"";
									return "<input id="+id+" class=\"editaction\" type =\"submit\" value=\"Edit\"></input>";
								}
                            }*/
				
	          },
	        }, this.gridLocationList);
		
    	this.gridLocationListWidget.set("sort","type");
		this.gridLocationListWidget.renderArray(SearchObjectList); 
			this.setVisibility(true);
			this.gridLocationListWidget.on(".dgrid-row:click", lang.hitch(this, function(event){
				this.i = this.i +1; 
				console.log("dgrid-select i = " + this.i + " j = " + this.j);
					// Report the item from the selected row to the console.
					if(this.i%(this.j)==0)
						this.onClickCallback(event);
				}));
		},
	       
		i: 0,
	    j: 0,
 
		onClickCallback : function(event) {   
			if(event.srcElement.type == "checkbox"){  
				//event.srcElement.checked = !event.srcElement.checked;
				debugger;
				var str = event.srcElement.id.split("|")[0];
				var locationid = event.srcElement.id.split("|")[1];	
				var type = event.srcElement.id.split("|")[2];
				var name = event.srcElement.id.split("|")[3];
				var group = event.srcElement.id.split("|")[4];
				var inputParams = { 	
						"locationId" : id, 
						"type"		 : locationType,
						"name" 		 : displayName== "null" ? "":displayName 
						
					};
			
			if(str == "active"){  
					inputParams.userActive = event.srcElement.checked? "1" : "0"; 
				}
				optimalPush(this.selectedElements, inputParams);
				console.log("i = " + this.i + " this.selectedElements = " + this.selectedElements.length);
				this.i = 0;   
			}else if(event.srcElement.type == "submit"){ 
					debugger;
				if(event.srcElement.defaultValue == "Edit"){ 
					console.log("inside Click ",event.srcElement.defaultValue + " is clicked");
					this.setVisibility(false);
				}else if(event.srcElement.defaultValue == "View"){ 
					this.setVisibility(false); 
				}
				console.log(event.srcElement.id);
				this.onEditCallback(event.srcElement.id);
				//this.onEditCallback();
				this.i = 0;
			} 
		},
	 
		setVisibility: function(isShow){
			if(isShow){
				domStyle.set(this.domNode, "display", "inherit"); 
			}
			else{
				domStyle.set(this.domNode, "display", "none");
			}
		},
	        
		destroy: function(){ 
			if(this.gridLocationListWidget == null){
				return;
			}
			this.gridLocationListWidget.noDataMessage = "";
			this.gridLocationListWidget.set("store", null);
		},
		
		postCreate: function() {
			this.inherited(arguments);  
			this.setProps();
		}  
    });
});


	
			
      