define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/dom",
	"dojo/on", 
				
    "dojo/dom-class", 
	"dojo/dom-style",  
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dijit/Tooltip",
    "dojo/has!testing?dojo/text!widgets/OVCHeader/widget.html:dojo/text!widgets/OVCHeader/widget.html",    
    "dojo/i18n!resource/nls/OVCHeader",  
    'dojox/mobile/Heading',  
    "dojox/mobile/ToolBarButton" ,
	 "dojox/mobile/Icon",
	"dojox/mobile/ContentPane",
    'dojo/dom-construct',  
    'dojo/_base/window',  
    'dojo/ready' ,
    "widgets/OVCHeader/controller"
], function(declare, lang, dom, on, domClass, domStyle, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin, Tooltip, template, bundle, Heading, ToolBarButton, Icon, domConstruct, win, ready, HeaderController){
	var logoutCallback = function(){
		console.log("Logout has been called");
	};
     
	var companyCallback = function(){
		console.log("companyCallback has been called");
	};
	
    return declare("widgets/OVCHeader/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin ], {
        templateString: template,
        controller : null,
        companyToolBar : null,
        logoutToolBar: null,
        
        constructor: function(){
          },
         
        title: "",
        // A class to be applied to the root node in our template
        baseClass: "OVCHeader",
        
        heading : null,
        
        postCreate: function() {  
            this.heading = new dojox.mobile.Heading({
  			  dojoType: "dojox.mobile.Heading", 
  			  fixed: "top",
  			  moveTo: "mblFormLayoutTitle",
  			  label: this.title
  			});
            this.companyToolBar = new ToolBarButton({class:"OVClogo"});
            this.companyToolBar.on("click", function(){ companyCallback(); }); 
            
            this.logoutToolBar = new ToolBarButton({ 
              label: bundle.logout,
              style:"float:right;",
              class:"OVClogout",
            });
            this.logoutToolBar.on("click", function(){ logoutCallback(); }); 
			
			var collapse_icon;
			collapse_icon = new dojox.mobile.Button({label:"OVCextender",class:"OVCextender",id:"OVCextender"}).placeAt(this.side_head_collapse);
		 
					collapse_icon.startup;
					var collapse = false;
					collapse_icon.on("click",function(){		
						if(!collapse){
							collapse=true;
							domStyle.set("dijit_layout_ContentPane_1", "width", "0%");
							domStyle.set("dijit_layout_ContentPane_1_splitter", "left", "0%");
							domStyle.set("dijit_layout_ContentPane_2", "width", "100%");
							domStyle.set("dijit_layout_ContentPane_2", "left", "0%");
							domClass.add("OVCextender", "collapsed");
						 }else{
							collapse=false;
							domStyle.set("dijit_layout_ContentPane_1", "width", "16%");
							domStyle.set("dijit_layout_ContentPane_1_splitter", "left", "16%");
							domStyle.set("dijit_layout_ContentPane_2", "width", "84%");
							domStyle.set("dijit_layout_ContentPane_2", "left", "16%");
							domClass.remove("OVCextender", "collapsed");
						 }
					});
							
			
			
			
    	},
    	
    	setData : function(){
    		this.heading.placeAt(this.titleNode);
    		this.heading.addChild(this.companyToolBar);
    		this.heading.addChild(this.logoutToolBar);  
    	},
    	
    	setLogoutCallback : function(functionName){
    		logoutCallback = functionName;
    	},
    	
    	setCompanyCallback : function(functionName){
    		companyCallback = functionName;
    	} 
         
    });
});
 
 
