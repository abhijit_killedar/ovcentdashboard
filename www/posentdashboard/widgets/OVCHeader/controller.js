define(
	["dojo/_base/declare", "ec/util", 
   	 "widgets/OVCManagementConsole/controller",
   	 "widgets/OVCHeader/widget" 
   	],  
   function(declare, Utils, ManagementController, OVCHeader) {
	var loadDataFromServiceCB = function(isActive, all){
		 var inputParams = { 
					"entityType" : "Device", 
					"searchOperators" :["NOTEQUALS","EQUALS"], 
					"searchParams" :["id", "isDeleted"], 
					"searchValues" :["*", "0"], 
					"searchType" : ["AND"] 
				}
		 
		 return HeaderController.makeServiceCall("ObjectSearch", inputParams);
	};
	
	
	var HeaderController = declare(null, {		
		makeServiceCall: function(name, inputParams) {
 			return Utils.execProcess(name,  inputParams
 			).then(this.loadDataFromService); 
		},
		
		setParent: function(widget){
			parentView = widget;
		},
		
		loadDataFromService : function(data){
 			console.log("data in controller = " + data); 
		},
		  	
		
		navigateToHomeScreen : function(id){
 			 
		},
		
		setData: function(){
 			parentView.setData();
			parentView.setLogoutCallback(this.logoutGoToLoginScreen);
			
		},
		
		logoutGoToLoginScreen: function(){ 
			 debugger;
			 console.log("I am inside controller Logout");
			 parentView.emit("logout", {data:"myData"});
		}
		 
	});
	var parentView = null;
	var HeaderController = new HeaderController();
	return  HeaderController;
	
});