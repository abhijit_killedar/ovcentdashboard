define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/on",
	"dgrid/Grid", 
	"dgrid/Keyboard", 
	 "widgets/OVCManagementConsole/controller",
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry",
    "dojo/dom-class",
    "dojo/dom-style",
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCDeviceSearch/widget.html:dojo/text!widgets/OVCDeviceSearch/widget.html",
    "dojo/i18n!resource/nls/OVCDeviceSearch",
    "custome/BoxContainer/widget",
	"dojox/mobile/Button",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/ListItem",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/EdgeToEdgeCategory"
	], function(declare, lang, on, Grid, Keyboard, ManagementConsole, Selection,  Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, registry, domClass, domStyle, JSON, template, bundle, BoxContainer, Button){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
 
	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() { 
			this._buttonClick(eventType, captureData);
 		});
	}
	 
	function optimalPush(array, value){ 
 		var temp = array[value.deviceId];
		if(temp == null){
			array[value.deviceId] = value;
		}else{
			array[value.deviceId] = lang.mixin(temp, value); 
		} 
	}
	 
    
    return declare("widgets/OVCDeviceSearch/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
  		buttonDefinitions: [],
		buttonArray: [],
		
		selectedElements: {},
  		
 		SearchObjectList:[],

        onEditCallback: null,
        onButtonClickCallback: null,
        
		// Our template - important!
        templateString: template,
        
        gridAlertDetailsWidget: null,
        
        constructor: function(){
         },
         

        // A class to be applied to the root node in our template
        baseClass: "OVCDeviceSearch",

  	        setProps : function(widget){
  	        	data = bundle;
  	        	debugger;
                var widgetBoxContainer = new BoxContainer({ widgetDefinitions: bundle.buttonDefinitions });
                widgetBoxContainer.placeAt(widget.buttonContainer);            
                widgetBoxContainer.buttonClickEvent = lang.hitch(this, function(eventType){
                	debugger;
                	widget.onButtonClickCallback(eventType, this.selectedElements);
				}); 
 	        },
	         
	        
	        setData: function(data){	 
				this.j = this.j +1;
				console.log(data);
				console.log("setData j = " + this.j);
 				// Create a new constructor by mixing in the components
			    var CustomGrid = declare([ Grid, Keyboard, Selection ]);
 				var SearchObjectList = data.SearchObjectList; 
 				var store = new Memory({ data: SearchObjectList });
 		          
 				this.gridAlertDetailsWidget = new (declare([OnDemandGrid, Selection]))({
 			          sort:[{attribute: "alias"}],	
			          loadingMessage: "Loading data...",
			          noDataMessage: "No results found.",
			          columns: {	 
                               dUUID: { 
                              	 		label: bundle.deviceType,
										width: "15%",
	                                	sortable: true,
				            	 		formatter: function(store) {
  	        		        	 			var splitArr = store.split("|");
				            	 			var ret = "";
				            	 			if(splitArr.length >=1)
				            	 				ret = splitArr[0];
				            	 			if(splitArr.length >=4)
				            	 				ret += " - " + splitArr[3];
				            	 			return ret ;
				            	 		}
					             }, 
					            alias: bundle.deviceAlias,
							    locationId: bundle.locationId,
				            	active: { 
	                                	field: "_item",
	                                	sortable: false,
		                                label: bundle.active,
		                                width: "5%",
		                                formatter: function(SearchObjectList) { 
		                                	var temp = (SearchObjectList.active == 1) ? "checked" : "";
		                                	var id = "\"active|"+SearchObjectList.id+"|"+SearchObjectList.alias+"|"+SearchObjectList.locationId+"\"";
		                                     return "<input id="+id+" class=\"deactive\" type =\"checkbox\" "+ temp +"/><label for="+id+"></label>";
		                                }
                               },
					            reset: { 
		                                	field: "_item",
			                                label: bundle.reset,
			                                width: "5%",
		                                	sortable: false,
			                                formatter: function(SearchObjectList) { 
 			                                	var id = "\"reset|"+SearchObjectList.id+"|"+SearchObjectList.alias+"|"+SearchObjectList.locationId+"\"";			                    				
			                                     return "<input id="+id+" class=\"active\" type =\"checkbox\" /><label for="+id+"></label>";
			                                }
	                                 },
                               edit: { 
	                                	field: "_item",
		                                label: bundle.action,
		                                width: "30%",
	                                	sortable: false,
		                                formatter: function(SearchObjectList) {
		                                     return "<input id=\""+ SearchObjectList.id +"\"  class=\"editod\" type =\"submit\" value=\"Edit\" >  </input>";
		                                }
                               }
						},
						cellNavigation: false // for Keyboard; allow only row-level keyboard navigation
			        }, this.gridAlertDetails);
 				
				this.gridAlertDetailsWidget.renderArray(SearchObjectList);
				this.gridAlertDetailsWidget.set("sort","alias");
				this.setVisibility(true);  
				this.gridAlertDetailsWidget.on(".dgrid-row:click", lang.hitch(this, function(event){
					this.i = this.i +1; 
					console.log("dgrid-select i = " + this.i + " j = " + this.j);
			            // Report the item from the selected row to the console.
						if(this.i%(this.j)==0)
							this.onClickCallback(event);
			        }));
	        },
	       
	        i: 0,
	        j: 0,
	        
	        onClickCallback : function(event) {   
         		if(event.srcElement.type == "checkbox"){  
	        		//event.srcElement.checked = !event.srcElement.checked;
  	        		var str = event.srcElement.id.split("|")[0];
	        		var id = event.srcElement.id.split("|")[1];	
	        		var alias = event.srcElement.id.split("|")[2];
	        		var locationId = event.srcElement.id.split("|")[3];
	        		var inputParams = { 	
							"deviceId" : id, 
 							"deviceAlias": alias,
							"locationId" : locationId == "null" ? "":locationId 
						};
	        		
	        		if(str == "active"){  
    					inputParams.deviceActive = event.srcElement.checked? "1" : "0"; 
    				}else if(str == "reset"){ 
    					inputParams.resetDevice ="true";  
    				}
	        		optimalPush(this.selectedElements, inputParams);
					console.log("i = " + this.i + " this.selectedElements = " + this.selectedElements.length);
	        		this.i = 0;  
         		}else if(event.srcElement.type == "submit"){ 
         			if(event.srcElement.defaultValue == "Edit"){ 
						this.setVisibility(false);
					}else if(event.srcElement.defaultValue == "View"){ 
						this.setVisibility(false); 
					}
         			this.onEditCallback(event.srcElement.id);
         			this.i = 0;
         		}
	        },
	        	        
	        setVisibility: function(isShow){
 	        	if(isShow){
	        		domStyle.set(this.domNode, "display", "inherit"); 
 	        	}
	        	else{
	        		domStyle.set(this.domNode, "display", "none");
	        	}
	        },
	        
	        destroy: function(){ 
	        	if(this.gridAlertDetailsWidget == null){
	        		return;
	        	}
	        	this.gridAlertDetailsWidget.noDataMessage = "";
 	        	this.gridAlertDetailsWidget.set("store", null);
	        },
	        
			postCreate: function() {
 	            this.inherited(arguments);   
 	        }
    });
});

