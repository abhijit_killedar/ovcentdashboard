define(
	[	"dojo/_base/declare", 
	 	"ec/util", 
		"dojo/_base/lang",
	   	 "OVCDeviceDetails/controller", 
	   	 "OVCManagementConsole/controller",
		 "widgets/OVCDeviceSearch/widget",
	   	 "widgets/OVCDeviceDetails/widget",
	     "dojo/request"
   	],  
   function(declare, Utils, lang, DeviceDetailsController, ManagementController, OVCDeviceSearch, OVCDeviceDetails, request) {
	
	 
	
	var navigateToLastKnownDeviceListing = function(lastSelected, DeviceSearchController, mgmtconsole){  
 			if(lastSelected == "Active"){ 
				DeviceSearchController.setDevices(true, false, false);
			}
			else if(lastSelected == "Inactive"){ 
				DeviceSearchController.setDevices(false, false, false);
			} 
			else if(lastSelected == "Deleted") { 
				DeviceSearchController.setDevices(false, true, false);
			}
			else { 
				DeviceSearchController.setDevices(true, false, true);
			} 
			//mgmtconsole.toggleRightPane(mgmtconsole.deviceSearchWidget);	
	};
		
	
	var deviceSearchWidget = null;
	var deviceDetails = null;
	var mgmtconsole = null;
	
	var DeviceSearchController = declare(null, {	  
		setParent: function(widget){ 
 			deviceSearchWidget = widget.deviceSearchWidget;
 			this.deviceSearchWidget = widget.deviceSearchWidget; 
 			this.mgmtconsole = widget;
			mgmtconsole = widget;
		},
		
		getDeviceSearchWidget: function(){
			return deviceSearchWidget;
		},
		
		deviceSearchWidget: deviceSearchWidget,
		mgmtconsole: mgmtconsole,
		
		deviceSearchCallback : function(data, parent){
			console.log("data in controller = " + data);
			if(Utils.handleError(data.returnCode)){ 
				debugger;
				//this.get("deviceSearchWidget").destroy();
				parent.mgmtconsole.deviceSearchWidget.destroy();
				parent.mgmtconsole.deviceSearchWidget.setData(data);
			} else{ 
				console.log("in controller AddUpdateDevicePeripheral error");
				Utils.showProgIndDlg("Error", data.returnMsg);
			}  
			return data;
		},
		
		_getDeviceSearchWidgetAttr: function () {
				return this.deviceSearchWidget;
		},
		
		_setDeviceSearchWidgetAttr: function (widget) {
				this.set("deviceSearchWidget", widget.deviceSearchWidget);

				// this._pickupToolBar is an attach point (@see widget.html)
				widget.tab && widget.tab.set("deviceSearchWidget", widget);
		},
		 	
	 	getDevices : function(isActive, isDeleted, isAll, parent){
			 var inputParams = { 
						"entityType" : "Device", 
						"searchOperators" :["NOTEQUALS"], 
						"searchParams" :["id"], 
						"searchValues" :["*"], 
						"searchType" : ["AND"] 
				};
			 if(!isAll){
				 if(!isDeleted){
					 if(isActive){
						 inputParams.searchParams.push("active");
						 inputParams.searchValues.push("1");
						 inputParams.searchOperators.push("EQUALS");
					 }else{
						 inputParams.searchParams.push("active");
						 inputParams.searchValues.push("0");
						 inputParams.searchOperators.push("EQUALS");
					 }  
				 }else{
					 if(!isDeleted){
						 inputParams.searchParams.push("isDeleted");
						 inputParams .searchValues.push("0");
						 inputParams .searchOperators.push("EQUALS");
					 }else{
						 inputParams .searchParams.push("isDeleted");
						 inputParams .searchValues.push("1");
						 inputParams .searchOperators.push("EQUALS");
					 }
				 }
			 } 
			 return Utils.execProcess("ObjectSearch", inputParams).then(lang.hitch(this, function(data){this.deviceSearchCallback(data, this);}));
		},
				 
		setDevices : function(isActive, isDeleted, isAll){		 
			return this.getDevices(isActive, isDeleted, isAll, this);
		},
		 
		onButtonClickCallback: function(eventtype, capturedData){ 
			console.log("eventType = " + eventtype + " capturedData = "+ capturedData);
			if(eventtype == "Save"){ 
			//console.log(capturedData);
				for(i in capturedData){
					var deviceDetails = capturedData[i];
					console.log("capturedData["+i+"]"+ deviceDetails); 
					DeviceSearchController.saveToDB(deviceDetails);
				} 
			}else if(eventtype == "Cancel"){
				var lastSelected = mgmtconsole.lastSelected; 
				ManagementController.resetToDBForLastSelected(lastSelected, DeviceSearchController);
			} 
		},
		  
		navigateToDeviceListing: function(){  
			ManagementController.resetToDB(); 
		},
		
		resettoDB:function(lastSelected){ 
			navigateToLastKnownDeviceListing(lastSelected, DeviceSearchController, mgmtconsole);
		},
		
		
		saveToDB:function(inputParams){ 
			console.log("call inside saveToDB = " +inputParams );  
			var lastSelected = mgmtconsole.lastSelected; 
			DeviceDetailsController.saveToDeviceDB(inputParams, lastSelected);
			//.then(lang.hitch(this, this.navigateToLastKnownDeviceListing(lastSelected, DeviceSearchController)));
		}		 
	}); 
	var DeviceSearchController = new DeviceSearchController();
	return  DeviceSearchController;
	
});
