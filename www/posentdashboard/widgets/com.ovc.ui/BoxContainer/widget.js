define([
    "dojo/_base/declare",
	"dojo/_base/lang",
    "dojo/has!testing?dojo/text!custome/BoxContainer/widget.html:dojo/text!custome/BoxContainer/widget.html",
    "custome/RootView/widget",
	"dojox/mobile/Button"
], function(declare, lang, template, RootView, Button){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
    function makeButtonEvent(eventType, captureData, context) {
    	debugger;
        return lang.hitch(context, function() {
            this._buttonClick(eventType, captureData);
        });
    }
    return declare("custome/BoxContainer/widget",[RootView], {
	  
 		widgetDefinitions: [],
	  // Our template - important!
        templateString: template,
        buttonClickEvent: null,
        // A class to be applied to the root node in our template
        baseClass: "BoxContainer",

		 _buttonClick: function(eventType, captureData) {
  	            var event = {}; 
	            if (captureData && this.getChildren()[0]) {
	                var children = this.getChildren()[0].getChildren();
	                for (var i = 0; i < children.length; i++) {
	                    var child = children[i];
	                    switch (child.declaredClass) {
	                        case "dojox.mobile.ComboBox":
	                        case "dojox.mobile.TextBox":
	                        case "dojox.mobile.ExpandingTextArea":
	                        case "dojox.mobile.Slider":
	                            event[child.name] = child.value;
	                            break;
	                        case "dojox.mobile.CheckBox":
	                        case "dojox.mobile.ToggleButton":
	                            event[child.name] = child.checked;
	                            break;
	                        case "dojox.mobile.RadioButton":
	                            if (child.checked) {
	                                event[child.name] = child.value;
	                            };
	                            break;
	                        case "dojox.mobile.RoundRectList":
	                        case "dojox.mobile.EdgeToEdgeList":
	                            findSelectedItems(child, event);
	                            break;
	                    }
	                }
	            }
	            console.log("Firing the event --- " + eventType );
	            // emit eventType
	            this.buttonClickEvent(eventType, event);
	        },
		
		postCreate: function() {
            this.inherited(arguments);
             for (var i = 0; i < this.widgetDefinitions.length; i++) {
                var widgetDefinition = this.widgetDefinitions[i];
                var label = widgetDefinition.label;
				//var elementid=widgetDefinition.id;
                var eventType = widgetDefinition.eventType || widgetDefinition.label;
                var captureData = widgetDefinition.captureData;

                var button = new Button({label: label, class:eventType}).placeAt(this.buttonContainer);
                button.startup();
                
                // use a function to pass the correct values of the params as we process the array
                // @see makeButtonEvent()
                button.on("click", makeButtonEvent(eventType, captureData, this));
            }
        }
	    
    });
});

