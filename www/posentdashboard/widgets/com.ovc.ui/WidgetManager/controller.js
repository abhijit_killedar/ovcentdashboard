define(
	["dojo/_base/declare", 
   	 "dojo/dom-construct", 
     "dojo/has!testing?UI/WidgetManager/widget:UI/WidgetManager.widget"
   	],  
   function(declare, domConstruct) {
	var alertCenter = null;
 	var appointmentScheduler = null;
 	var searchCenter = null;
	var calendarSettings = null;
	var orderLookup = null;
	var searchView = null;
	var parentView = null;
 	
	var WidgetBaseController = declare(null, {	  
		create: function(){ 
			parentView = new WidgetManager({title:"" },  parentView.baseNode);
 			return parentView;
		},		
		
		setProp : function(configkey, configValue){
			if(parentView == null){create();}
			parentView.setProp(configkey, configValue);
      	},
    	
    	setData : function(key, value){			
    		if(parentView == null){create();} 
			parentView.setData(configkey, configValue);
     	},
     	
     	setCallback : function(functionName){
			if(parentView == null){create();}
			myCallback = functionName;
    	},
		
		reload: function(widgetId){
			if(parentView == null){create();}

			if(widgetId == 'alert'){
				if(alertCenter!=null)
					alertCenter = new OVCAlertCenter({},  parentView.alertCenter);
				alertCenter.reload();
			}else if(widgetId == 'appointment'){
				if(appointmentScheduler!=null)
					appointmentScheduler = new OVCAlertCenter({},  parentView.appointmentScheduler);
				appointmentScheduler.reload();
			}else if(widgetId == 'searchCenter'){
				if(searchCenter!=null)
					searchCenter = new OVCAppointmentScheduler({title:parentView.title },  parentView.searchCenter);
				searchCenter.reload();
			}else if(widgetId == 'calendarSettings'){
				if(calendarSettings!=null)
					calendarSettings = new OVCCalendarSettings({title:parentView.title },  parentView.calendarSettings);
				calendarSettings.reload();
			}else if(widgetId == 'orderLookup'){
				if(orderLookup!=null)
					orderLookup = new OVCOrderCenter({title:parentView.title },  parentView.orderLookup);
				appointmentScheduler.reload();
			}else if(widgetId == 'searchView'){
				if(searchView!=null)
					searchView = new OVCSearchCenter({title:parentView.title },  parentView.searchView);
				searchView.reload();
			} 
		}
	});

	var WidgetBaseController = new WidgetBaseController();
	return  WidgetBaseController; 
});