define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin",
	"dijit/_WidgetsInTemplateMixin",   
    "dojo/has!testing?dojo/text!UI/WidgetManager/widget.html:dojo/text!UI/WidgetManager.widget.html",  
    "dojo/has!testing?UI/WidgetManager/controller:UI/WidgetManager.controller"
], function(declare, lang, on, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin, 
			template, WidgetManager ){
 	var myCallback = function(widget, event){
		console.log("Callback has been called on widget = " + widget + " event = " + event);
	};
  
    return declare("UI/WidgetManager/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin ], {
        templateString: template,
        baseController : null, 
        data: {},
        config: {},
        // A class to be applied to the root node in our template
        baseClass: "WidgetManager",
        
        constructor: function (params) {
            params = params || {};
            console.log(this.baseClass+" constructor" + dojo.toJson(params));
         },
        
        startup: function () {
            this.inherited(arguments);
        },
        
        postCreate: function () {
            this.inherited(arguments);
        },
    	
    	setProp : function(configkey, configValue){
    		config[configkey] = configValue;
      	},
    	
    	setData : function(key, value){
    		data[key] = value;
     	},
     	
     	setCallback : function(functionName){
     		myCallback = functionName;
    	}
    });
});
 
 
