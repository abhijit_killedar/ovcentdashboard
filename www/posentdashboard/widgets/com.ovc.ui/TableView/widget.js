define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dgrid/Grid", 
	"dgrid/Keyboard", 
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid","dgrid/extensions/DijitRegistry",
	"custome/RootView/widget",
    "dojo/has!testing?dojo/text!UI/TableView/widget.html:dojo/text",
	"dojox/mobile/Button"
], function(declare, lang, Grid, Keyboard, Selection,  Memory, Observable, OnDemandGrid, DijitRegistry, RootView, template, Button){
	var setStore = function(orderData){
		var store = Observable(Memory({ data: orderData.items }));
		gridAlertDetails.set("store", store);
	};
	
	var setPrototype = function(prototype){
		var columns = {}; 
      	for(i in prototype){
     		columns[i] = {
 				field: "_item",	
 				label: prototype[i]["label"],
 				width: prototype[i]["width"],
 				formatter: getFormatFunction(i, prototype[i]["formatter"])
     		};
     	}
		gridAlertDetails.set("columns", columns);
	};
	
	getFormatFunction = function(data, formatFunction){
		var functionReturn;
 		if(formatFunction == null){
 			functionReturn = function(orderData) {
	 			return orderData[data];
	 		}
 		}else{
 			functionReturn = formatFunction;
 		}
 		return functionReturn;
	};
	
	var gridAlertDetails = null;
	
    return declare("custome/TableView/widget",[RootView], {
 		orderData:[], 
		prototype : {},
        templateString: template,

        // A class to be applied to the root node in our template
        baseClass: "TableView",
        
        constructor: function(arguments){
        	debugger; 
        	this.prototype = arguments.prototype;
        	this.orderData = arguments.orderData;
            this.inherited(arguments); 
        },
 
		postCreate: function() {
			debugger;
            this.inherited(arguments);  
			gridAlertDetails = new (declare([OnDemandGrid, Selection, DijitRegistry]))({
		            selectionMode: "none", 
					cellNavigation: false // for Keyboard; allow only row-level keyboard navigation
		        }, this.gridAlertDetails);
			
			this.reload(); 
        },
	     
    	reload : function(){ 
    		debugger;
    		setStore(this.orderData);
    		setPrototype(this.prototype);
			gridAlertDetails.renderArray(this.orderData); 
    	},
    	
    	_getPrototypeAttr: function(){ return this.get("prototype"); },
    	  
    	_setPrototypeAttr: function(value){ this.prototype = value; this.set("prototype", value); },	
    	
    	_getOrderDataAttr: function(){ return this.get("orderData"); },
   	  
    	_setOrderDataAttr: function(value){ this.orderData = value; this.set("orderData", value); },
    	
    	setData : function(orderData){
    		if(orderData==null || orderData.length == 0)
    			throw "Incorrect orderData set on setStore() for the TableView - " + this.id;
    		this.orderData = orderData;
    	},
    	
    	setPrototype : function(prototype){      
            this.prototype = prototype; 
    	} 
    });
});



