define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo",
	"dojo/on",
    "UI/RootView/widget",
    "dojo/dom-construct",
    "dojox/mobile", 
    "dojox/mobile/deviceTheme", 
    "dojox/mobile/compat",
    "dojo/has!testing?dojo/text!UI/HTMLView/widget.html:dojo/text!UI/HTMLView/widget.html",  
    "dojo/request/xhr",
    "UI/HTMLView/controller" 
], function(declare, lang, dojo, on, RootView, domConstruct,
			mobile, deviceTheme, compat, template, xhr, HTMLViewController ){
 	var myCallback = function(){
		console.log("Callback has been called");
	};
  
    return declare("UI/HTMLView/widget",[RootView], {
    	templateString: template,
        baseController : null, 
        href: null,
        content: null,
        // A class to be applied to the root node in our template
        baseClass: "HTMLView",
        
        constructor: function (params) {  
        },
        
        startup: function () {
            this.inherited(arguments);
        },
        
        postCreate: function () {
            this.inherited(arguments);     	 
            if(this.href != null){
	            var parentNode = this.htmlNode;
	             xhr(this.href, {
	                handleAs: "text"
	            }).then(function(response) {
            		parentNode.innerHTML = response; 
	             });
            }else{
            	var node = domConstruct.toDom(this.content);
                domConstruct.place(node,this.htmlNode); 
            }
        },
    	
    	setProp : function(){  
      	},
    	
    	setData : function(contentParam){
    		this.content = contentParam;
     	},
     	
     	setCallback : function(functionName){
     		myCallback = functionName;
    	}
    });
});
 
 
