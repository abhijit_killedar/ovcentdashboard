define(
	["dojo/_base/declare", 
   	 "dojo/dom-construct" 
   	],  
   function(declare, domConstruct) {
	var customEventCallback = function(){ 
		 debugger;
		 listener.emit("customEvent", {data:"myData"});
	};
	
	var WidgetBaseController = declare(null, {	  
		createHeader: function(){ 
			parentView = new HTMLView({title:parentView.title },  parentView.baseNode);
			parentView.setProp();
 			return parentView;
		},
		
		setData : function(data, callbackListener){
			parentView.setData(data);
			parentView.setCallback(customEventCallback);
			listener = callbackListener;
		}
	});

	var listener = null;
	var parentView = null;
	var WidgetBaseController = new WidgetBaseController();
	return  WidgetBaseController; 
});