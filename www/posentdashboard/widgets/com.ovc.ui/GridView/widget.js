define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/Deferred",
	"dojo/has",
	"dojo/when",
	"dgrid/Grid", 
	"UI/TableView/widget",
	"dgrid/Keyboard", 
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
	"UI/RootView/widget",
 	"dijit/registry",
    "dojo/dom-class",
    "dojo/dom-style",
	"dojo/json", 
	"dojo/has!testing?dojo/text!UI/GridView/widget.html:dojo/text!UI/GridView.widget.html", 
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/Button",
	"dojox/mobile/SearchBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/ListItem",
	"dojox/mobile/EdgeToEdgeList",
	"dojo/_base/declare",
	"dojox/mobile/EdgeToEdgeCategory"
], function(declare, lang, Deferred, has, when, Grid, TableView, Keyboard, Selection,  Memory, Observable, OnDemandGrid,  
		RootView, registry, domClass, domStyle, JSON, template){
	  
	function createStatusFilter(){ 
		_widget.renderingDone = new Deferred(); 
		var deferred = new Deferred();
		if (!has("testing")) {
            when(require("generic/WidgetUtils").requireWidgets(["generic/OVCDialogMenuButton"], 
            		                        function(OVCDialogMenuButton) {
											 	deferred.resolve(OVCDialogMenuButton);
											}));
		} else {
			require(["generic/OVCDialogMenuButton/widget"], 
					function(OVCDialogMenuButton) {
						deferred.resolve(OVCDialogMenuButton);
					});
		}
		
		return deferred.then(lang.hitch(_widget, function(OVCDialogMenuButton) {
													_widget.statusFilter = new OVCDialogMenuButton().placeAt(_widget.alertSearch,"after");
													_widget.statusFilter.startup();
													domClass.add(_widget.statusFilter.button.domNode,"mblSimpleDialogButton");
													domClass.add(_widget.statusFilter.dialog.domNode,"GridView");
													_widget.statusFilter.dialog.set({top:10,left:10});
													domStyle.set(_widget.statusFilter.domNode,"display","inline"); 
													_widget.statusFilter.set("listDefinition",_widget.statusFilterDefinition);
													_widget.statusFilter.on("change", lang.hitch(_widget, function(e) {
														_widget.emit("statusChange", e);						
													})); 
													_widget.renderingDone.resolve();
											  }));
		
	};
	
	
	function createGrid(){
		var store = Observable(Memory({ data: _widget.alertData.items, idProperty: "orderNumber" }));
		_widget.alertGrid = new TableView( {orderData:_widget.alertData, 
		prototype : _widget.prototype}, _widget.alertGrid);
		_widget.alertGrid.startup();
		_widget.alertGrid.on("dgrid-select", lang.hitch(_widget,function(event){
		 	_widget.emit("AlertGridSelect", event);
		}));
		
		_widget.alertGrid.on("dgrid-select", lang.hitch(_widget,function(event){
			 	_widget.emit("AlertGridDeSelect", event);
		}));
	};
	
	var _widget = null;
	
    return declare("UI/GridView/widget",[RootView], {
	    allBtnLabel: "",
        pickupBtnLabel: "",
		statusFilterDefinition: [],
		alertData:[],
		prototype:{},
		
	    // Our template - important!
        templateString: template,

        // A class to be applied to the root node in our template
        baseClass: "GridView",

		buildRendering: function(){		
			this.inherited(arguments); 
            _widget =  this;
		},
		
		_pickupfilterClick: function(e){
			console.log("_pickupfilterClick is called()");
			this.emit("filterClick", {value: e.target.firstChild.data});
		},
				
		_pickupSearchChange: function(e){
			 if(e.keyCode == 13 && e.type == "keydown" && e.keyIdentifier == "Enter"){
					console.log("_pickupSearchChange is called()");
                 this.emit("searchChange", {value: e.srcElement.value});
             } 
		},
		 
		_closeAllStatusDialog: function() {
			console.log("_pickupSearchChange is called()");
			this.allStatus.hide();
		},
		
		postCreate: function() {
            this.inherited(arguments);			
  			createGrid();
			createStatusFilter();
			this.allToolBar.set("label",this.allBtnLabel);
			this.pickUpToolBar.set("label",this.pickupBtnLabel); 
	    }
	
    });
});

