define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/on",
	"dojo/dom-construct",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin",
	"dijit/_WidgetsInTemplateMixin",
    "dojo/has!testing?dojo/text!custome/RootView.widget.html:dojo/text!custome/RootView/widget.html",  
    "dojo/has!testing?custome/RootView.controller:custome/RootView/controller",  
 ], function(declare, lang, on, domConstruct, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin, 
			template, RootViewController){
 	var myCallback = function(){
		console.log("Callback has been called");
	};
  
    return declare("custome/RootView/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin ], {
        templateString: template,
        baseController : null,
         
        title: "",
        
        // A class to be applied to the root node in our template
        baseClass: "com.ovc.ui/RootView",
        
        constructor: function (params) { 
            params = params || {};
            console.log(this.baseClass+" constructor" + dojo.toJson(params));
            var tempHref = base + "/widgets/"+this.baseClass+"/widget.css"
		    console.log("Adding the class for "+this.baseClass+" href = " + tempHref);
			domConstruct.create("link", { rel: "stylesheet", href:  tempHref }, document.head);
		  	 
         },
        
        startup: function () {
            this.inherited(arguments);
        },
        
        postCreate: function () {
            this.inherited(arguments);
        },
    	
    	setProp : function(){  
      	},
    	
    	setData : function(){
     	},
     	
     	setCallback : function(functionName){
     		myCallback = functionName;
    	}
    });
});
 
 
