define(
        [ 
                "dojo","dojo/_base/declare", "dojo/_base/lang", "dojo/Deferred", "dojo/has", 
				"dojo/when", 
				"dojo/dom",
				"dojo/on", 
				
                "dojo/dom-class", 
                "dojo/dom-style",  
 				"dojo/query",  "dgrid/Grid",
                "dgrid/Keyboard", "dgrid/Selection", "dojo/store/Memory", "dojo/store/Observable",
                "dgrid/OnDemandGrid", "dijit/_WidgetBase", "dijit/_TemplatedMixin", "dijit/_OnDijitClickMixin",
                "dijit/_WidgetsInTemplateMixin", 
                "dojo/i18n!resource/nls/OVCAccordionView",
                "dijit/registry", "dojo/json",
                "dojo/has!testing?dojo/text!custome/AccordionView/widget.html:dojo/text!custome/AccordionView/widget.html",
 				"dojo/date/locale","dojox/mobile/Heading",
                "dojox/mobile/ToolBarButton", "dojox/mobile/Button", "dojox/mobile/Icon","dojox/mobile/SearchBox",
                "dojox/mobile/SimpleDialog", "dojox/mobile/ListItem", "dojox/mobile/EdgeToEdgeList",
                 "dojox/mobile/EdgeToEdgeCategory",
                "dojox/mobile/Accordion",
                "dojox/mobile/ContentPane",
                "dojox/mobile/RoundRectList",
               
                "dojox/mobile/parser",
                "dojox/mobile",
                "dijit/_Widget",
                "dijit/layout/ContentPane",
                "dojo/dom-construct",
                "dijit/form/TextBox",
				 "widgets/OVCManagementConsole/controller",
				 "widgets/OVCRolesList/controller",
				 "widgets/OVCUsersList/controller"],
        function(dojo, declare, lang, Deferred, has, when, dom, on, domClass, domStyle, query, Grid, Keyboard, Selection, Memory, Observable, OnDemandGrid,
                _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin, bundle, registry, JSON, template, locale, Button, Icon, ManagementController , RolesController, UsersListController)
        {
        	
        	var setDataInternal = function(data, accordion){ 
        		Object.keys(data).forEach(function(key){	
        			Object.keys(data[key]).forEach(function(index){
						if((index=="Devices") || (index=="Geräte")){
								var pane = new dojox.mobile.ContentPane({
										 label: "<i class = 'fa fa-"+index+"'></i>"+index,
										 selected:true//Dashboard
								 });	
						}
						else{
							var pane = new dojox.mobile.ContentPane({
										 label: "<i class = 'fa fa-"+index+"'></i>"+index,
										 
								 });
						
						}
        				accordion.addChild(pane);
        		     	var array = [];
        		     	array = data[key];
        		     				    			     	
        		     	Object.keys(array[index]).forEach(function(iter){
	     		     		var list = new dojox.mobile.RoundRectList();
	    				    list.placeAt(pane.containerNode);
	    				    list.startup();
	    				    var array2 = [];
	    				    array2 = array[index]; 
	    				    var item = new dojox.mobile.ListItem({label: array2[iter], id: iter}); 
	    				    if(iter == "id1"){
	    				    	item.set("class", "style1");
	    				    }
	    					list.addChild(item);
        		     	})//inner for
        		  	})//outer for
        		})//outermost for		
        	};
        	
		var accordion=null;
		 return declare(
				 "custome/AccordionView/widget",
				 [ _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin ],
				 {
						templatePath: dojo.moduleUrl("custome/AccordionView","widget.html"),
					
						data: "",
						//accordion: null,
						constructor: function() {
						},
						 
						postCreate: function() {
							 this.inherited(arguments);		
							this.reload();		
							
						},
						
						reload: function(){		
							accordion = this.AccordionNode;
							accordion.startup();		
						},
						
						setData: function(data){
							var accordion = this.AccordionNode;
							setDataInternal(bundle.data, accordion); 
						},
							
						accordionClick: function(e){
							domStyle.set(this.AccordionNode, "backgroundColor", "blue");
							//TODO Need to revisit
							var result = query(".mblRoundRectList li");
							for (i = 0; i < result.length; i++){
								console.log("result ["+i+"] = " + result[i].id);
								domClass.remove(result[i].id, "style1"); 
							}
							if(e.srcElement.childElementCount == 0){
								domClass.add(e.srcElement.parentElement.id, "style1");
								console.log("listItemClick:: e[srcElement] = " + e.srcElement.outerText); 
							} 
						}
					 
				 });
		
		
	});


