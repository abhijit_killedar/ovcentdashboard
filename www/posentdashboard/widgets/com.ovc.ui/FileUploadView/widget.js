define(
        [ 
        "dojo/_base/declare",
		 "dojo/when", 
		 "dojo/dom",
		 "dojo/on", 
		 "dojo/dom-construct",
         "dojo/dom-class", 
         "dojo/dom-style",  
         "dojo/i18n!resource/nls/OVCFileUploadView",
         "dojo/has!testing?dojo/text!custome/FileUploadView/widget.html:dojo/text!custome/FileUploadView/widget.html",
         "dijit/_WidgetBase",
		 "dijit/_TemplatedMixin",
		 "dijit/_OnDijitClickMixin",
		 "dijit/_WidgetsInTemplateMixin",
		 "dijit/registry",
		 "dijit/form/Button",
		 "dojox/form/Uploader", 
		 "dojox/form/uploader/FileList", 
		 "dojox/form/uploader/plugins/HTML5",
		 "custome/RootView/widget",
		 "widgets/OVCManagementConsole/controller",
		 "widgets/OVCRolesList/controller",
		 "widgets/OVCUsersList/controller"
		 ],
        function(declare, when, dom, on, domConstruct, domClass, domStyle, bundle, template,
        	_WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin , 
        	registry, FileUploader, 
        	RootView, ManagementController , RolesController, UsersListController)
        { 
			var fileUploadView=null;
		 	return declare(
				 "custome/FileUploadView/widget",
				 [ _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, _WidgetsInTemplateMixin  ],
				 { 
						templatePath: dojo.moduleUrl("custome/FileUploadView","widget.html"),					
						data: "", 
						pathToUploadServerScript:  null, 
						filemask : null,
						
						constructor: function(){ 
							var tempHref = base + "/lib/dojo/dojox/form/resources/FileUploader.css"
							domConstruct.create("link", { rel: "stylesheet", href:  tempHref }, document.head);
							var tempHref = base + "/widgets/com.ovc.ui/FileUploadView/widget.css"
							domConstruct.create("link", { rel: "stylesheet", href:  tempHref }, document.head);
						},
						
						postCreate : function() {
							this.inherited(arguments);	
							this.reload();																		 
  						},
						
						reload: function(){		
  						},
  						
  						setVisibility: function(isShow){
							if(isShow){
								domStyle.set(this.domNode, "display", "inherit"); 
							}
							else{
								domStyle.set(this.domNode, "display", "none");
							}
						},
						
						setData: function(data){ 
							if(data.URL==null || data.URL=="")
								data.URL = base + "/../../"+ bundle.URL;
							this.pathToUploadServerScript = data.URL; 
							this.myform.action = this.pathToUploadServerScript;
							/*debugger;
							if(data.filemask==null || data.filemask=="")
								data.filemask = bundle.filemask;
							this.filemask = data.filemask;
							//this.uploaderNode.fileMask = this.filemask; */
						},
							
						upload: function(e){
 						} 
				 }); 
		
	});


