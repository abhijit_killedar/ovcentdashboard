define(
	["dojo/_base/declare", 
		"dojo/request",
	 	"ec/util", 
   	 "dojo/dom-construct" 
   	],  
   function(declare,request, Utils, domConstruct) { 
	var reportWidget = null;
	var reportDetails = null;
	var mgmtconsole = null;
	
	var getLocations = function(){   
		 var inputParams = { 
					"entityType" : "Location", 
					"searchOperators" :["NOTEQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :["*"], 
					"searchType" : ["AND"] 
				}
		     
		 return Utils.execProcess("ObjectSearch", inputParams).then(function(data){
			 		debugger;
					console.log("in controller GetDevicePeripheral data = " + data);
					reportWidget.setLocationData(data);
		});  
	}; 

	
	var ReportController = declare(null, {	  
		setReport : function(widget,report){	 
			this.reportWidget = widget.reportWidget;
			reportWidget = widget.reportWidget;
			mgmtconsole = widget; 
			//getLocations();
			reportWidget.setData(null,null,report);
			return;
		}
	});
	

	
 
	var ReportController = new ReportController();
	return  ReportController; 
});