define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo",
	"dojo/date",
	"dojo/date/locale",
	"dojo/dom",
	"dojo/on",
    "custome/RootView/widget",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojox/mobile", 
    "dojox/mobile/deviceTheme", 
    "dojox/mobile/compat",
    "dojo/has!testing?dojo/text!widgets/OVCReport/widget.html:dojo/text!widgets/OVCReport/widget.html",  
	"dojo/i18n!resource/nls/OVCReport",
    "dojo/request/xhr",
    "widgets/OVCReport/controller" 
], function(declare, lang, dojo,date, locale,dom, on, RootView, domConstruct, domStyle,
			mobile, deviceTheme, compat, template, bundle,xhr, OVCReportController ){
 	var reportCallback = function(){
		console.log("Callback has been called");
	};
  
    return declare("widgets/OVCReport/widget",[RootView], {
    	templateString: template,
        baseController : null, 
        href: null,
        content: null,
        // A class to be applied to the root node in our template
        baseClass: "OVCReport",
        
        constructor: function (params) {  
        },
		
        testMethod: function (params) {  
        },
        
        startup: function () {
            this.inherited(arguments);
        },
        
        postCreate: function () {
            this.inherited(arguments);   
            this.reload();
        },
        
        reload: function(){
        	if(this.href != null){
	            var parentNode = this.htmlNode;
	             xhr(this.href, {
	                handleAs: "text"
					
	            }).then(function(response) {
            		parentNode.innerHTML = response; 
	             });
            }else{ 
				
				var src =  bundle.cfgReportServer + "frameset?__title=&__report=";
				            	src = src + bundle.cfgReportPath+bundle.Transactions;
            	var node = dojo.create("iframe", {
            		id: "iframeID",
                    "src": src,
                    "style": "border: 0; width: 100%; height: 100%"
                });  
                domConstruct.place(node,this.htmlNode); 
            }
        },
    	
    	setProp : function(){  
      	},
    	
    	setData : function(hrefParam, contentParam,report){
    		
    		debugger;
    		this.href = hrefParam;
    		this.content = contentParam;
    		if(this.href != null){
	            var parentNode = this.htmlNode;
	             xhr(this.href, {
	                handleAs: "text"
	            }).then(function(response) {
            		parentNode.innerHTML = response; 
	             });
            }else{
             	this.htmlNode.lastChild.remove();
				var src =  bundle.cfgReportServer + "frameset?__title=&__report=";
				
				if(report == 'transactions')
					{
						src = src + bundle.cfgReportPath+bundle.Transactions;
						
						var now = new Date();
				
				
						var utcDateTomo = new Date( now.getTime() + ((now.getTimezoneOffset()+2880) * 60000));
						var utcDate = new Date( now.getTime() + (now.getTimezoneOffset() * 60000));
						var toDate  = locale.format( utcDateTomo, {selector:"date", datePattern:"dd/MM/yyyy" });
						var monthBack =  date.add(now, "day", -30);
						var fromDate = locale.format( monthBack, {selector:"date", datePattern:"dd/MM/yyyy" });
						
						src = src + "&FromDate="+fromDate;
						src = src + "&ToDate="+toDate;
						
						
					}    
					else if(report == 'transactionItems')
							{
								src = src + bundle.cfgReportPath+bundle.TransactionItems;
						
								var now = new Date();
				
				
								var utcDateTomo = new Date( now.getTime() + ((now.getTimezoneOffset()+2880) * 60000));
								var utcDate = new Date( now.getTime() + (now.getTimezoneOffset() * 60000));
								var toDate  = locale.format( now, {selector:"date", datePattern:"dd/MM/yyyy" });
								var monthBack =  date.add(now, "day", -30);
								var fromDate = locale.format( monthBack, {selector:"date", datePattern:"dd/MM/yyyy" });
						
								//src = src + "&fromDate="+fromDate;
								//src = src + "&toDate="+toDate;
						
						
							}              	
				else if(report == 'tillTotals')
					{
						src = src + bundle.cfgReportPath+bundle.TenderTotalTills;
						
						var now = new Date();
				
				//var utcDateTS = new Date(now.toUTCString());
				
						var utcDateTomo = new Date( now.getTime() + ((now.getTimezoneOffset()+1440) * 60000));
						var toDate  = locale.format( now, {selector:"date", datePattern:"dd/MM/yyyy" });
						var dayBack =  date.add(now, "day", -1);
						
						var fromDate = locale.format( dayBack, {selector:"date", datePattern:"dd/MM/yyyy" });
						
						src = src + "&startDate="+fromDate;
						src = src + "&endDate="+toDate;
					}
				else if(report == 'Sales Summary'){
						src = src + bundle.cfgReportPath+bundle.SalesSummary;
						
						var now = new Date();
				
						//var utcDateTS = new Date(now.toUTCString());
				
						//var utcDateTomo = new Date( now.getTime() + ((now.getTimezoneOffset()+1440) * 60000));
						var toDate  = locale.format( now, {selector:"date", datePattern:"dd/MM/yyyy h:m:s a" });

						
						var dayBack =  date.add(now, "day", -1);
						
						dayBack.setHours(20);
						dayBack.setMinutes(0.0);
						dayBack.setSeconds(0.0);
						
						
						var fromDate = locale.format( dayBack, {selector:"date", datePattern:"dd/MM/yyyy h:m:s a" });
						
						src = src + "&fromDateTime="+fromDate;
						src = src + "&toDateTime="+toDate;

				}else if(report == 'JDExtract'){ 
						src = src + bundle.cfgReportPath+bundle.JDExtract;
						
						var now = new Date();
				
				
						var utcDateTomo = new Date( now.getTime() + ((now.getTimezoneOffset()+2880) * 60000));
						var utcDate = new Date( now.getTime() + (now.getTimezoneOffset() * 60000));
						var toDate  = locale.format( now, {selector:"date", datePattern:"dd/MM/yyyy" });
						var monthBack =  date.add(now, "day", -30);
						var fromDate = locale.format( monthBack, {selector:"date", datePattern:"dd/MM/yyyy" });
						
						src = src + "&fromDate="+fromDate;
						src = src + "&toDate="+toDate;
				}
				

				
				
					
				src = src + "&DBURL="+bundle.cfgReportDBURL;
				src = src + "&DBUserId="+bundle.cfgReportDBUserId;
				src = src + "&DBPassword="+bundle.cfgReportDBPassword;

				
             	var node = dojo.create("iframe", {
            		id: "iframeID",
                    "src": src,
                    "style": "border: 0; width: 100%; height: 85%"
                });  
                domConstruct.place(node,this.htmlNode);
            }
     	},
     	
     	setVisibility: function(isShow){
			if(isShow){
				domStyle.set(this.domNode, "display", "inherit"); 
			}
			else{
				domStyle.set(this.domNode, "display", "none");
			}
		},
     	
     	setCallback : function(functionName){
     		myCallback = functionName;
    	}
    });
});
 
 
