define([ 
    	"dojo/_base/array",
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dgrid/Grid", 
	"dgrid/Keyboard",  
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry",
	"dojo/dom",
    "dojo/dom-class",
    "dojo/dom-style",
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCDeviceDetails/widget.html:dojo/text!widgets/OVCDeviceDetails/widget.html",    
    "dojo/i18n!resource/nls/OVCDeviceDetails", 
	"dojox/mobile/Button",
	"dojox/mobile/EdgeToEdgeStoreList",
	"dojox/mobile/FilteredListMixin",
	"dojo/dom-construct",
	"dojo/ready",
	"dojox/mobile/RoundRectList",
	"dojox/mobile/ScrollableView",
	"dojox/mobile/ListItem",
	"dijit/registry",  
	"dojox/mobile/CheckBox",
	"dojox/mobile/TextBox",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dijit/form/ValidationTextBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/ContentPane",
	"dojox/mobile/EdgeToEdgeCategory"
], function(array, declare, lang, Grid, Keyboard, Selection,  Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, 
			registry, dom,domClass, domStyle, JSON, template, bundle, Button, EdgeToEdgeStoreList, FilteredListMixin, domConstruct,ready,RoundRectList, ScrollableView, ListItem, registry){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() {
			this._buttonClick(eventType, captureData);
		});
	}
	
	function createCheckBoxCorner(activeStr, activeVal, labelText, ParentNode){
		OVCDevicesCheckBoxContainer = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
		}, dojo.doc.createElement('div')) .placeAt(ParentNode);
 		createDynamicCheckBox(OVCDevicesCheckBoxContainer, activeVal, activeStr, bundle.active);
 		createDynamicCheckBox(OVCDevicesCheckBoxContainer, false, "ResetData", bundle.resetData);
	};
	
	function createDynamicCheckBox(parentNode, checked, id, labelText){  
		var myDiv = new dojox.mobile.ContentPane({class:"OVCinnerdiv"}, dojo.doc.createElement('div')) .placeAt(parentNode);
 		chckboxReset = new dojox.mobile.CheckBox({ 
			name: id, 
			label: id, 
			checked : checked,  
			id:"chkbx"+id
		}) .placeAt(myDiv); 
		chckboxReset.startup();  
		myDiv.domNode.appendChild(dojo.doc.createTextNode(labelText));
	};
	
	function createButtons(buttonDefinitions, parentNode){
		for (var i = 0; i < buttonDefinitions.length; i++) {
			var buttonDefinition = buttonDefinitions[i];
			var label = buttonDefinition.label;
			var eventType = buttonDefinition.eventType || buttonDefinition.label;
			var captureData = buttonDefinition.captureData;

			var button = new Button({label: label}).placeAt(parentNode.OVCDevicesContainers);
			button.startup();
			
			// use a function to pass the correct values of the params as we process the array
			// @see makeButtonEvent() 
			button.on("click", makeButtonEvent(eventType, captureData, parentNode));
			 
		}
	};
	
	function createDynamicSearchBox(parentNode, labelText, searchId, searchText){
		console.log(store);  
		var OVCDeviceDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"}, dojo.doc.createElement('div')) .placeAt(parentNode);
		
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
			}, dojo.doc.createElement('label')) .placeAt(OVCDeviceDetailsform);
		
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
		debugger;
		searchBox = new dojox.mobile.SearchBox( {
													id:"txtbx"+searchId,
													selectOnClick:true,
													type:"search",
													placeHolder:searchText,
													incremental:true,
													store:store,
													searchAttr: "label",
													value:"",
													ignoreCase: true,
													onSearch:onSearch
												}
												); 
		searchBox.placeAt(OVCDeviceDetailsform);
		
		var OVCDeviceDetails_div = new dojox.mobile.ContentPane({id:"scrollableView"}, dojo.doc.createElement('div')).set('data-dojo-type','dojox/mobile/ScrollableView').placeAt(OVCDeviceDetailsform);
		var ul  = new RoundRectList({}, domConstruct.create("ul",{id:"list"}, parentNode) );
		ul.set("data-dojo-type","dojox/mobile/RoundRectList");
		ul.startup();		
		ul.placeAt(OVCDeviceDetails_div);
	};
	
	function createDynamicTextBox(parentNode, labelText, id, boxText, disabled){
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
	
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
		
		var textbox = new dojox.mobile.TextBox({
			name: id,
			label: id,
			id: "txtbx"+id,
			value: boxText,
			disabled : (disabled !=null) ? disabled : false 
		},dojo.doc.createElement('input')).placeAt(parentNode);
		OVCeditlabel.startup();
		textbox.startup();
	};
	 
	
	function createTextBoxCorner(devicetypeDefinition, parentNode){
 		var OVCDeviceDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
		}, dojo.doc.createElement('div')) .placeAt(parentNode);
		debugger;
		if(j == "printerIP"){ 
 			var appendIndex = devicetypeDefinition.length>1;
 			for(i in devicetypeDefinition){
				var OVCDeviceDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
				}, dojo.doc.createElement('div')) .placeAt(parentNode);
			    
				createDynamicTextBox(OVCDeviceDetailsform, (appendIndex ? bundle.printerAddress+" "+i :  bundle.printerAddress) +":", i, devicetypeDefinition[i]);
			}
		}else if(j == "cashDrawerIP"){ 
 			var appendIndex = devicetypeDefinition.length>1;
 			for(i in devicetypeDefinition){
				var OVCDeviceDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
				}, dojo.doc.createElement('div')) .placeAt(parentNode);
			    
				createDynamicTextBox(OVCDeviceDetailsform, (appendIndex ? bundle.cashDrawerAddress+" "+i :  bundle.cashDrawerAddress) +":", j, devicetypeDefinition[i]);
			}
		}else if(j == "locationId"){
			createDynamicSearchBox(parentNode, bundle.locationId, j, devicetypeDefinition);
		} else if(j == "alias"){ 
			createDynamicTextBox(OVCDeviceDetailsform, bundle.deviceAlias+":", j, devicetypeDefinition);
		} else if(j == "dUUID"){ 
			debugger;  
			var splitArr = devicetypeDefinition.split("|");
			for(i in splitArr){ 
				console.log("dduid splitArr = " + splitArr[i]);
			}
			createDynamicTextBox(OVCDeviceDetailsform, bundle.deviceType, j, splitArr[0], true);
			var OVCDeviceDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
			}, dojo.doc.createElement('div')) .placeAt(parentNode);
			createDynamicTextBox(OVCDeviceDetailsform, bundle.deviceVersion, "Version", splitArr[1],  true);
		}
	};
	
	
	function resetData(){ 
 		var widget = registry.byId("txtbx0").set("value", "")
 		var widget = registry.byId("txtbxcashDrawerIP").set("value", "")
		var widget = registry.byId("txtbxlocationId").set("value", "");;
		var widget = registry.byId("txtbxalias").set("value", "");; 
		var widget = registry.byId("txtbxdUUID").set("value", "");; 
		var widget = registry.byId("txtbxVersion").set("value", "");;  
		var widget = registry.byId("chkbxisActive").set("checked", false);; 		
	};

	
	var chckboxActive = null;
	
	var store;
	
	var chckboxReset = null;
	 
	var OVCDevicesCheckBoxContainer = null;
	
	var OVCDeviceDetailsform = null;
	
	return declare("widgets/OVCDeviceDetails/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
	  
 		buttonDefinitions: [],
 		devicetypeDefinitions: [],
 		activeDefinitions: [],  
        templateString: template, 
        id: null,
         
        setID: function(temp){
        	this.id = temp;
        },
        
        constructor: function(){
             this.inherited(arguments); 
  			 store = new Memory({data: []});
  			
  			 onSearch = function(results, query, options){
   				var list = registry.byId("list");				
  				if(query.label == '*' || query.label == 'undefined'){
  					dom.byId("list").style.visibility = "hidden";
  				}else{
  					dom.byId("list").style.visibility = "visible";
  				}
  				if(options.start == 0){
  				  list.destroyDescendants();
  				}
  				
  			array.forEach(results, function(item){
  					if(query.label != '*' && query.label != 'undefined'){	
  						list.addChild(new ListItem({id: item.id,label: item.label, value:item.value,clickable : true,
  						onClick : function() {
  							debugger;
  							var searchres = registry.byId("txtbxlocationId");
  							searchres.set('value',this.value);
  							dom.byId("list").style.visibility = "hidden";
  						}}));
  					}	
  				}); 
  			};
        },
         
        getTBData: function(widgetId){   
         	var widget = registry.byId(widgetId);
         	var value = widget.get("value");
         	if(value == null)
         		value = "";

        	return value;
        },
        
        getCBData: function(widgetId){
        	debugger;
        	var widget = registry.byId(widgetId); 
    		if(widget.id == "chkbxisActive"){
    			return widget.get("checked") == true ? "1" : "0";
         	}else{
         		return widget.get("checked") == true ? "true" : "false";
         	} 
        },
        
        reload: function(){  
			debugger;
			var deviceData = bundle; 
			console.log(deviceData);
	  		console.log("deviceData = " + deviceData.SearchObjectList);
			for (j in deviceData.SearchObjectList) {  
				var devicetypeDefinition = deviceData.SearchObjectList[j];
 				createTextBoxCorner(devicetypeDefinition, this.OVCDevicesContainers); 
			} 
 			var activeStr = "isActive";
			var activeVal = deviceData.SearchObjectList[activeStr] == 0 ? true : false;
			createCheckBoxCorner(activeStr, activeVal, "Active", this.OVCDevicesContainers);
			createButtons(deviceData.buttonDefinitions, this);  
        },
        
        setLocationData: function(locationData){  
        	var items = [];
        	debugger;      
        	for(i in locationData.SearchObjectList){
        		var element = locationData.SearchObjectList[i];
        		items.push({id:element.id,label: element.id, value:element.id});
        		items.push({postalCodeId:element.id,label: element.postalCode, value:element.id});
        		items.push({townId:element.id,label: element.town + " "+ element.streetName, value:element.id});
        		items.push({streetNameId:element.id,label: element.streetName, value:element.id});
        	}

 			var widget = registry.byId("txtbxlocationId"); 
        	store = new Memory({data: items});
        	widget.set("store", store);
        },
        
        setData: function(deviceData){   
         	resetData();
			debugger;
        	for (j in deviceData.devicePeripheralObj) {
 		  		console.log("deviceData = " + deviceData.devicePeripheralObj[j]);
//        		widget.set("value", "");
				var devicetypeDefinition = deviceData.devicePeripheralObj[j]; 
	        	if(j == "printerIP"){  
	        		
		 			for(i in devicetypeDefinition){
		     			var widget = registry.byId("txtbx"+i); 
		                widget.set("value", devicetypeDefinition[i]);
		    			if(devicetypeDefinition.length>1){
		    				break;
		    			}
					}
	    		}else if(j == "cashDrawerIP"){  
	        		
		 			for(i in devicetypeDefinition){
		     			var widget = registry.byId("txtbx"+j); 
		                widget.set("value", devicetypeDefinition[i]);
		    			if(devicetypeDefinition.length>1){
		    				break;
		    			}
					}
	    		}
				else if(j == "locationId"){
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		} else if(j == "alias"){ 
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		} else if(j == "dUUID"){ 
	    			var splitVal = devicetypeDefinition.split("|");
	    			if(splitVal.length >= 1){
		     			var widget = registry.byId("txtbx"+j); 
		            	widget.set("value", splitVal[0]);
	    			}
	    			if(splitVal.length >= 4){
	    				var widget = registry.byId("txtbxVersion"); 
	     				widget.set("value",  splitVal[3]); 
	    			}
	    		} else if(j == "isActive"){ 
	     			var widget = registry.byId("chkbx"+j); 
	            	widget.set("checked", devicetypeDefinition == 1 ? true : false);
	    		}
        	}
		},
		        
        // A class to be applied to the root node in our template
        baseClass: "OVCDeviceDetails",


		_buttonClick: function(eventType, captureData) {
			// object to emit
			var event = {};
	
			if (captureData && this.getChildren()[0]) {
				var children = this.getChildren()[0].getChildren();
				for (var i = 0; i < children.length; i++) {
					var child = children[i];
					switch (child.declaredClass) {
					case "dojox.mobile.ComboBox":
					case "dojox.mobile.TextBox":
					case "dojox.mobile.ExpandingTextArea":
					case "dojox.mobile.Slider":
						event[child.name] = child.value;
						break;
					case "dojox.mobile.CheckBox":
					case "dojox.mobile.ToggleButton":
						event[child.name] = child.checked;
						break;
					case "dojox.mobile.RadioButton":
						if (child.checked) {
							event[child.name] = child.value;
						};
						break;
					case "dojox.mobile.RoundRectList":
					case "dojox.mobile.EdgeToEdgeList":
						findSelectedItems(child, event);
						break;
					}
				}
			}

			// emit eventType
			this.emit(eventType, event);
		},
		
		setVisibility: function(isShow){
	        if(isShow){
        		//domStyle.set(this.domNode, "visibility", "visible");
        		domStyle.set(this.domNode, "display", "block");
	        }
        	else{
        		domStyle.set(this.domNode, "display", "none");
        		//domStyle.set(this.domNode, "visibility", "hidden");
	        } 
        },
        
		postCreate: function() {
 			var heading = new dojox.mobile.Heading({
			  dojoType: "dojox.mobile.Heading",
			  moveTo: "mblFormLayoutTitle",
			  fixed: "top",
			  label: bundle.editDeviceHeader
			}).placeAt(this.titleNode);

			var OVCDeviceDetailsform = new dojox.mobile.ContentPane({class:"OVCcheckerdiv"
			}, dojo.doc.createElement('div')) .placeAt(this.OVCDevicesContainers);
			
			this.reload(); 					
 		}	 
	});
});
