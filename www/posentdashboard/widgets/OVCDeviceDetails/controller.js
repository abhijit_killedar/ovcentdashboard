define(
	["dojo/_base/declare", 
	 "dojo/_base/lang",
	 "ec/util", 
   	 "widgets/OVCDeviceDetails/widget", 
	 "widgets/OVCDeviceSearch/controller"
   	],  
   function(declare, lang, Utils, OVCDeviceDetails, DeviceSearchController) {
		  
	var getDeviceByID = function(id){ 
		 var inputParams = { 
					"deviceId" : id 
				}; 
		 return Utils.execProcess("GetDevicePeripheral", inputParams).then(function(data){
			 debugger;
			 																			for(i in data)
			 																				console.log("in controller GetDevicePeripheral data["+i+"] = " + data[i]);
																							console.log(data);
			 																			if(Utils.handleError(data.returnCode)){
			 																				deviceDetails.setData(data);
			 																			} else{ 
			 																				debugger;
			 																				console.log("in controller GetDevicePeripheral error");
			 																				Utils.showProgIndDlg("Error", data.returnMsg);
			 																			}
			 																}); 
	}; 
	
	
	var getLocations = function(){   
		 var inputParams = { 
					"entityType" : "Location", 
					"searchOperators" :["NOTEQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :["*"], 
					"searchType" : ["AND"] 
				}
		     
		 return Utils.execProcess("ObjectSearch", inputParams).then(function(data){
			 		debugger;
					console.log("in controller GetDevicePeripheral data = " + data);
					deviceDetails.setLocationData(data);
		});  
	}; 

	var saveToDeviceDB = function(inputParams, lastSelected){ 
		for(i in inputParams)
				console.log("in controller AddUpdateDevicePeripheral inputParams["+i+"] = " + inputParams[i]); 
 		return Utils.execProcess("AddUpdateDevicePeripheral", inputParams).then(function(data){
 																					debugger;
 																					for(i in data)
																					console.log(data);
		 																				console.log("in controller AddUpdateDevicePeripheral data["+i+"] = " + data[i]); 
 																					if(Utils.handleError(data.returnCode)){ 
 																						resetToDB(lastSelected);
		 																			} else{
		 																				debugger; 
		 																				console.log("in controller AddUpdateDevicePeripheral error")
		 																				Utils.showProgIndDlg("Error", data.returnMsg);
		 																			} 
 																				}); 
	};
	
	var navigateToDeviceListing = function(){ 
		if(deviceDetails!=null){
			deviceDetails.setVisibility(false);  
			DeviceSearchController.resettoDB(); 
		}
	};
	
	var resetToDB = function(lastSelected){  
		if(deviceDetails!=null)
			deviceDetails.setVisibility(false); 
		if(managementController!=null)
			managementController.resetToDB();  
		else	
			DeviceSearchController.resettoDB(lastSelected); 
	};
	  
	 
	var deviceDetails = null;
	var managementController = null;
	
	var DeviceDetailsController = declare(null, {	 
		initializeDeviceDetails: function(controller){
			debugger;
			if(deviceDetails==null){   
				console.log(controller);
				managementController = controller;
				deviceDetails = new OVCDeviceDetails(); 
				deviceDetails.on("save",  function(){
													debugger;
													var inputParams = { 	
															"deviceId" :  deviceDetails.id, 
															"deviceAlias": deviceDetails.getTBData("txtbxalias") ,
															"locationId" :deviceDetails.getTBData("txtbxlocationId"),
															"printerIP": deviceDetails.getTBData("txtbx0"),
															"cashDrawerIP": deviceDetails.getTBData("txtbxcashDrawerIP"),
															"deviceActive": deviceDetails.getCBData("chkbxisActive"),
															"resetDevice": deviceDetails.getCBData("chkbxResetData")
														};
													saveToDeviceDB(inputParams);
													});
				deviceDetails.on("cancel",  resetToDB); 
 				deviceDetails.startup();
			}
			return deviceDetails;
		},
		
		setDevice: function(id){
			deviceDetails.setID(id);
			getDeviceByID(id);
			getLocations();
			deviceDetails.setVisibility(true);
		}, 
		
		saveToDeviceDB : saveToDeviceDB,
		navigateToDeviceListing : navigateToDeviceListing,
		resetToDB : resetToDB
	}); 
	var DeviceDetailsController = new DeviceDetailsController();
	return  DeviceDetailsController;
	
});
