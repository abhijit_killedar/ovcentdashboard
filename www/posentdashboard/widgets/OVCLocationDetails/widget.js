var colorMemoryStore ;
define([ 
    	"dojo/_base/array",
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/on",
	"dgrid/Grid", 
	"dgrid/OnDemandList",
	"dgrid/Selection",
	"dgrid/Keyboard",  
	"dojo/data/ObjectStore",
	"dojo/data/ItemFileReadStore",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry",
	//"dijit/Dialog",
	"dojo/dom",
    "dojo/dom-class",
	"dojo/dom-attr",
	"dojo/query",
    "dojo/dom-style",
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCLocationDetails/widget.html:dojo/text!widgets/OVCLocationDetails/widget.html",
	"dojo/i18n!resource/nls/OVCLocationDetails", 
	"dojox/mobile/Button",
	"dojox/mobile/EdgeToEdgeStoreList",
	"dojox/mobile/FilteredListMixin",
	"dojo/dom-construct",
	"dojo/ready",
	"dojox/mobile/RoundRectList",
	"dojox/mobile/ScrollableView",
	"dojox/mobile/ListItem",
	"dojox/mobile/ComboBox",
	"dojox/mobile/CheckBox",
	"dojox/mobile/TextBox",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dojox/form/DropDownSelect",
	"dijit/form/ComboBox",
	//"dijit/form/Select",
	"dijit/form/FilteringSelect",
	"dijit/form/ValidationTextBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/ContentPane",
	"dojox/mobile/EdgeToEdgeCategory"
], function(array, declare, lang, on, Grid,  List, Selection, Keyboard, ObjectStore, ItemFileReadStore, Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, registry, dom, domClass, domAttr, query, domStyle, JSON, template, bundle, Button, EdgeToEdgeStoreList, FilteredListMixin, domConstruct, ready, RoundRectList, ScrollableView, ListItem, ComboBox, DropDownSelect, FilteringSelect){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() {
			this._buttonClick(eventType, captureData);
		});
	}
	
	 var DropDown = declare([List, Selection, Keyboard]);
	 
	
	function createDropDownSelectCorner(parentNode,labelText,id,selecteddata){ 
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
	
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
	
		var OVCnewdiv = new dojox.mobile.ContentPane({class:"mySelect",id:"select"
		}, dojo.doc.createElement('div')).placeAt(parentNode);
		
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"label button",id:"selectlabel",innerHTML:selecteddata
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
			
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"arrow button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
															
		var store = new Memory({ identifier:"id", 

					data: [
						  { id: "store", name: "Store" ,value: "Store"},
						  { id: "warehouse", name: "Warehouse" ,value:"Warehouse"},
						  { id: "corporate", name: "Corporate",value: "Corporate"}
						]
					  });
			
		function renderItem(item){
						var divNode, textNode;
						textNode = document.createTextNode(item.name);
						divNode = domConstruct.create("div", {
							"class": item.name
						});
						domConstruct.place(textNode, divNode);
						return divNode;
		}
 
		var dropDown = new DropDown({
				selectionMode: "single",
				store: store,
				//value:selecteddata,
				renderRow: renderItem		
		});
		domConstruct.place(dropDown.domNode, "select");
			dropDown.startup();

		var open = false;
				function toggle(state){
						open = state;
						domClass[open ? "add" : "remove"](dropDown.domNode, "opened");
				}
				on(dom.byId("select"), ".button:click", function(e){
						toggle(!open);
				});
			
		var label = query(".label", dom.byId("select"))[0];
					dropDown.on("dgrid-select", function(e){
						var node = renderItem(e.rows[0].data);
						domConstruct.place(node, label, "only");
						toggle(false);
					});
			
	};
	
	function createButtons(buttonDefinitions, parentNode){
		for (var i = 0; i < buttonDefinitions.length; i++) {
			var buttonDefinition = buttonDefinitions[i];
			var label = buttonDefinition.label;
			var eventType = buttonDefinition.eventType || buttonDefinition.label;
			var captureData = buttonDefinition.captureData;

			var button = new Button({label: label}).placeAt(parentNode.OVCLocationsbuttonsContainer);
			button.startup();
			
			// use a function to pass the correct values of the params as we process the array
			// @see makeButtonEvent() 
			button.on("click", makeButtonEvent(eventType, captureData, parentNode));
			 
		}
	};
	

	
	function createDynamicTextBox(parentNode, labelText, id, boxText, disabled){
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
	
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
		
		var textbox = new dojox.mobile.TextBox({
			name: id,
			label: id,
			id: "txtbx"+id,
			value: boxText,
			disabled : (disabled !=null) ? disabled : false 
		},dojo.doc.createElement('input')).placeAt(parentNode);
		OVCeditlabel.startup();
		textbox.startup();
	};
	
	
	function createDynamicPassTextBox(parentNode, labelText, id, boxText, disabled){
		var OVCeditlabel1 = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
		
		parentNode.innerHTML = "<input type='password' />";
		var inpassw = parentNode.firstchild;
	
		OVCeditlabel1.domNode.appendChild(dojo.doc.createTextNode(labelText));
		
		var textbox = new dojox.mobile.TextBox({
			type:'password',
			name:"password",
			label: id,
			id: "txtbx"+id,
			value: boxText,
			disabled : (disabled !=null) ? disabled : false 
			//disabled : (disabled !=null) ? disabled : false 
		},inpassw).placeAt(parentNode);
		OVCeditlabel1.startup();
		textbox.startup();
	};
	 
	
	function createTextBoxCorner(devicetypeDefinition, parentNode){
 		var OVCLocationDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
		}, dojo.doc.createElement('div')) .placeAt(parentNode);
	  
 		//SIM-140 Making text boxes disabled for PHASE-1
 		
		if(j == "id"){ 
 			createDynamicTextBox(OVCLocationDetailsform, "Location ID"+":", j, devicetypeDefinition,"disabled");
		}
		else if(j == "displayName"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Location Name"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "streetName"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Street Name"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "streetNumber"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Street Number"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "postalCode"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Postal Code"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "town"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Town"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "country"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Country"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "phoneNumber"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Phone Number"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "faxNumber"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Fax Number"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "locationType"){ 
			createDynamicTextBox(OVCLocationDetailsform, "Location Type"+":", j, devicetypeDefinition,"disabled");
		}
	};
	
	
	function resetData(){  
		registry.byId("txtbxdisplayName").set("value", ""); 
		registry.byId("txtbxlocationType").set("value", ""); 
		registry.byId("txtbxstreetName").set("value", ""); 
		registry.byId("txtbxid").set("value", ""); 
		registry.byId("txtbxstreetNumber").set("value", "");  
		registry.byId("txtbxpostalCode").set("value", ""); 
		registry.byId("txtbxtown").set("value", "");  
		registry.byId("txtbxcountry").set("value", ""); 
		registry.byId("txtbxphoneNumber").set("value", "");   
		registry.byId("txtbxfaxNumber").set("value", "");    
	};
	
	var chckboxActive = null;
	 
	var chckboxReset = null;
	 
	var OVCLocationsCheckBoxContainer = null;
	
	var OVCLocationDetailsform = null;
	
	return declare("widgets/OVCLocationDetails/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
	  
 		buttonDefinitions: [],
 		devicetypeDefinitions: [],
 		activeDefinitions: [],  
        templateString: template, 
        id: null,
         
        setID: function(temp){
        	this.id = temp;
        }, 
        
        constructor: function(){
             this.inherited(arguments);  
        },
         
        getTBData: function(widgetId){   
         	var widget = registry.byId(widgetId);
			var value = widget.get("value");
         	if(value == null)
         		value = ""; 
        	return value;
        },
		  
        reload: function(){   
			var heading = new dojox.mobile.Heading({
			  dojoType: "dojox.mobile.Heading",
			  moveTo: "mblFormLayoutTitle",
			  fixed: "top",
			  label: "Edit Location Detail"
			}).placeAt(this.titleNode);

			var OVCLocationDetailsform = new dojox.mobile.ContentPane({class:"OVCcheckerdiv"
			}, dojo.doc.createElement('div')) .placeAt(this.OVCLocationsContainers);					
				 
			var deviceData = bundle; 
			console.log(deviceData);
			console.log("deviceData = " + deviceData.SearchObjectList);
		
			for (j in deviceData.SearchObjectList) {   
					var devicetypeDefinition = deviceData.SearchObjectList[j];
					createTextBoxCorner(devicetypeDefinition, this.OVCLocationsContainers);  
			} 
			createButtons(deviceData.buttonDefinitions, this); 
		},
					  
        setData: function(deviceData){   
         	resetData();
         	debugger;
        	for (j in deviceData.SearchObjectList[0]) {
 		  		console.log("deviceData = " + deviceData.SearchObjectList[0]);
 				var devicetypeDefinition = deviceData.SearchObjectList[0][j]; 
	        	if(j == "id"){   
	             	debugger;
	        		var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "streetNumber"){  
	             	debugger;
	    			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		} else if(j == "displayName"){
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		} else if(j == "streetName"){ 
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "postalCode"){ 
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "town"){ 
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "country"){ 
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "phoneNumber"){ 
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "faxNumber"){ 
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "locationType"){ 
	             	debugger;
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}  
        	}
		},
		        
        // A class to be applied to the root node in our template
        baseClass: "OVCLocationDetails",


		_buttonClick: function(eventType, captureData) {
			// object to emit
			var event = {};
	
			if (captureData && this.getChildren()[0]) {
				var children = this.getChildren()[0].getChildren();
				for (var i = 0; i < children.length; i++) {
					var child = children[i];
					switch (child.declaredClass) {
					case "dojox.mobile.ComboBox":
					case "dojox.mobile.TextBox":
					case "dojox.mobile.ExpandingTextArea":
					case "dojox.mobile.Slider":
						event[child.name] = child.value;
						break;
					case "dojox.mobile.CheckBox":
					case "dojox.mobile.ToggleButton":
						event[child.name] = child.checked;
						break;
					case "dojox.mobile.RadioButton":
						if (child.checked) {
							event[child.name] = child.value;
						};
						break;
					case "dojox.mobile.RoundRectList":
					case "dojox.mobile.EdgeToEdgeList":
						findSelectedItems(child, event);
						break;
					}
				}
			} 
			// emit eventType
			this.emit(eventType, event);
		}, 

		setVisibility: function(isShow){
	        if(isShow)
        		domStyle.set(this.domNode, "display", "inherit");
        	else
        		domStyle.set(this.domNode, "display", "none");
        },
        
		postCreate: function() { 
			this.inherited(arguments); 
			this.reload();  
 		}	 
	});
});
