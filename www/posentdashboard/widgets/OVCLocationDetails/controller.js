define(
	["dojo/_base/declare", 
	 "dojo/_base/lang",
	 "ec/util", 
	 "widgets/OVCLocationDetails/widget",
   	 "widgets/OVCManagementConsole/controller",
	 "widgets/OVCLocationList/controller"
   	 
   	],  
   function(declare, lang, Utils,  OVCLocationDetails, ManagementController, LocationListController ) {
	var getLocationByIDCallback = function(data){  
		debugger;
		for(i in data)
			console.log("in controller getLocationByIDCallback data["+i+"] = " + data[i]);
 		if(Utils.handleError(data.returnCode)){
			LocationDetails.setData(data);
		} else{ 
			debugger;
			console.log("in controller GetLocationPeripheral error");
			Utils.showProgIndDlg("Error", data.returnMsg);
		} 
	}; 
	
	var getLocationByID = function(id){
		debugger;
		 var inputParams = { 
					"entityType" : "Location", 
					"searchOperators" :["EQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :[id], 
					"searchType" : [""] 
				}
		  
		 return Utils.execProcess("ObjectSearch", inputParams).then(getLocationByIDCallback);  
	};
	

	var saveToLocationDB = function(inputParams){ 
		debugger;
		for(i in inputParams)
				console.log("in controller AddUpdateLocationPeripheral inputParams["+i+"] = " + inputParams[i]); 
			return Utils.execProcess("AddUpdateLocation", inputParams).then(function(data){
						debugger;
						for(i in data)
							console.log("in controller AddUpdateLocationPeripheral data["+i+"] = " + data[i]); 
						if(Utils.handleError(data.returnCode)){ 
							resetToDB();
						} else{
							debugger; 
							console.log("in controller AddUpdateLocationPeripheral error")
							Utils.showProgIndDlg("Error", data.returnMsg);
						} 
 			}); 
	};
	
	var navigateToLocationListing = function(){  
		debugger;
		if(LocationDetails!=null){
			LocationDetails.setVisibility(false); 
			LocationListController.navigateToLocationListing();  
		}
	};
	
	var resetToDB = function(){  
		debugger;
		LocationDetails.setVisibility(false); 
		LocationListController.navigateToLocationListing(); 
		//LocationListController.resetToDB(); 
	};
	  
	 
	var LocationDetails = null;
	var LocationListController=null;
	var LocationDetailsController = declare(null, {	 
			initializeLocationDetails: function(controller){
			debugger;
			if(LocationDetails==null){   
				//managementController = controller;
				LocationListController=controller;
				LocationDetails = new OVCLocationDetails(); 
				
				LocationDetails.on("save",  function(){  
													var inputParams = { 	
															"locationId" :  LocationDetails.getTBData("txtbxid"),
															"locationType" :  LocationDetails.getTBData("txtbxlocationType"),
															"locationName": LocationDetails.getTBData("txtbxdisplayName") ,
															"streetName" :LocationDetails.getTBData("txtbxstreetName"),
															"streetNumber": LocationDetails.getTBData("txtbxstreetNumber"),
															"postalCode": LocationDetails.getTBData("txtbxpostalCode"),
															"town": LocationDetails.getTBData("txtbxtown"),
															"country": LocationDetails.getTBData("txtbxcountry"),
 															"phoneNumber": LocationDetails.getTBData("txtbxphoneNumber"),
															"faxNumber": LocationDetails.getTBData("txtbxfaxNumber"),
 													};
													saveToLocationDB(inputParams);
													});
				LocationDetails.on("cancel", navigateToLocationListing); 	
				LocationDetails.on("delete", navigateToLocationListing); 
  				LocationDetails.startup();
			}

			return LocationDetails;
		},
		setLocation: function(id){
			LocationDetails.setID(id);
			getLocationByID(id);
			LocationDetails.setVisibility(true);
		}, 
		
		
		saveToLocationDB : saveToLocationDB,
		navigateToLocationListing : navigateToLocationListing,
		resetToDB : resetToDB
	}); 
	var LocationDetailsController = new LocationDetailsController();
	return  LocationDetailsController;
	
});
