//var setGridRowDataFormatter;
define([
    "dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/dom",
	"dojo/on",
	"dojo/_base/window",
	"dojo/query",
	"dgrid/Grid", 
	"dgrid/Keyboard", 
	"widgets/OVCManagementConsole/controller",
	"dgrid/Selection",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry",
    "dojo/dom-class",
    "dojo/dom-style",
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCUsersList/widget.html:dojo/text!widgets/OVCUsersList/widget.html",
	 "dojo/i18n!resource/nls/OVCUsersList",
	 "custome/BoxContainer/widget",
	"dojox/mobile/Button",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/ListItem",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/ContentPane",
	"dojox/mobile/EdgeToEdgeCategory"
	], function(declare, lang, dom, on, win, query, Grid, Keyboard,/* ManagementConsole,*/ ManagementController , Selection,  Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, registry, domClass, domStyle, JSON, template, bundle, BoxContainer, Button){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
 
	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() { 
			this._buttonClick(eventType, captureData);
 		});
	}
	 
	function optimalPush(array, value){ 
		debugger;
		var temp = array[value.userId];
		if(temp == null){
			array[value.userId] = value;
		}else{
			array[value.userId] = lang.mixin(temp, value); 
		} 
	}
	
    return declare("widgets/OVCUsersList/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
  		buttonDefinitions: [],
		buttonArray: [],	
		selectedElements: {},	
 		SearchObjectList:[],
        onEditCallback: null,
        onButtonClickCallback: null,
		
		// Our template - important!
        templateString: template,
        
        gridUsersListWidget: null,
        
        constructor: function(){
        },
		
		// A class to be applied to the root node in our template
        baseClass: "OVCUsersList",
		
		setProps : function(){
			data = bundle;
			var widgetBoxContainer = new BoxContainer({ widgetDefinitions: bundle.userbuttonDefinitions});
            widgetBoxContainer.placeAt(this.usersButtonContainer);
            widgetBoxContainer.buttonClickEvent = lang.hitch(this, function(eventType){
				debugger;
				this.onButtonClickCallback(eventType, this.selectedElements);
			}); 
 	    },
	 
		setData: function(data){	 
			debugger;
			this.j = this.j +1;
			console.log(data); 
			console.log("setData j = " + this.j);
			// Create a new constructor by mixing in the components
			var CustomGrid = declare([ Grid, Keyboard, Selection ]);
			var usersList = data.usersList;
			var store = new Memory({ data: usersList });
			 
			this.gridUsersListWidget.renderArray(usersList); 
 			this.gridUsersListWidget.on(".dgrid-row:click", lang.hitch(this, function(event){
				this.i = this.i +1; 
				console.log("dgrid-select i = " + this.i + " j = " + this.j);
					// Report the item from the selected row to the console.
					if(this.i%(this.j)==0)
						this.onClickCallback(event);
				}));
				
		},
	       
		i: 0,
	    j: 0,
		   
		onClickCallback : function(event) {    
			debugger;
			if(event.srcElement.type == "checkbox"){  
				//event.srcElement.checked = !event.srcElement.checked;
				debugger;
				var str = event.srcElement.id.split("|")[0];
				var userid = event.srcElement.id.split("|")[1];	
				var firstname = event.srcElement.id.split("|")[2];
				var lastname = event.srcElement.id.split("|")[3];
				var username = event.srcElement.id.split("|")[5]; 
				var email = event.srcElement.id.split("|")[4]; 
				var inputParams = { 	
						"userId" : userid, 
						"userName" : username, 
						"firstname": firstname,
						"lastname" : lastname== "null" ? "":lastname , 
						"email": email== "null" ? "":email  
					};
			
				if(str == "active"){  
					inputParams.active = event.srcElement.checked? "1" : "0"; 
				}
				optimalPush(this.selectedElements, inputParams);
				console.log("i = " + this.i + " this.selectedElements = " + this.selectedElements);
				this.i = 0;   
			}else if(event.srcElement.type == "submit"){ 
 				if(event.srcElement.defaultValue == "Edit"){ 
					console.log("inside Click ",event.srcElement.defaultValue + " is clicked");
 				}else if(event.srcElement.defaultValue == "View"){ 
 				}
				console.log(event.srcElement.id);
				this.onEditCallback(event.srcElement.id);
 				this.i = 0;
			} 
		},
	 
		setVisibility: function(isShow){
			if(isShow){
				domStyle.set(this.domNode, "display", "inherit"); 
			}
			else{
				domStyle.set(this.domNode, "display", "none");
			}
		},
	        
		destroy: function(){ 
			if(this.gridUsersListWidget == null){
				return;
			}
			this.gridUsersListWidget.noDataMessage = "";
			this.gridUsersListWidget.set("store", null);
		},
		
		postCreate: function() {
			this.inherited(arguments);   
			this.setProps();
			this.gridUsersListWidget = new (declare([OnDemandGrid, Selection]))({  
				sort:[{attribute: "lastName"}], 
			    columns: {	   
						firstName: bundle.firstName,
						lastName: bundle.lastName,
						userName: bundle.userName, 
						//SIM-140 -Disabling for simplot PHASE-1
						/*active	:{ 
									field: "_item",
									//sortable: false,
									label: bundle.active,
									formatter: function(usersList) { 
										debugger; 
										var temp = (usersList.active == 1) ? "checked" : "";
										var disableString = "disabled";
										var id = "\"active|"+usersList.userId+"|"+usersList.firstName+"|"+usersList.lastName+"|"+usersList.email+"|"+usersList.userName+"\"";
										return "<input id="+id+" type =\"checkbox\" "+ temp +" "+  disableString +"><label for="+id+"></label></input>";
									}
								   },
						action	:{ 
								field: "_item",
								label: bundle.action,
								//sortable: false,
								formatter: function(usersList) {
									debugger;
									// var id = "\""+usersList.userId+"\"";
									var id = "\""+usersList.id+"\"";
									return "<input id="+id+" class=\"editaction\" type =\"submit\" value=\"Edit\"></input>";
								}
                            }*/
	            },
			  cellNavigation: false
			}, this.gridUsersList);

			this.gridUsersListWidget.set("sort","lastName");
		}
			
			
    });
});

