define(
	[	"dojo/_base/declare", 
	 	"ec/util", 
		"dojo/_base/lang",
		 "widgets/OVCUserDetails/controller", 
	   //	 "widgets/OVCUsersList/controller", 
	   	 "OVCManagementConsole/controller",
		 "widgets/OVCUsersList/widget",
	   	"widgets/OVCUserDetails/widget",
	     "dojo/request"
   	],  
   function(declare, Utils, lang, UserDetailsController, ManagementController,  OVCUsersList, OVCUserDetails, request) {
   
	var updateUsers = function(inputparams){  
		return Utils.execProcess("AddUpdateUser", inputparams).then(userUpdateCallback);  
	};
		
	var userUpdateCallback = function(data){
		debugger;
		console.log("data in userUpdateCallback = " + data);
		if(Utils.handleError(data.returnCode)){  
		} else{ 
			console.log("in controller userUpdateCallback error")
			Utils.showProgIndDlg("Error", data.returnMsg);
		}  
		return data;
	};
	
	var getUsers = function(){ 
		return Utils.execProcess("GetUsers", {}).then(userListCallback);  
	};
	
	var userListCallback = function(data){
		debugger;
		console.log("data in controller = " + data);
		if(Utils.handleError(data.returnCode)){ 
			usersListWidget.destroy();
			usersListWidget.setData(data);
		} else{ 
			console.log("in controller AddUpdateDevicePeripheral error")
			Utils.showProgIndDlg("Error", data.returnMsg);
		}  
		return data;
	};
	
	var usersListWidget = null;
	var userDetails = null;
	var mgmtconsole = null;
	
	var UsersListController = declare(null, {	  
		setParent: function(widget){
			this.usersListWidget = widget.userListWidget;
			usersListWidget = widget.userListWidget;
			mgmtconsole = widget;
			//usersListWidget.setProps(widget.userListWidget);
		},
		 		 
		setUsers : function(){		
			debugger;
			return getUsers(); 
		},
		 
		onButtonClickCallback: function(eventtype, capturedData){
			debugger;
			console.log(capturedData);
			console.log("eventType = " + eventtype + " capturedData = "+ capturedData)
			if(eventtype == "save"){ 
				for(i in capturedData){
					var userDetails = capturedData[i];
					console.log("capturedData["+i+"]"+ userDetails);  
					updateUsers(userDetails);
				} 
			}else if(eventtype == "cancel"){
				var lastSelected = mgmtconsole.lastSelected; 
				//ManagementController.resetToDBForLastSelected(lastSelected, UsersListController);
			} 
		},
		  
		navigateToUsersList: function(){ 
			mgmtconsole.toggleRightPane(usersListWidget);
			return getUsers();  
		},
			
		saveToDB:function(inputParams){ 
			debugger;
			console.log("call inside saveToDB = " +inputParams );  
			var lastSelected = mgmtconsole.lastSelected; 
			//UsersListController.saveToDeviceDB(inputParams).then(lang.hitch(ManagementController, ManagementController.resetToDBForLastSelected(lastSelected, UsersListController)));
		}		 
	}); 
	var UsersListController = new UsersListController();
	return  UsersListController;
	
});
