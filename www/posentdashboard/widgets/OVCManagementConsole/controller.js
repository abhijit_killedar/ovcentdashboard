define(
	[
	 	"dojo/_base/declare", 
	 	"ec/util", 
		"dojo/_base/lang",
	   	"OVCManagementConsole/widget",
	   	"OVCDeviceSearch/controller" ,
	   	"OVCDeviceDetails/controller" ,
		"widgets/OVCUsersList/controller" ,
		"widgets/OVCUserDetails/controller" ,
		"widgets/OVCLocationList/controller" ,
		"widgets/OVCLocationDetails/controller" ,
	   	"dojo/request"
   	],  
   function(declare, Utils, lang, OVCManagementConsole, DeviceSearchController, DeviceDetailsController, UsersListController, UserDetailsController, LocationListController, LocationDetailsController, request) {
		 
  	var ManagementController = declare(null, {		
 		setParent: function(widget){
			parentView = widget;
		},
		
		getParent: function(){
			return parentView;
		},
		 
		performLogout: function(){ 
			console.log("Inside perform logout");
			document.location.href = '../posentdashboard/index.html';
		},
  
		navigateToEditScreen : function(id){
 			var device = DeviceDetailsController.initializeDeviceDetails(ManagementController); 
 			parentView.deviceDetails = device; 
 			parentView.toggleRightPane(device);	
			DeviceDetailsController.setDevice(id);
		},
		
		navigateToEdituserScreen : function(id){ 
 			var device = UserDetailsController.initializeUserDetails(UsersListController); 
			//var device = UserDetailsController.initializeUserDetails(ManagementController); 
 			parentView.toggleRightPane(device);	
			UserDetailsController.setUser(id);
		},
		
		navigateToEditlocationScreen : function(id){ 
			var device = LocationDetailsController.initializeLocationDetails(LocationListController); 
 			parentView.toggleRightPane(device);	
			LocationDetailsController.setLocation(id);
		},
		 
		resetToDBForLastSelected: function(lastSelected, DeviceSearchController){			
			console.log("call inside resetToDB = " +  lastSelected);
			if(lastSelected == "Active"){ 
				DeviceSearchController.setDevices(true, false, false);
			}
			else if(lastSelected == "Inactive"){ 
				DeviceSearchController.setDevices(false, false, false);
			} 
			else if(lastSelected == "Deleted") { 
				DeviceSearchController.setDevices(false, true, false);
			}
			else { 
				DeviceSearchController.setDevices(true, false, true);
			} 
			parentView.toggleRightPane(parentView.deviceSearchWidget);	
		} ,
		
		resetToDB: function(){ 
			var lastSelected = parentView.lastSelected;
			this.resetToDBForLastSelected(lastSelected, DeviceSearchController);
		},
		

		resetToDBuser: function(){ 
			var lastSelected = parentView.lastSelected;
			UsersListController.setUsers(false, true, false);
			parentView.toggleRightPane(parentView.userListWidget);	
		}
		
	});
	var parentView = null;
	var ManagementController = new ManagementController();
	return  ManagementController;
	
});
