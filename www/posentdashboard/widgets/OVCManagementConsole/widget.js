define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/dom",
	"dojo/on",
	"dijit/_WidgetBase", 
	"dijit/_TemplatedMixin", 
	"dijit/_OnDijitClickMixin",
    "dijit/_WidgetsInTemplateMixin",
	"dijit/layout/ContentPane",
	"dojo/dom-construct", 
	"dijit/layout/BorderContainer",
	"custome/AccordionView/widget",
	"custome/FileUploadView/widget", 
	"widgets/OVCDeviceSearch/widget",
	"OVCDeviceSearch/controller",
	"widgets/OVCManagementConsole/controller",
	"widgets/OVCHeader/controller",
	"widgets/OVCHeader/widget",
	"widgets/OVCReport/widget",
	"widgets/OVCReport/controller",
	"widgets/OVCRolesList/widget",
	"widgets/OVCRolesList/controller",
	"widgets/OVCUsersList/widget",
	"widgets/OVCUsersList/controller",
	"widgets/OVCLocationList/widget",
	"widgets/OVCLocationList/controller",
	"widgets/OVCUpload/controller",
	
], function(declare, lang, dom, on, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin,
        _WidgetsInTemplateMixin, ContentPane, domConstruct, BorderContainer, OVCAccordionView,  FileUploadView,
        OVCDeviceSearch, DeviceSearchController, ManagementController, HeaderController, OVCHeader, 
        OVCReport, ReportController, OVCRolesList, RolesController, OVCUsersList, UsersListController, 
        OVCLocationList, LocationListController, UploadController){
	var viewToggle = function(newWidget){
		if(lastSelectedView!=null)
			lastSelectedView.setVisibility(false);
		lastSelectedView = newWidget; 
		rightPane.addChild(newWidget);  
		newWidget.setVisibility(true); 
		
	};
	

	var lastSelectedView = null;
	var bc = null;

    var leftPane = null;
    var rightPane = null;
    var topPane = null;
   //var leftPaneAccordian = null;
	
	return declare("widgets/OVCManagementConsole/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin,
	                                                                                          _WidgetsInTemplateMixin], {
	
	       templatePath : dojo.moduleUrl("widgets/OVCManagementConsole","widget.html"),      
	       
	        // A class to be applied to the root node in our template
	        baseClass: "OVCManagementConsole",
	         
	        rightPaneTab : null,
	        headerLayout : null,
	        deviceSearchWidget : null, 
	        roleListWidget : null, 
			userListWidget :null,
			locationListWidget:null,
			deviceDetails: null,
			reportWidget:null,
			uploadWidget: null,
	        leftPaneAccordian : null,
			
	        constructor: function(){ 
		    	bc = new BorderContainer({
	                style: "height: 1024px;"
	            });
		    	 
		    	 topPane = new ContentPane({
	                 region:"top", 
                     style:"width: 100%"
	            });
		         bc.addChild(topPane);
		    	 
	            // create a ContentPane as the left pane in the BorderContainer
	            leftPane = new ContentPane({
	                    region:"left", 
                        style:"width: 16%",
                        splitter:"true"
	            });
	            bc.addChild(leftPane);
	            
	            // create a ContentPane as the center pane in the BorderContainer
	            rightPane = new ContentPane({
	                region: "center",
					style:"overflow-y:hidden !important"
	            });
 	            bc.addChild(rightPane);
	            // put the top level widget into the document, and then call startup()
 	        },
 
	        postCreate: function() {
 	        	this.leftPaneAccordian = new OVCAccordionView();
				on(this.leftPaneAccordian, "click", lang.hitch(this,this.leftPaneSelect));
      			//this.deviceSearchWidget 	= DeviceSearchController.createDeviceSearchWidget();
    			this.deviceSearchWidget 	=  new OVCDeviceSearch();
				this.userListWidget 		=  new OVCUsersList();
				this.roleListWidget 		=  new OVCRolesList();
				this.locationListWidget 	=  new OVCLocationList();
				this.uploadWidget 			=  new FileUploadView();  
				this.reportWidget 			=  new OVCReport(); 
				
    			DeviceSearchController.setParent(this);
    			
      			this.headerLayout = new OVCHeader({title:localStorage.getItem("lastUname") },  domConstruct.create("div"));
      			this.headerLayout.on("logout",  function(){ 
					 console.log("Inside logout method");
					 ManagementController.performLogout();
					});
 	        	this.reload();
			},
			
			reload: function(){
				this.addTopPane(this.headerLayout);
 				this.addLeftPane(this.leftPaneAccordian); 
				this.lastSelected = "Inactive";
  			    DeviceSearchController.setDevices(false, false, false);
 			    this.setRightPaneProp(this.deviceSearchWidget);  
 				this.deviceSearchWidget.setProps( this.deviceSearchWidget);
  			    viewToggle(this.deviceSearchWidget) ;
	        	bc.placeAt(domConstruct.create("div", {}, this.tab));
 			    bc.startup();
			},
			
			addTopPane: function(node){
 				topPane.addChild(node);
 				this.setTopPaneData();
			},
			
			setTopPaneData: function(){ 
				HeaderController.setParent(this.headerLayout); 
				HeaderController.setData(); 
			},
			
			addLeftPane: function(node){
 				leftPane.addChild(node);
 				this.setLeftPaneData();
			},
			
			setLeftPaneProp: function(values){
				for(i in values){
					dojo.style(this.AccordionNode, i, values[i]);
				}
			},

			setLeftPaneData: function(){ 
				this.leftPaneAccordian.setData(); 
			},
			
			lastSelected: null,
			
			_getLastSelectedAttr : function(){
				return lastSelected;
			},
			

			_setLastSelectedAttr : function(newValue){
				this.lastSelected = newValue;
			},
			
			leftPaneSelect : function(values){
 				if(values.srcElement.childElementCount == 0){
 					debugger;
					console.log(this);
 					console.log("listItemClick:: values[srcElement] = " + values.srcElement.outerText);
 					console.log("listItemClick:: values[srcElement] = " + values.target.offsetParent.id);  
 					var newViewToShow = null;
 					if(values.target.offsetParent.id == "id1"){
 						this.lastSelected = "Inactive";
 						newViewToShow = this.deviceSearchWidget ;
 						DeviceSearchController.setDevices(false, false, false);
						this.setRightPaneProp(this.deviceSearchWidget); 
 					}
 					else if(values.target.offsetParent.id == "id2"){
 						this.lastSelected = "Active";
 						newViewToShow = this.deviceSearchWidget ;
 						DeviceSearchController.setDevices(true, false, false);
						this.setRightPaneProp(this.deviceSearchWidget); 
 					} 
 					else if(values.target.offsetParent.id == "id3") {
 						this.lastSelected = "Deleted";
 						newViewToShow = this.deviceSearchWidget ;
 						DeviceSearchController.setDevices(false, true, false);
						this.setRightPaneProp(this.deviceSearchWidget); 
 					}
 					else if(values.target.offsetParent.id == "id5") {
						debugger;
 						this.lastSelected = "All Users";
  						UsersListController.setParent(this);
 						UsersListController.setUsers(false, true, false);
						this.setRightPaneProp(this.userListWidget); 
						//UsersListController.setProp(this);
 						newViewToShow = this.userListWidget; 
						
 					}
 					else if(values.target.offsetParent.id == "id6") {
 						debugger;
 						this.lastSelected = "All Roles";
						this.setRightPaneProp(this.roleListWidget); 
 						RolesController.setParent(this);
 						RolesController.setRoles(false, true, false);
 						newViewToShow = this.roleListWidget;  
 					}
					else if(values.target.offsetParent.id == "id7") {
 						debugger;
 						this.lastSelected = "All Locations";
 						LocationListController.setParent(this);
 						LocationListController.setLocations(false, true, false);
 						newViewToShow = this.locationListWidget;  
						this.setRightPaneProp(this.locationListWidget); 
 					}
					else if(values.target.offsetParent.id == "id8") {
 						debugger;
 						this.lastSelected = "All Reports";
 						ReportController.setReport(this,'transactions');
 						newViewToShow = this.reportWidget;  
						this.setRightPaneProp(this.reportWidget); 
 					}
					else if(values.target.offsetParent.id == "id9") {
 						debugger;
 						this.lastSelected = "All Reports";
 						ReportController.setReport(this,'tillTotals');
 						newViewToShow = this.reportWidget;  
						this.setRightPaneProp(this.reportWidget); 
 					}
 					else if(values.target.offsetParent.id == "id10") {
 						debugger;
 						this.lastSelected = "Sales Reports";
 						ReportController.setReport(this,'Sales Summary');
 						newViewToShow = this.reportWidget;  
						this.setRightPaneProp(this.reportWidget); 
 					}
 					else if(values.target.offsetParent.id == "id11") {
 						debugger;
 						this.lastSelected = "JDE Extract Reports";
 						ReportController.setReport(this,'JDExtract');
 						newViewToShow = this.reportWidget;  
						this.setRightPaneProp(this.reportWidget); 
 					}
 					else if(values.target.offsetParent.id == "id12") {
 						debugger;
 						this.lastSelected = "Transaction Items";
 						ReportController.setReport(this,'transactionItems');
 						newViewToShow = this.reportWidget;  
						this.setRightPaneProp(this.reportWidget); 
 					}
 					else if(values.target.offsetParent.id == "idUpload") {
 						debugger;
 						this.lastSelected = "Upload";
 						UploadController.setFileUploadView(this,'');
 						newViewToShow = this.uploadWidget;  
						this.setRightPaneProp(this.uploadWidget); 
 					}else   {
 						this.lastSelected = "All";
 						newViewToShow = this.deviceSearchWidget ;
 						DeviceSearchController.setDevices(true, false, true); 
						this.setRightPaneProp(this.deviceSearchWidget); 
 					}
 					
 					viewToggle(newViewToShow);  
				} 	
			},
			
			toggleRightPane : function(newView){
				viewToggle(newView);
			},

			reloadRightPane: function(node){
				rightPane.addChild(node);  
				node.setVisibility(true); 
				bc.startup();
			},
			
			addRightPane: function(node){
				rightPane.addChild(node);  
				node.setVisibility(false); 
			},
			 
			setRightPaneProp: function(widget){
				debugger;
				var newwidget=widget;
				//var widget = this.deviceSearchWidget; 
				if(newwidget ==  this.deviceSearchWidget){
					debugger;
				  	this.deviceSearchWidget.onEditCallback = ManagementController.navigateToEditScreen;
				  	this.deviceSearchWidget.onButtonClickCallback = DeviceSearchController.onButtonClickCallback; 
				}
				else if(newwidget == this.userListWidget){
					debugger;
				  	this.userListWidget.onEditCallback = ManagementController.navigateToEdituserScreen;
	 			  	this.userListWidget.onButtonClickCallback = UsersListController.onButtonClickCallback;  
				}
				else if(newwidget == this.locationListWidget){
				  	this.locationListWidget.onEditCallback = ManagementController.navigateToEditlocationScreen;
				  	this.locationListWidget.onButtonClickCallback = LocationListController.onButtonClickCallback;
 				}
				else if(newwidget == this.roleListWidget){
				  	//this.roleListWidget.onEditCallback = ManagementController.navigateToEditlocationScreen;
				  	this.roleListWidget.onButtonClickCallback = RolesController.onButtonClickCallback;
 				}
			},
			  
			addRightPaneProp: function(values){
				for(i in values){
					dojo.style(this.deviceSearchWidget, i, values[i]);
				}
			}
		});//declare
}//function

);
