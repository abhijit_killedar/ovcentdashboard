define(
	["dojo/_base/declare", 
	 "dojo/_base/lang",
	 "ec/util", 
	 "widgets/OVCUserDetails/widget",
   	 "widgets/OVCManagementConsole/controller",
	 "widgets/OVCUsersList/controller"
   	 
   	],  
   function(declare, lang, Utils,  OVCUserDetails, ManagementController, UsersListController ) {
	var getUserByID = function(id){ 
			var inputParams = { 
				"userId" : id 
			}; 
			return Utils.execProcess("GetUsers", inputParams).then(function(data){
				for(i in data)
				console.log("in controller GetUserPeripheral data["+i+"] = " + data[i]); 
				if(Utils.handleError(data.returnCode)){ 
														userDetails.setData(data);
													}else{
														
														console.log("incontrollerGetUserPeripheralerror");
														Utils.showProgIndDlg("Error", data.returnMsg);
													}
			}); 
	}; 
	
	var getLocations = function(){   
		 var inputParams = { 
					"entityType" : "Location", 
					"searchOperators" :["NOTEQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :["*"], 
					"searchType" : ["AND"] 
				}
		     
		 return Utils.execProcess("ObjectSearch", inputParams).then(function(data){
			 		
					console.log("in controller GetDevicePeripheral data = " + data);
					userDetails.setLocationData(data);
		});  
	}; 
	
	var getRoles = function(){   
		 var inputParams = { 
					"entityType" : "Role", 
					"searchOperators" :["NOTEQUALS"], 
					"searchParams" :["id"], 
					"searchValues" :["*"], 
					"searchType" : ["AND"] 
				}
		     
		 return Utils.execProcess("ObjectSearch", inputParams).then(function(data){ 
					console.log("in controller GetDevicePeripheral data = " + data);
					userDetails.setRoleData(data);
		});  
	}; 

	var saveToUserDB = function(inputParams){ 
		for(i in inputParams)
				console.log("in controller AddUpdateuserPeripheral inputParams["+i+"] = " + inputParams[i]); 
			return Utils.execProcess("AddUpdateUser", inputParams).then(function(data){
						
						for(i in data)
							console.log("in controller AddUpdateUserPeripheral data["+i+"] = " + data[i]); 
						if(Utils.handleError(data.returnCode)){ 
							resetToDB();
						} else{
							 
							console.log("in controller AddUpdateUserPeripheral error")
							Utils.showProgIndDlg("Error", data.returnMsg);
						} 
 			}); 
	};
	
	var navigateToUserListing = function(){ 
		if(userDetails!=null){
			userDetails.setVisibility(false);  
			UsersListController.navigateToUsersList(); 
		}
	};
	
	var resetToDB = function(){  
		userDetails.setVisibility(false);  
		UsersListController.navigateToUsersList(); 
	};
	  
	 
	var userDetails = null; 
	var UsersListController=null;
	var UserDetailsController = declare(null, {	  
			initializeUserDetails: function(controller){
			
			if(userDetails==null){   
			console.log(controller); 
				UsersListController=controller;
				userDetails = new OVCUserDetails();    	
				userDetails.on("save",  function(){ 
													debugger;
													var rolesData = [];
													var splitStr = userDetails.getTBData("txtbxroles").split(",");
													var locationId = userDetails.getTBData("txtbxlocation");
													for(i in splitStr){
														if(splitStr[i]!=""){
															rolesData.push({
																locationId : locationId,
																roleId: splitStr[i] 
															});
														}
													}
													var inputParams = { 	
															"userId" :  userDetails.getTBData("txtbxuserId"),
															"userName" :  userDetails.getTBData("txtbxuserName"), 
															"firstName" :userDetails.getTBData("txtbxfirstName"),
															"lastName": userDetails.getTBData("txtbxlastName"),
															"email": userDetails.getTBData("txtbxemail"),
															"resetpassword": userDetails.getTBData("txtbxresetpassword"),
															"retypepassword": userDetails.getTBData("txtbxretypepassword"), 
															"rolesList" : rolesData,
															"active": userDetails.getCBData("chkbxactive") 
													};
													saveToUserDB(inputParams);
													});
				userDetails.on("cancel", resetToDB); 	
				userDetails.on("delete", navigateToUserListing);  
 				userDetails.startup();
			} 
			return userDetails;
		},
		
		setUser: function(id){
			userDetails.setID(id);
			getUserByID(id);
			getLocations();
			getRoles();
			userDetails.setVisibility(true);
		}, 
		
		
		saveToUserDB : saveToUserDB,
		navigateToUserListing : navigateToUserListing,
		resetToDB : resetToDB,
	
	}); 
	var UserDetailsController = new UserDetailsController();
	return  UserDetailsController;
	
});
