var colorMemoryStore ;
define([ 
    	"dojo/_base/array",
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/on",
	"dgrid/Grid", 
	"dgrid/OnDemandList",
	"dgrid/Selection",
	"dgrid/Keyboard",  
	"dojo/data/ObjectStore",
	"dojo/data/ItemFileReadStore",
	"dojo/store/Memory", 
	"dojo/store/Observable",
	"dgrid/OnDemandGrid",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
	"dijit/_OnDijitClickMixin", 
 	"dijit/registry",
	//"dijit/Dialog",
	"dojo/dom",
    "dojo/dom-class",
	"dojo/dom-attr",
	"dojo/query",
    "dojo/dom-style",
	"dojo/json",
    "dojo/has!testing?dojo/text!widgets/OVCUserDetails/widget.html:dojo/text!widgets/OVCUserDetails/widget.html",
	"dojo/i18n!resource/nls/OVCUserDetails", 
	"dojox/mobile/Button",
	"dojox/mobile/EdgeToEdgeStoreList",
	"dojox/mobile/FilteredListMixin",
	"dojo/dom-construct",
	"dojo/ready",
	"dojox/mobile/RoundRectList",
	"dojox/mobile/ScrollableView",
	"dojox/mobile/ListItem",
	"dojox/mobile/ComboBox",
	"dojox/mobile/CheckBox",
	"dojox/mobile/TextBox",
    "dojox/mobile/Heading",
  	"dojox/mobile/ToolBarButton",
	"dojox/mobile/SearchBox",
	"dojox/form/DropDownSelect",
	"dijit/form/ComboBox",
	//"dijit/form/Select",
	"dijit/form/FilteringSelect",
	"dijit/form/ValidationTextBox",
	"dojox/mobile/SimpleDialog",
	"dojox/mobile/EdgeToEdgeList",
	"dojox/mobile/ContentPane",
	"dojox/mobile/EdgeToEdgeCategory"
], function(array, declare, lang, on, Grid,  List, Selection, Keyboard, ObjectStore, ItemFileReadStore, Memory, Observable, OnDemandGrid, _WidgetBase, _TemplatedMixin, _OnDijitClickMixin, registry, dom, domClass, domAttr, query, domStyle, JSON, template, bundle, Button, EdgeToEdgeStoreList, FilteredListMixin, domConstruct, ready, RoundRectList, ScrollableView, ListItem, ComboBox, DropDownSelect, FilteringSelect){
	// This function has a local copy of the variables it was passed so the values will not change as the loop iterates
	function makeButtonEvent(eventType, captureData, context) {
		return lang.hitch(context, function() {
			this._buttonClick(eventType, captureData);
		});
	}
	
	 var DropDown = declare([List, Selection, Keyboard]);
	 
	function createDropDownSelectNewCorner(parentNode,c){ 		
		var OVCnewdiv = new dojox.mobile.ContentPane({class:"mySelect dynamic_dd",id:"select"+c
		}, dojo.doc.createElement('div')).placeAt(parentNode);
		
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"label button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
			
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"arrow button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
															
		var store3 = new Memory({ identifier:"id",                 
								data: [
									  { id: "manager", name: "Manager" ,value: "Manager"},
									  { id: "associate", name: "Associate" ,value:"Associate"},
									  { id: "cashier", name: "Cashier",value: "Cashier"}
									]
								});
			
		function renderItem(item){
			var divNode, textNode;
			textNode = document.createTextNode(item.name);
			divNode = domConstruct.create("div", {
				"class": item.name
			});
			domConstruct.place(textNode, divNode);
			return divNode;
		}
 
		var dropDown = new DropDown({
				selectionMode: "single",
				store: store3,
				//value:selecteddata,
				renderRow: renderItem		
		});
			domConstruct.place(dropDown.domNode, "select"+c);
			dropDown.startup();

		var open = false;
				function toggle(state){
						open = state;
						domClass[open ? "add" : "remove"](dropDown.domNode, "opened");
				}
				on(dom.byId("select"+c), ".button:click", function(e){
						toggle(!open);
				});
			
		var label = query(".label", dom.byId("select"+c))[0];
					dropDown.on("dgrid-select", function(e){
						var node = renderItem(e.rows[0].data);
						domConstruct.place(node, label, "only");
						toggle(false);
					});
			
	};	
	
	
	function createDropDownSelectNewLocCorner(parentNode,c){ 
			
		var OVCnewdiv = new dojox.mobile.ContentPane({class:"mySelect dynamic_dd",id:"selectloc"+c
		}, dojo.doc.createElement('div')).placeAt(parentNode);
		
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"label button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
		
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"arrow button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
															
		var store1 = new Memory({ identifier:"id",                 
								data: [
									  { id: "southboston", name: "South Boston" ,value: "South Boston"},
									  { id: "northboston", name: "North Boston" ,value:"North Boston"},
									  { id: "boston", name: "Boston",value: "Boston"}
									]
								  });
			
		function renderItem(item){
						var divNode, textNode;
						textNode = document.createTextNode(item.name);
						divNode = domConstruct.create("div", {
							"class": item.name
						});
						domConstruct.place(textNode, divNode);
						return divNode;
		}
 
		var dropDown = new DropDown({
				selectionMode: "single",
				store: store1,
				//value:selecteddata,
				renderRow: renderItem		
		});
			domConstruct.place(dropDown.domNode, "selectloc"+c);
			dropDown.startup();

		var open = false;
				function toggle(state){
						open = state;
						domClass[open ? "add" : "remove"](dropDown.domNode, "opened");
				}
				on(dom.byId("selectloc"+c), ".button:click", function(e){
						toggle(!open);
				});
			
		var label = query(".label", dom.byId("selectloc"+c))[0];
					dropDown.on("dgrid-select", function(e){
						var node = renderItem(e.rows[0].data);
						domConstruct.place(node, label, "only");
						toggle(false);
					});
			
	};
	 
	 
	function createDropDownSelectCorner(parentNode,labelText,id,selecteddata){ 
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
	
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
	
		var OVCnewdiv = new dojox.mobile.ContentPane({class:"mySelect",id:"select"
		}, dojo.doc.createElement('div')).placeAt(parentNode);
		
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"label button",id:"selectlabel",innerHTML:selecteddata
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
			
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"arrow button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
															
		var store = new Memory({ identifier:"id", 

					data: [
						  { id: "manager", name: "Manager" ,value: "Manager"},
						  { id: "associate", name: "Associate" ,value:"Associate"},
						  { id: "cashier", name: "Cashier",value: "Cashier"}
						]
					  });
			
		function renderItem(item){
						var divNode, textNode;
						textNode = document.createTextNode(item.name);
						divNode = domConstruct.create("div", {
							"class": item.name
						});
						domConstruct.place(textNode, divNode);
						return divNode;
		}
 
		var dropDown = new DropDown({
				selectionMode: "single",
				store: store,
				//value:selecteddata,
				renderRow: renderItem		
		});
			domConstruct.place(dropDown.domNode, "select");
			dropDown.startup();

		var open = false;
				function toggle(state){
						open = state;
						domClass[open ? "add" : "remove"](dropDown.domNode, "opened");
				}
				on(dom.byId("select"), ".button:click", function(e){
						toggle(!open);
				});
			
		var label = query(".label", dom.byId("select"))[0];
					dropDown.on("dgrid-select", function(e){
						var node = renderItem(e.rows[0].data);
						domConstruct.place(node, label, "only");
						toggle(false);
					});
			
	};
	

	function createDropDownSelectLocCorner(parentNode,labelText,id,selecteddata){ 
			
		var OVCnewdiv = new dojox.mobile.ContentPane({class:"mySelect",id:"select1"
		}, dojo.doc.createElement('div')).placeAt(parentNode);
		
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"label button",id:"select1label",innerHTML:selecteddata
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
			
		var OVCnewdiv1 = new dojox.mobile.ContentPane({class:"arrow button"
		}, dojo.doc.createElement('div')).placeAt(OVCnewdiv);
															
		var store1 = new Memory({ identifier:"id", 
                
								data: [
									  { id: "southboston", name: "South Boston" ,value: "South Boston"},
									  { id: "northboston", name: "North Boston" ,value:"North Boston"},
									  { id: "boston", name: "Boston",value: "Boston"}
									]
								  });
			
		function renderItem(item){
						var divNode, textNode;
						textNode = document.createTextNode(item.name);
						divNode = domConstruct.create("div", {
							"class": item.name
						});
						domConstruct.place(textNode, divNode);
						return divNode;
		}
 
		var dropDown = new DropDown({
				selectionMode: "single",
				store: store1,
				//value:selecteddata,
				renderRow: renderItem		
		});
			domConstruct.place(dropDown.domNode, "select1");
			dropDown.startup();

		var open = false;
				function toggle(state){
						open = state;
						domClass[open ? "add" : "remove"](dropDown.domNode, "opened");
				}
				on(dom.byId("select1"), ".button:click", function(e){
						toggle(!open);
				});
			
		var label = query(".label", dom.byId("select1"))[0];
					dropDown.on("dgrid-select", function(e){
						var node = renderItem(e.rows[0].data);
						domConstruct.place(node, label, "only");	
						toggle(false);
					});
	};

	function createComboBoxCorner(parentNode,labelText,id,selecteddata){ 
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
	
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
			
			colorMemoryStore = new Memory({ idProperty: "name", data: [
												{ name: "Mr" },
												{ name: "Mrs" },
												]});	
														
		var myCombo = new dojox.mobile.ComboBox({
				name: id,
				id: id,
				store: colorMemoryStore ,
				value:  selecteddata,
			}).placeAt(parentNode);
			OVCeditlabel.startup();
			myCombo.startup();
	
	};

	function createCheckBoxCorner(activeStr, activeVal, labelText, ParentNode){
		OVCUsersCheckBoxContainer = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
		}, dojo.doc.createElement('div')) .placeAt(ParentNode);
 		createDynamicCheckBox(OVCUsersCheckBoxContainer, activeVal, activeStr, "Aktiv","disabled");
 		//createDynamicCheckBox(OVCUsersCheckBoxContainer, activeVal, activeStr, "Aktiv");//(SIM-140 making stuff readonly for PHASE-1)
 		//createDynamicCheckBox(OVCUsersCheckBoxContainer, false, "ResetData", "Daten zurücksetzen");
	};
	
	function createDynamicCheckBox(parentNode, checked, id, labelText,disabled){  
		//var myDiv = new dojox.mobile.ContentPane({class:"OVCinnerdiv"}, dojo.doc.createElement('div')) .placeAt(parentNode);
 		chckboxReset = new dojox.mobile.CheckBox({ 
			name: id, 
			label: id, 
			checked : checked,  
			id:"chkbx"+id,
			disabled : (disabled !=null) ? disabled : false 
		}) .placeAt(OVCUsersCheckBoxContainer); 
		chckboxReset.startup();  
		OVCUsersCheckBoxContainer.domNode.appendChild(dojo.doc.createTextNode(labelText));
	};
	
	function createButtons(buttonDefinitions, parentNode){
		for (var i = 0; i < buttonDefinitions.length; i++) {
			var buttonDefinition = buttonDefinitions[i];
			var label = buttonDefinition.label;
			var eventType = buttonDefinition.eventType || buttonDefinition.label;
			var captureData = buttonDefinition.captureData;

			var button = new Button({label: label}).placeAt(parentNode.OVCUsersbuttonsContainer);
			button.startup();
			
			// use a function to pass the correct values of the params as we process the array
			// @see makeButtonEvent() 
			button.on("click", makeButtonEvent(eventType, captureData, parentNode));
			 
		}
	};
	

	
	function createDynamicTextBox(parentNode, labelText, id, boxText, disabled){
		var OVCeditlabel = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
	
		OVCeditlabel.domNode.appendChild(dojo.doc.createTextNode(labelText));
		
		var textbox = new dojox.mobile.TextBox({
			name: id,
			label: id,
			id: "txtbx"+id,
			value: boxText,
			disabled : (disabled !=null) ? disabled : false 
		},dojo.doc.createElement('input')).placeAt(parentNode);
		OVCeditlabel.startup();
		textbox.startup();
	};
	
	
	function createDynamicPassTextBox(parentNode, labelText, id, boxText, disabled){
		var OVCeditlabel1 = new dojox.mobile.ContentPane({class:"labels"
		}, dojo.doc.createElement('label')).placeAt(parentNode);
		
		parentNode.innerHTML = "<input type='password' />";
		var inpassw = parentNode.firstchild;
	
		OVCeditlabel1.domNode.appendChild(dojo.doc.createTextNode(labelText));
		
		var textbox = new dojox.mobile.TextBox({
			type:'password',
			name:"password",
			label: id,
			id: "txtbx"+id,
			value: boxText,
			//disabled : (disabled !=null) ? disabled : false 
			disabled : (disabled !=null) ? disabled : false 
		},inpassw).placeAt(parentNode);
		OVCeditlabel1.startup();
		textbox.startup();
	};
	 
	
	function createTextBoxCorner(devicetypeDefinition, parentNode){
 		var OVCUserDetailsform = new dojox.mobile.ContentPane({class:"OVCinnerdiv"
		}, dojo.doc.createElement('div')) .placeAt(parentNode);
	  
 		//(SIM-140 making stuff readonly for PHASE-1),added disabled field to all createDynamicTextBox
 		
		if(j == "userId"){ 
 			createDynamicTextBox(OVCUserDetailsform, "User ID"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "firstName"){ 
			createDynamicTextBox(OVCUserDetailsform, "First Name"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "lastName"){ 
			createDynamicTextBox(OVCUserDetailsform, "Last Name"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "gender"){ 
			createComboBoxCorner(OVCUserDetailsform,"Gender"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "email"){ 
			createDynamicTextBox(OVCUserDetailsform, "Email"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "resetpassword"){ 
			createDynamicPassTextBox(OVCUserDetailsform, "Reset Password"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "retypepassword"){ 
			createDynamicPassTextBox(OVCUserDetailsform, "Retype Password"+":", j, devicetypeDefinition,"disabled");
		}else if(j == "roles"){ 
			createDynamicTextBox(OVCUserDetailsform, "Role"+":", j, devicetypeDefinition,"disabled");
		} 
	};
	
	
	function resetData(){  
		var widget = registry.byId("txtbxfirstName").set("value", "");
		var widget = registry.byId("txtbxlastName").set("value", "");
		var widget = registry.byId("txtbxuserId").set("value", "");
		//var widget = registry.byId("gender").set("value", "");
		var widget = registry.byId("txtbxemail").set("value", "");
		var widget = registry.byId("txtbxresetpassword").set("value", "");
		var widget = registry.byId("txtbxretypepassword").set("value", ""); 
		//var widget = registry.byId("roles").set("value", "");
		var widget = registry.byId("chkbxactive").set("checked", false);		
	};
	
	var chckboxActive = null;
	
	//var store;
	
	var chckboxReset = null;
	 
	var OVCUsersCheckBoxContainer = null;
	
	var OVCUserDetailsform = null;
	
	return declare("widgets/OVCUserDetails/widget",[_WidgetBase, _TemplatedMixin, _OnDijitClickMixin ], {
	  
 		buttonDefinitions: [],
 		devicetypeDefinitions: [],
 		activeDefinitions: [],  
        templateString: template, 
        id: null,
         
        setID: function(temp){
        	this.id = temp;
        },
        
        constructor: function(){
             this.inherited(arguments); 
  			 
        },
         
        getTBData: function(widgetId){   
         	var widget = registry.byId(widgetId);
         	//var value = widget.get("value");
			var value = widget.get("value");
         	if(value == null)
         		value = "";

        	return value;
        },
		
		getDDData: function(widgetId){   
         	var widget = dom.byId(widgetId);
			//var value =dom.byId(widgetId).innerHTML;
			var value =dom.byId(widgetId).innerText;
         	if(value == null)
         		value = "";

        	return value;
        },
		
        
        getCBData: function(widgetId){
        	debugger;
        	var widget = registry.byId(widgetId); 
    		if(widget.id == "chkbxactive"){
    			return widget.get("checked") == true ? "1" : "0";
         	}else{
         		return widget.get("checked") == true ? "true" : "false";
         	} 
        },
        
        reload: function(){ 
			
			var heading = new dojox.mobile.Heading({
			  dojoType: "dojox.mobile.Heading",
			  moveTo: "mblFormLayoutTitle",
			  fixed: "top",
			  label: "Edit User Detail"
			}).placeAt(this.titleNode);

			var OVCUserDetailsform = new dojox.mobile.ContentPane({class:"OVCcheckerdiv"
			}, dojo.doc.createElement('div')) .placeAt(this.OVCUsersContainers);					
				
			debugger;
			var deviceData = bundle; 
			console.log(deviceData);
			console.log("deviceData = " + deviceData.SearchObjectList); 
			for (j in deviceData.SearchObjectList) {  
				
					var devicetypeDefinition = deviceData.SearchObjectList[j];
					createTextBoxCorner(devicetypeDefinition, this.OVCUsersContainers); 
				
			}
			 		
			var activeStr = "active";
			var activeVal = (deviceData.SearchObjectList.active == 1) ? true : false;
			createCheckBoxCorner(activeStr, activeVal, "Active", this.OVCUsersContainers);
			createButtons(deviceData.buttonDefinitions, this); 
		
		},
									
						
        	
        setData: function(deviceData){   
         	resetData(); 
			for (j in deviceData.usersList[0]) { 
 		  		console.log("deviceData["+j+"]= " + deviceData.usersList[0][j]);  
 		  		var devicetypeDefinition = deviceData.usersList[0][j];
	        	if(j == "userId"){   
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "gender"){   
	    			var widget = registry.byId("gender"); 
	            	widget.set("value", devicetypeDefinition);
	    		}
				else if(j == "firstName"){
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		} else if(j == "lastName"){ 
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "email"){ 
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "resetpassword"){ 
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", "");
	    		}else if(j == "retypepassword"){ 
	     			var widget = registry.byId("txtbx"+j); 
	            	widget.set("value", "");
	    		}else if(j == "roleList"){ 
					var widget = registry.byId("txtbxroles");
					var roleValue = "";
					for(i in devicetypeDefinition)
						roleValue += devicetypeDefinition[i].roleId+",";
					widget.set("value", roleValue);
	    		}else if(j == "location"){  
					var widget = dom.byId("select1label"); 
	            	widget.set("value", devicetypeDefinition);
	    		}else if(j == "active"){ 
	    			debugger;
	     			var widget = registry.byId("chkbx"+j); 
	            	widget.set("checked", devicetypeDefinition == 1 ? true : false);
	    		}
        	}
		},
		        
        // A class to be applied to the root node in our template
        baseClass: "OVCUserDetails",


		_buttonClick: function(eventType, captureData) {
			// object to emit
			var event = {};
	
			if (captureData && this.getChildren()[0]) {
				var children = this.getChildren()[0].getChildren();
				for (var i = 0; i < children.length; i++) {
					var child = children[i];
					switch (child.declaredClass) {
					case "dojox.mobile.ComboBox":
					case "dojox.mobile.TextBox":
					case "dojox.mobile.ExpandingTextArea":
					case "dojox.mobile.Slider":
						event[child.name] = child.value;
						break;
					case "dojox.mobile.CheckBox":
					case "dojox.mobile.ToggleButton":
						event[child.name] = child.checked;
						break;
					case "dojox.mobile.RadioButton":
						if (child.checked) {
							event[child.name] = child.value;
						};
						break;
					case "dojox.mobile.RoundRectList":
					case "dojox.mobile.EdgeToEdgeList":
						findSelectedItems(child, event);
						break;
					}
				}
			}

			// emit eventType
			this.emit(eventType, event);
		},
		
		setVisibility: function(isShow){
	        if(isShow)
        		domStyle.set(this.domNode, "display", "inherit");
        	else
        		domStyle.set(this.domNode, "display", "none");
        },
        
		postCreate: function() {
 			
			this.inherited(arguments); 
			this.reload(); 
					
 		}	 
	});
});
