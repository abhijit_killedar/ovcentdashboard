define(
	[	"dojo/_base/declare", 
		"dojo/request",
	 	"ec/util", 
   	 	"dojo/dom-construct" 
   	],  
   function(declare,request, Utils, domConstruct) { 
	var uploadWidget = null;
	var reportDetails = null;
	var mgmtconsole = null;
	
	var upload = function(){   
		 var inputParams = {};
 		 return Utils.execProcess("upload", inputParams).then(function(data){
					console.log("in controller GetDevicePeripheral data = " + data);
		});  
	}; 
 
	var UploadController = declare(null, {	  
		setFileUploadView : function(widget, URL){	   
			mgmtconsole = widget; 
			uploadWidget = widget.uploadWidget;
			data.URL = URL;
 			uploadWidget.setData(data);
			return;
		}
	});
	  
	var UploadController = new UploadController();
	return  UploadController; 
});