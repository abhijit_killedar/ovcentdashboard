package com.ovc.entdash.dataimport.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import com.ovc.entdash.dataimport.pojo.DevicePeripheral;
import com.ovc.entdash.dataimport.pojo.RestPayloadModel;
import com.oneview.jaxrs.JAXRSClientHelper;
/*import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;*/



import java.util.Properties;

import com.oneview.util.StreamHelper;

import org.json.JSONObject;

public class BulkAddUpdate {
	private static final String fname = "dashboard.conf";
	
	public static String processAddUpdateDvcPerprl(File file) {
		
		// Build the jsonRequest content
		StringBuilder returnString = new StringBuilder();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		int lineNum = 0;
		String errorString="";
		try {
			 
			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				lineNum ++;
			        // use comma as separator
				String[] dataItems = line.split(cvsSplitBy); 
				
				final RestPayloadModel rstPayldMdl = new RestPayloadModel();
				rstPayldMdl.setSource("external");
				rstPayldMdl.setUsername("eCommerce");
				rstPayldMdl.setPassword("changeme");
				
				final DevicePeripheral dvcPerphrl = new DevicePeripheral();
				dvcPerphrl.setDeviceId(dataItems[0]);
				dvcPerphrl.setLocationId(dataItems[1]);
				dvcPerphrl.setPrinterIP(dataItems[2]);
				dvcPerphrl.setDeviceActive(dataItems[3]);	
				
				rstPayldMdl.setData(dvcPerphrl);
				
				final JSONObject jsonObj = new JSONObject(rstPayldMdl);
				System.out.println("rest Payload: "+jsonObj.toString());
				
				//do the rest call to POSMClient
				errorString = doRestCall(jsonObj);
				
				if(returnString.length() > 0){
					returnString.append("<br>processed line ["+lineNum+" ] Values ["+line+"]");
					returnString.append("<br>"+errorString);
				}
				else{
					returnString.append("processed line ["+lineNum+" ] Values ["+line+"]");
					returnString.append("<br>"+errorString);
				}
			}
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e){
			returnString.append("invalid parameters on line ["+lineNum+" ] Values ["+line+"]");
			if(errorString.length() > 0)
				returnString.append("<br> Error Details :"+errorString);
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return returnString.toString();
	}

	private static String doRestCall(JSONObject jsonObj) {
		
		String error = "";
		//DefaultClientConfig config = new DefaultClientConfig();
		//Client client = Client.create(config);
		//WebResource service = client.resource(getAddUpdateDvcPerphrlURI());

		//ClientResponse response = service.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, input);
		System.out.println("URL :"+getAddUpdateDvcPerphrlURI().toString());
		Response response = JAXRSClientHelper.executeRequest(getAddUpdateDvcPerphrlURI().toString(), Entity.entity(jsonObj.toString(), MediaType.APPLICATION_JSON_TYPE));
		
		String strResponse = null;
		try (InputStream is = response.readEntity(InputStream.class);
			Reader r = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			strResponse = StreamHelper.read(r, false);
			if (!(response.getStatus() == 200 || response.getStatus() == 201)) {
				error = "Failed : HTTP error code : "+ response.getStatus();
				error = error + "<br>" + strResponse;
			}
			else{
				error = strResponse;
				//error = error + "<br>" + strResponse;
			}
		} catch (IOException ioe) {
			error = "Failed : HTTP error code : "+ response.getStatus();
			
		}
		return error;
	}
	
	private static URI getAddUpdateDvcPerphrlURI() {
		String posClientIP = "";
		String confDir = System.getProperty("ovc.prop.dir");
		System.out.println("ovc.prop.dir :"+confDir);
		Properties prop = new Properties();
		try{
			FileInputStream fis = new FileInputStream(confDir + "/" + fname);
			prop.load(fis);
			fis.close();
		}catch(IOException e){
			e.printStackTrace();
		}

		posClientIP = (null != prop.getProperty("POSMClient.IP")) ? prop.getProperty("POSMClient.IP") : posClientIP;
		posClientIP = posClientIP.isEmpty() ? "127.0.0.1:9190" : posClientIP;
		return UriBuilder
				.fromUri(
						"http://" + posClientIP + "/POSMClient/json/process/execute/AddUpdateDevicePeripheral")
				.build();
	}
}
