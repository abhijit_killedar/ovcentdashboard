package com.ovc.entdash.dataimport.pojo;

public class DevicePeripheral {
	private String deviceId;
	private String deviceAlias;
	private String locationId;
	private String printerIP;
	private String deviceType ;
	private String printerType;
	private String printerModel;
	private String printerVersion;
	private String printerAlias;
	private String printerIsActive;
	private String printerIsDeleted;
	private String deviceActive;
	private String resetDevice;
	
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceAlias() {
		return deviceAlias;
	}
	public void setDeviceAlias(String deviceAlias) {
		this.deviceAlias = deviceAlias;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getPrinterIP() {
		return printerIP;
	}
	public void setPrinterIP(String printerIP) {
		this.printerIP = printerIP;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getPrinterType() {
		return printerType;
	}
	public void setPrinterType(String printerType) {
		this.printerType = printerType;
	}
	public String getPrinterModel() {
		return printerModel;
	}
	public void setPrinterModel(String printerModel) {
		this.printerModel = printerModel;
	}
	public String getPrinterVersion() {
		return printerVersion;
	}
	public void setPrinterVersion(String printerVersion) {
		this.printerVersion = printerVersion;
	}
	public String getPrinterAlias() {
		return printerAlias;
	}
	public void setPrinterAlias(String printerAlias) {
		this.printerAlias = printerAlias;
	}
	public String getPrinterIsActive() {
		return printerIsActive;
	}
	public void setPrinterIsActive(String printerIsActive) {
		this.printerIsActive = printerIsActive;
	}
	public String getPrinterIsDeleted() {
		return printerIsDeleted;
	}
	public void setPrinterIsDeleted(String printerIsDeleted) {
		this.printerIsDeleted = printerIsDeleted;
	}
	public String getDeviceActive() {
		return deviceActive;
	}
	public void setDeviceActive(String deviceActive) {
		this.deviceActive = deviceActive;
	}
	public String getResetDevice() {
		return resetDevice;
	}
	public void setResetDevice(String resetDevice) {
		this.resetDevice = resetDevice;
	}
	
	
	
}
