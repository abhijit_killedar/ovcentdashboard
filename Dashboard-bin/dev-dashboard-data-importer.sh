#!/bin/bash

java -Dovc.prop.dir=/Applications/ovc/ovc-repo/config -Dovc.scheme=http -Dasync=false -Dovc.host=127.0.0.1 -Dovc.port=8080 -Dovc.webcontext=POSMClient -Dovc.repo_loc=/Applications/ovc/ovc-repo -cp /Applications/ovc/lib/ovc-tools.jar com.oneview.tools.AppDataImporter Dashboard-Server-Data Dashboard-Client-Data ovc-all false

